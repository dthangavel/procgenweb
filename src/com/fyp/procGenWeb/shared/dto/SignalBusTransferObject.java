package com.fyp.procGenWeb.shared.dto;

import java.io.Serializable;

import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.logicHelper.InvalidSignalException;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class SignalBusTransferObject implements Serializable {
	private Signal value[];
	public Signal[] getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	private String name;
	
	public SignalBusTransferObject(){}
	
	public SignalBusTransferObject(SignalBus signalBusToSerialize){
		this.value = signalBusToSerialize.getValue();
		this.name = signalBusToSerialize.getName();
	}
	
	public SignalBus convertToSignalBus(){
		SignalBus newSignalBus = new SignalBus(this.name, this.value.length);
		try {
			newSignalBus.setValue(value);
		} catch (InvalidSignalException e) {
			e.printStackTrace();
		}
		
		return newSignalBus;
	}
}
