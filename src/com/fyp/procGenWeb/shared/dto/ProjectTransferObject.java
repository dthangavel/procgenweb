/**
 * 
 */
package com.fyp.procGenWeb.shared.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Connection;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusConstantMapping;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusUniqueLocater;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectToClockCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectToConstant;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.InitialiseSignalBusCommand;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class ProjectTransferObject implements Serializable{
	String name;
	
	// used instead of entity manager
	ArrayList<EntityTransferObject> baseEntityList = new ArrayList<EntityTransferObject>();
	
	// used instead of project connection manager
	HashMap<String, HashMap<String, List<ConnectionTransferObject>>> connectionDirectory = new HashMap<String, HashMap<String, List<ConnectionTransferObject>>>();
	HashMap<String,List<SignalBusConstantMapping>> constantsMap = new HashMap<String,List<SignalBusConstantMapping>>(); // stores entityId, signalName, constant value
	
	List<SignalBusUniqueLocater> signalBusConnectedToClock = new ArrayList <SignalBusUniqueLocater>();
	HashMap<SignalBusUniqueLocater,Integer> initialisationValueList = new HashMap<SignalBusUniqueLocater,Integer>();
	
	public ProjectTransferObject(){}
	
	public ProjectTransferObject(Project projectToConvert){
		this.name = projectToConvert.getName();
		for(Entity entityToAdd :projectToConvert.getEntityManager().getBaseEntities())
		{
			this.baseEntityList.add(entityToAdd.convertToEntityTransferObject(null));

			HashMap<String,List<Connection>> signalConnectionMapForEntity = projectToConvert.getConnectionManager().getConnectionForEntity(entityToAdd.getId());
			
			
			HashMap<String,List<ConnectionTransferObject>> signalConnectionTransferMapForEntity = new HashMap<String,List<ConnectionTransferObject>>();
			Iterator<String> signalItr = signalConnectionMapForEntity.keySet().iterator();
			while(signalItr.hasNext()){
				String signalName = signalItr.next();
				
				ArrayList<ConnectionTransferObject> connectionTransferObjectList = new ArrayList<ConnectionTransferObject>();
				for(Connection connectionToConvert: signalConnectionMapForEntity.get(signalName)){
					connectionTransferObjectList.add(new ConnectionTransferObject(connectionToConvert));
				}
				
				
				signalConnectionTransferMapForEntity.put(signalName, connectionTransferObjectList);
				
			}
			
			this.connectionDirectory.put(entityToAdd.getId(), signalConnectionTransferMapForEntity);
		}
		
		this.constantsMap = projectToConvert.getConnectionManager().getConstantConnections();
		
		this.signalBusConnectedToClock = projectToConvert.getProjectSimulator().getSignalBusListConnectedToClock();
		this.initialisationValueList = projectToConvert.getProjectSimulator().getInitialisationValueList();
	}

	public Project convertToProject() throws ProcGenException {
		Project newProject = new Project(this.name);
		for(EntityTransferObject baseEntity: baseEntityList){
			baseEntity.convertToEntityAndAddToProject(newProject);
		}
		
		HashMap<String, HashMap<String, List<Connection>>> connectionDirectoryToCreate = newProject.getConnectionManager().getConnectionDirectory();
		Iterator<String> entityItr = this.connectionDirectory.keySet().iterator();
		
		
		while(entityItr.hasNext()){
			String entityId1 = entityItr.next();
			Iterator<String> signalItr = this.connectionDirectory.get(entityId1).keySet().iterator();
			HashMap<String,List<Connection>> newConnectionToAdd = new HashMap<String,List<Connection>>();
			
			while(signalItr.hasNext()){
				
				String signalName = signalItr.next();
				List<ConnectionTransferObject> connectiondtoList = this.connectionDirectory.get(entityId1).get(signalName);
				List<Connection> connectionList = new ArrayList<Connection>();	
				for(ConnectionTransferObject connectionDtoToConvert: connectiondtoList){
					//connectionList.add(connectionDtoToConvert.convertToConnection());
					ConnectCommand cnctCommand = new ConnectCommand();
					cnctCommand.establishConnection(connectionDtoToConvert.sourceEntityId, connectionDtoToConvert.destinationEntityId, connectionDtoToConvert.inputSignal.getName(), connectionDtoToConvert.outputSignal.getName(), newProject);
				}
//				newConnectionToAdd.put(signalName, connectionList);
			}
//			connectionDirectoryToCreate.put(entityId1, newConnectionToAdd);
		}
		
//		newProject.getConnectionManager().setConstantMap(this.constantsMap);
		Iterator<String> entityIterator = this.constantsMap.keySet().iterator();
		while(entityIterator.hasNext()){
			String id = entityIterator.next();
			List<SignalBusConstantMapping> signalBusMappingList= this.constantsMap.get(id);
			for(SignalBusConstantMapping signalBusMapping:signalBusMappingList){
				ConnectToConstant cmd = new  ConnectToConstant();
				cmd.establishConstantConnection(newProject, id, signalBusMapping.getSignalBusName(), String.valueOf(signalBusMapping.getSignalBusValue()));
			}
		}
		
		// Connect entities to clock
		ConnectToClockCommand connectToClockCommand = new ConnectToClockCommand();
		for(SignalBusUniqueLocater uniqueSignalBus:this.signalBusConnectedToClock){
			connectToClockCommand.establishClockConnection(newProject, uniqueSignalBus.getEntityId(), uniqueSignalBus.getSignalBusName());
		}
		
		// Initialise signal bus value
		InitialiseSignalBusCommand signalBusInitCommand = new InitialiseSignalBusCommand();
		for(SignalBusUniqueLocater uniqueSignalBus:this.initialisationValueList.keySet()){
			String entityId = uniqueSignalBus.getEntityId();
			String signalBusName = uniqueSignalBus.getSignalBusName();
			int width = newProject.getEntityManager().getEntityById(entityId).getSignalBusByName(signalBusName).getBusWidth();
			signalBusInitCommand.initialiseSignalBusStartValue(newProject, entityId, signalBusName, SignalBus.getStringForInteger(uniqueSignalBus.getSignalBusValue(), width));
		}
		
		return newProject;
	}
	
	
}
