/**
 * 
 */
package com.fyp.procGenWeb.shared.dto.entities;

import java.util.ArrayList;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit.MemoryUnitType;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class MemoryUnitDto extends EntityTransferObject {

	
	List<String> storageUnit = new ArrayList<String>();
	MemoryUnitType memoryUnitType;
	int noOfExtraInOut;
	
	public MemoryUnitDto() {
	}

	/**
	 * @param entityToSerialize
	 * @param parent
	 */
	public MemoryUnitDto(MemoryUnit memoryUnitToSerialize, EntityTransferObject parent) {
		super(memoryUnitToSerialize, parent);
		this.storageUnit = memoryUnitToSerialize.getStorage();
		this.memoryUnitType = memoryUnitToSerialize.getMemoryUnitType();
		this.noOfExtraInOut = memoryUnitToSerialize.getNumberOfExtraInOut();
	}
	
	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
		assert (this.inputList.size() > 0);
		MemoryUnit memUnit = new MemoryUnit(this.name,this.noOfExtraInOut);
		
		memUnit.programMemoryUnit(this.storageUnit);
		memUnit.setMemoryUnitType(this.memoryUnitType);
		
		return super.convertToEntityAndAddToProject(prj,memUnit);
	}

}
