package com.fyp.procGenWeb.shared.dto.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.ComplexMultiplexer;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

@SuppressWarnings("serial")
public class ComplexMultiplexerDto extends EntityTransferObject{
	int noOfDataInputs;
	int noOfSelectionInputs;
	
	public ComplexMultiplexerDto(){}
	
	public ComplexMultiplexerDto(ComplexMultiplexer complexMux, EntityTransferObject parent) {
		super(complexMux,parent);
		this.noOfDataInputs = complexMux.getNumberOfDataInputs();
		this.noOfSelectionInputs = complexMux.getNumberOfSelectionInputs();
	}

	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
		assert (this.inputList.size() > 0);
		Entity complexMux = super.convertToEntityAndAddToProject(prj, new ComplexMultiplexer(this.name,noOfDataInputs,noOfSelectionInputs));
		return complexMux;
	}
}
