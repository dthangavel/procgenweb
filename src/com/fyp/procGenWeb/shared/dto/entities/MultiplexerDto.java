/**
 * 
 */
package com.fyp.procGenWeb.shared.dto.entities;

import java.util.HashMap;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Multiplexer;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class MultiplexerDto extends EntityTransferObject{

	HashMap<String,String> inputOutputMappingDto = new HashMap<String,String>();
	
	public MultiplexerDto(){}
	
	public MultiplexerDto(Multiplexer mult, EntityTransferObject parent){
		super(mult,parent);
		this.inputOutputMappingDto = mult.getMuxMapping();
	}
	
	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
	
		assert (this.inputList.size() > 0);
		Multiplexer newMux = new Multiplexer(this.name);
		
		newMux.setMuxMapping(inputOutputMappingDto);
		return super.convertToEntityAndAddToProject(prj,newMux);
	}
	
}
