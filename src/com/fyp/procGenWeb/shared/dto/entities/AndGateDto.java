/**
 * 
 */
package com.fyp.procGenWeb.shared.dto.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.AndGate;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class AndGateDto extends EntityTransferObject{

	public AndGateDto(){}
	
	public AndGateDto(AndGate andGate, EntityTransferObject parent) {
		super(andGate,parent);
	}

	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
		assert (this.inputList.size() > 0);
		Entity andGate = super.convertToEntityAndAddToProject(prj, new AndGate(
				"", this.name));
		return andGate;
	}
}
