/**
 * 
 */
package com.fyp.procGenWeb.shared.dto.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusJoint;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class SignalBusJointDto extends EntityTransferObject {

	/**
	 * 
	 */
	public SignalBusJointDto() {}
	
	public SignalBusJointDto(SignalBusJoint signalBusJoint, EntityTransferObject parent) {
		super(signalBusJoint,parent);
	}

	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
		assert (this.inputList.size() == 2);
		
		int input0Width = this.inputList.get(0).getValue().length;
		int input1Width = this.inputList.get(1).getValue().length;
		
		Entity signalBusJoint = super.convertToEntityAndAddToProject(prj, new SignalBusJoint(name));
		return signalBusJoint;
	}

}
