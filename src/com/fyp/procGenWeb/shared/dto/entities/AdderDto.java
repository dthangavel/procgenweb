/**
 * 
 */
package com.fyp.procGenWeb.shared.dto.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Adder;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class AdderDto extends EntityTransferObject {

	int input0BusWidth;
	int input1BusWidth;

	public AdderDto() {}
	
	public AdderDto(Adder adder, EntityTransferObject parent) {
		super(adder,parent);
		if(adder.getInputPortList().size()>2)
			throw new UnsupportedOperationException(Consts.ExceptionMessages.TWO_INPUT_AND_GATE_SUPPORTED);
		this.input0BusWidth = adder.getInputPortList().get(0).getBusWidth();
		this.input1BusWidth = adder.getInputPortList().get(1).getBusWidth();
	}

	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
		assert (this.inputList.size() > 0);
		Entity andGate = super.convertToEntityAndAddToProject(prj, new Adder(name,2,input0BusWidth,input1BusWidth,false));
		return andGate;
	}

}
