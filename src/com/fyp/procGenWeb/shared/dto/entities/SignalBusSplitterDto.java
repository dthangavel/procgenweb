/**
 * 
 */
package com.fyp.procGenWeb.shared.dto.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusSplitter;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class SignalBusSplitterDto extends EntityTransferObject {

	int inputBusWidth;
	int outputWidth;
	int startingIndex;
	
	public SignalBusSplitterDto() {}

	/**
	 * @param entityToSerialize
	 * @param parent
	 */
	public SignalBusSplitterDto(SignalBusSplitter signalBusSplitter,
			EntityTransferObject parent) {
		super(signalBusSplitter, parent);
		this.inputBusWidth = signalBusSplitter.getInputPortList().get(0).getBusWidth();
		this.startingIndex = signalBusSplitter.getStartingIndex();
		this.outputWidth = signalBusSplitter.getOutputPortList().get(0).getBusWidth();
	}

	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
		assert (this.inputList.size() == 1);
		
		Entity signalBusSplitter = super.convertToEntityAndAddToProject(prj, new SignalBusSplitter(name,this.inputBusWidth,this.outputWidth,this.startingIndex,false));
		return signalBusSplitter;
	}
}
