/**
 * 
 */
package com.fyp.procGenWeb.shared.dto.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Register;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class RegisterDto extends EntityTransferObject {

	public RegisterDto() {}

	/**
	 * @param entityToSerialize
	 * @param parent
	 */
	public RegisterDto(Entity entityToSerialize, EntityTransferObject parent) {
		super(entityToSerialize, parent);
	}

	public Entity convertToEntityAndAddToProject(Project prj){
		try {
			Entity reg = super.convertToEntityAndAddToProject(prj, new Register(this.name,entityTriggerType));
			return reg;
		} catch (ProcGenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
