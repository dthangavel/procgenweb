package com.fyp.procGenWeb.shared.dto.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Comparator;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Multiplexer;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

@SuppressWarnings("serial")
public class ComparatorDto  extends EntityTransferObject{
	Comparator.Type type;
	public ComparatorDto(){}
	
	public ComparatorDto(Comparator comp, EntityTransferObject parent){
		super(comp,parent);
		this.type = comp.getType();
	}
	
	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
	
		assert (this.inputList.size() > 0);
		Comparator newMux = new Comparator(this.name,this.type);
		return super.convertToEntityAndAddToProject(prj,newMux);
	}
}
