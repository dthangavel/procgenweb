/**
 * 
 */
package com.fyp.procGenWeb.shared.dto.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.OrGate;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class OrGateDto extends EntityTransferObject {

	public OrGateDto() {}

	/**
	 * @param entityToSerialize
	 * @param parent
	 */
	public OrGateDto(Entity entityToSerialize, EntityTransferObject parent) {
		super(entityToSerialize, parent);
	}
	
	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
		assert (this.inputList.size() > 0);
		Entity orGate = super.convertToEntityAndAddToProject(prj, new OrGate(
				"", this.name));
		return orGate;
	}

}
