/**
 * 
 */
package com.fyp.procGenWeb.shared.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Connection;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity.EntityTriggerType;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityType;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusConstantMapping;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectToConstant;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class EntityTransferObject implements Serializable{
	public String id;
	public String name;
	public EntityTransferObject parentEntity;
	public EntityTriggerType entityTriggerType;
	
	public List<SignalBusTransferObject> inputList = new ArrayList<SignalBusTransferObject>();
	public List<SignalBusTransferObject> outputList = new ArrayList<SignalBusTransferObject>();
	public List<EntityTransferObject> entityList = new ArrayList<EntityTransferObject>();
		
	// this attribute keeps track of whether the entity should be converted to a standalone entity
	public HdlConversionType hdlConversionType;

	public EntityType entityType;
	public HashMap<String, HashMap<String, List<ConnectionTransferObject>>> connectionDirectory = new HashMap<String, HashMap<String, List<ConnectionTransferObject>>>();
	public HashMap<String,List<SignalBusConstantMapping>> constantsMap = new HashMap<String,List<SignalBusConstantMapping>>(); // stores entityId, signalName, constant value
	
	public EntityTransferObject(){}
	
    public EntityTransferObject(Entity entityToSerialize,EntityTransferObject parent){
        this.id = entityToSerialize.getId();
        this.name = entityToSerialize.getName();
        if(entityToSerialize.getParent()!= null)
            this.parentEntity = parent;
        this.entityTriggerType = entityToSerialize.getEntityTriggerType();
        this.entityType = entityToSerialize.getEntityType();
        
        for(SignalBus signalBusToConvert: entityToSerialize.getInputPortList())
            this.inputList.add(new SignalBusTransferObject(signalBusToConvert));
        

        for(SignalBus signalBusToConvert: entityToSerialize.getOutputPortList())
            this.outputList.add(new SignalBusTransferObject(signalBusToConvert));

        
        for(Entity entityToConvert: entityToSerialize.getChildEntityList())
            this.entityList.add(entityToConvert.convertToEntityTransferObject(this));

        this.hdlConversionType = entityToSerialize.getHdlConversionType();
    
        // connection manager related stuff
        HashMap<String, HashMap<String, List<Connection>>> connectionDirectoryOriginal = entityToSerialize.getEntityConnectionManager().getConnectionDirectory();
        Iterator<String> entityItr = connectionDirectoryOriginal.keySet().iterator();
        
        
        while(entityItr.hasNext()){
            String entityId = entityItr.next();
            Iterator<String> signalItr = connectionDirectoryOriginal.get(entityId).keySet().iterator();
            HashMap<String,List<ConnectionTransferObject>> newConnectionToAdd = new HashMap<String,List<ConnectionTransferObject>>();
            
            while(signalItr.hasNext()){
                List<ConnectionTransferObject> connectiondtoList = new ArrayList<ConnectionTransferObject>();
                String signalName = signalItr.next();
                List<Connection> connectionList = connectionDirectoryOriginal.get(entityId).get(signalName);    
                for(Connection connectionToConvert: connectionList){
                    connectiondtoList.add(new ConnectionTransferObject(connectionToConvert));
                }
                newConnectionToAdd.put(signalName, connectiondtoList);
            }
            this.connectionDirectory.put(entityId, newConnectionToAdd);
        }
        
        this.constantsMap = entityToSerialize.getEntityConnectionManager().getConstantConnections();
    }
	
	public Entity convertToEntityAndAddToProject(Project prj) throws ProcGenException{
		return convertToEntityAndAddToProject(prj,null);
	}
	
	public Entity convertToEntityAndAddToProject(Project prj,Entity entityToConvert) throws ProcGenException{
		
		Entity newEntity = null;
		if(entityToConvert == null)
			newEntity = new Entity(this.name);
		
		else
			newEntity = entityToConvert;
		
		EntityManager prjEntityManager = prj.getEntityManager();
		
		if(this.parentEntity != null)
			newEntity.setParent(prjEntityManager.getEntityById(this.parentEntity.id));
		else{
			newEntity.setParent(null);
		}
		
		newEntity.setEntityTriggerType(this.entityTriggerType);
		newEntity.setHdlConversionType(this.hdlConversionType);
		
		String entityId = null;
		try {
			entityId = prjEntityManager.addEntity(newEntity);
		} catch (ProcGenException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (SignalBusTransferObject signalDto : inputList) {
			try {
				if (entityId != null)
					prjEntityManager.addInputSignal(entityId,
							signalDto.getName(), signalDto.getValue().length);
			} catch (ProcGenException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for (SignalBusTransferObject signalDto : outputList) {
			try {
				if (entityId != null)
					prjEntityManager.addOutputSignal(entityId,
							signalDto.getName(), signalDto.getValue().length);
			} catch (ProcGenException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for (EntityTransferObject entityDto : entityList) {
			if (entityId != null)
				entityDto.convertToEntityAndAddToProject(prj);
		}

		HashMap<String, HashMap<String, List<Connection>>> connectionDirectoryToCreate = newEntity.getEntityConnectionManager().getConnectionDirectory();
		Iterator<String> entityItr = this.connectionDirectory.keySet().iterator();
		
		
		while(entityItr.hasNext()){
			String entityId1 = entityItr.next();
			Iterator<String> signalItr = this.connectionDirectory.get(entityId1).keySet().iterator();
			HashMap<String,List<Connection>> newConnectionToAdd = new HashMap<String,List<Connection>>();
			
			while(signalItr.hasNext()){
				
				String signalName = signalItr.next();
				List<ConnectionTransferObject> connectiondtoList = this.connectionDirectory.get(entityId1).get(signalName);
				List<Connection> connectionList = new ArrayList<Connection>();	
				for(ConnectionTransferObject connectionDtoToConvert: connectiondtoList){
//					connectionList.add(connectionDtoToConvert.convertToConnection());
					ConnectCommand cnctCommand = new ConnectCommand();
					cnctCommand.establishConnection(connectionDtoToConvert.sourceEntityId, connectionDtoToConvert.destinationEntityId, connectionDtoToConvert.inputSignal.getName(), connectionDtoToConvert.outputSignal.getName(), prj);
				}
//				newConnectionToAdd.put(signalName, connectionList);
			}
//			connectionDirectoryToCreate.put(entityId1, newConnectionToAdd);
		}
		Iterator<String> entityIterator = this.constantsMap.keySet().iterator();
		while(entityIterator.hasNext()){
			String id = entityIterator.next();
			List<SignalBusConstantMapping> signalBusMappingList= this.constantsMap.get(id);
			for(SignalBusConstantMapping signalBusMapping:signalBusMappingList){
				ConnectToConstant cmd = new  ConnectToConstant();
				cmd.establishConstantConnection(prj, id, signalBusMapping.getSignalBusName(), String.valueOf(signalBusMapping.getSignalBusValue()));
			}
		}
		
		return newEntity;
	}

//	private Entity instantiateAppropriateEntity(
//			EntityTransferObject entityTransferObject) {
//		
//		try {
//		switch(entityTransferObject.entityType){
//			case AndGate: return new AndGate("", this.name, this.inputList.size());
//			case OrGate: return new OrGate("",this.name,this.inputList.size());
//			case Mux: return new Multiplexer(this.name, this.inputList.size(),this.inputList.size()> 1?this.inputList.get(1).getValue().length:0);
//			case Register: return new Register(this.name, 0,entityTriggerType);
//		} 		
//		}catch (ProcGenException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
}
