/**
 * 
 */
package com.fyp.procGenWeb.shared.dto;

import java.io.Serializable;

import com.fyp.procGenWeb.client.procGen.electronics.core.Connection;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class ConnectionTransferObject implements Serializable{
	String sourceEntityId;
	String destinationEntityId;
	SignalBusTransferObject inputSignal;
	SignalBusTransferObject outputSignal;
	
	SignalBusTransferObject inputOutputMapping;
	
	public ConnectionTransferObject(){}
	
	public ConnectionTransferObject(Connection connectionToSerialize){
		this.sourceEntityId = connectionToSerialize.getSourceEntityId();
		this.destinationEntityId = connectionToSerialize.getDestinationEntityId();
		this.inputSignal = new SignalBusTransferObject(connectionToSerialize.getInputSignal());
		this.outputSignal = new SignalBusTransferObject(connectionToSerialize.getOutputSignal());
		
		this.inputOutputMapping = new SignalBusTransferObject(connectionToSerialize.getInputOutputMapping());
	}
	
	public Connection convertToConnection(){
		
		SignalBus inputSignalBus = this.inputSignal.convertToSignalBus();
		SignalBus outputSignalBus = this.outputSignal.convertToSignalBus();
		SignalBus inputOutputMapping = this.inputOutputMapping.convertToSignalBus();
		
		Connection newConnection = new Connection(this.sourceEntityId,this.destinationEntityId,inputSignalBus,outputSignalBus,inputOutputMapping);
		return newConnection;		
	}
}
