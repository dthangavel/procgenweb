/**
 * 
 */
package com.fyp.procGenWeb.client;

import java.util.HashMap;

import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author DINESH THANGAVEL
 *
 */
public interface ImportHdlServiceAsync {

	void transferHdlFiles(HashMap<String, String> hdlFiles,
			AsyncCallback<ProjectTransferObject> callback);

}
