/**
 * 
 */
package com.fyp.procGenWeb.client;

import java.util.HashMap;

import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author DINESH THANGAVEL
 *
 */
@RemoteServiceRelativePath("importhdl")
public interface ImportHdlService extends RemoteService {
	public ProjectTransferObject transferHdlFiles(HashMap<String, String> hdlFiles) throws Exception;
}
