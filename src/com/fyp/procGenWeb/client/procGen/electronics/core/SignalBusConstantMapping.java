/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.core;

import java.io.Serializable;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class SignalBusConstantMapping implements Serializable{ 
	String signalBusName;
	int value;
	int busWidth;
	
	// for serialization
	public SignalBusConstantMapping(){}
	
	public SignalBusConstantMapping(String signalBusName,int valueToSet,int busWidth){
		this.signalBusName = signalBusName;
		this.value = valueToSet;
		this.busWidth = busWidth;
	}
	
	public String getSignalBusName(){
		return this.signalBusName;
	}
	
	public int getSignalBusValue(){
		return this.value;
	}

	public int getSignalBusWidth() {
		return this.busWidth;
	}
	
}
