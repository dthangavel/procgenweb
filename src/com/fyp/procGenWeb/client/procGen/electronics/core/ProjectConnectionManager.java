/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.core;


/**
 * @author DINESH THANGAVEL
 *
 */
public class ProjectConnectionManager extends ConnectionManager{
	
	private Project hostProject;
	
	public ProjectConnectionManager(Project hostProject){
		this.hostProject = hostProject;
	}

	public void updateAboutEvent(EntityChangeEvent entityChangeEvent) throws com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException{
		Entity affectedEntity = entityChangeEvent.entityAfterChange;
		if(affectedEntity.getParent() == null){
			super.updateAboutEvent(entityChangeEvent);
		}
		
		else{
		String parentId = affectedEntity.getParent().getId();
		Entity parentEntity = hostProject.getEntityManager().getEntityById(parentId);
		EntityConnectionManager cm = parentEntity.getEntityConnectionManager();
		cm.updateAboutEvent(entityChangeEvent);
		}
		
		affectedEntity.notifyEntityChangeEvent(entityChangeEvent);
	}
}
