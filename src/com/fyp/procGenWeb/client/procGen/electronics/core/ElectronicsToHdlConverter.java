package com.fyp.procGenWeb.client.procGen.electronics.core;

import java.util.HashMap;

import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

public interface ElectronicsToHdlConverter {

	public HashMap<String,String> convertProjectToHdl(Project project) throws ProcGenException;
}
