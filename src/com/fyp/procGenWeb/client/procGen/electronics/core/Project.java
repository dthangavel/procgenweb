/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectToConstant;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.simulation.ProjectSimulator;

/**
 * @author DINESH THANGAVEL
 *
 */
public class Project implements ProcGenType {
	String name;
	ProjectSimulator projectSim = new ProjectSimulator(this);
	EntityManager entityManager =  new EntityManager(this);
	ProjectConnectionManager connectionManager = new ProjectConnectionManager(this);	
	
	public Project(String projectName){
		this.name = projectName;
	}
	
	public void publishEntityChangeEvent(EntityChangeEvent entityChangeEvent) throws ProcGenException {
		this.connectionManager.updateAboutEvent(entityChangeEvent);
	}
	
	public ProjectConnectionManager getConnectionManager(){
		return this.connectionManager;
	}
	
	public EntityManager getEntityManager(){
		return this.entityManager;
	}
	
	public ProjectSimulator getProjectSimulator(){
		return projectSim;	
	}
	
	public String getName(){
		return this.name;
	}
	
	public String copyEntity(Project sourceProject, Entity sourceEntity, Project destinationProject, String destinationParentId) throws ProcGenException{
		String sourceId =  sourceEntity.getId();
		EntityManager destinationProEntityManager = destinationProject.getEntityManager();
		
		ElectronicsLogicFacade.getInstance().setActiveProjectInstance(destinationProject);
		
		Entity destinationEntity = sourceEntity.deepCopy();
		Entity destinationParentEntity = null;
		
		if(destinationParentId != null && destinationParentId.length() >0){
			destinationParentEntity = destinationProEntityManager.getEntityById(destinationParentId);
		}
		
		destinationEntity.setParent(destinationParentEntity);
		String newDestEntityId = destinationProEntityManager.addEntity(destinationEntity);
		
		this.copyConnections(sourceProject, sourceEntity, destinationProject, destinationEntity, destinationParentId);
		return newDestEntityId;
	}
	
	private void copyConnections(Project sourceProject, Entity sourceEntity, Project destinationProject, Entity destinationEntity, String destinationParentId) throws ProcGenException{
		// example for source entity Id: 2-3
		// destination entity Id can be : 4
		
		// child entity of source entity will become relative to destination entity eg 2-3-1 will now become 4-1
		HashMap<String,HashMap<String,List<Connection>>> connectionsForSourceEntity = sourceEntity.getEntityConnectionManager().connectionDirectory;
		Iterator<String> itr = connectionsForSourceEntity.keySet().iterator();
		
		String textToReplace = sourceEntity.getId();
		String replacementText = destinationEntity.getId();
		
		while(itr.hasNext()){
			String signalSourceEntityId = itr.next();
			List<Connection> connectionInEntity = sourceEntity.getEntityConnectionManager().getAllConnectionsInEntityAsList(signalSourceEntityId);
			for(Connection connectionToAdd:connectionInEntity){
				
				String oldSourceEntityId = connectionToAdd.getSourceEntityId();
				String oldDestinationEntityId = connectionToAdd.getDestinationEntityId();
				
				String newSourceEntityId = oldSourceEntityId.replaceFirst(textToReplace, replacementText);
				String newDestinationEntityId = oldDestinationEntityId.replaceFirst(textToReplace, replacementText);
				
				ConnectCommand newCommand= new ConnectCommand();
				newCommand.establishConnection(newSourceEntityId, newDestinationEntityId, connectionToAdd.getInputSignal().getName(), connectionToAdd.getOutputSignal().getName(),destinationProject);
			}
			
			// connect all constant signals
			List<SignalBusConstantMapping> constantConnectionsList = sourceEntity.getEntityConnectionManager().getConstantConnectionsInEntityAsList(signalSourceEntityId);
			String newEntityId = signalSourceEntityId.replaceFirst(textToReplace, replacementText);
			
			if (constantConnectionsList != null) {
				for (SignalBusConstantMapping mapping : constantConnectionsList) {
					ConnectToConstant cnstCommand = new ConnectToConstant();
					cnstCommand.establishConstantConnection(destinationProject,
							newEntityId, mapping.signalBusName,
							Integer.toString(mapping.value));
				}
			}
		
			// TODO: check the project simulator for connection to clock and input simulations
		//	 sourceProject.projectSim.getInitialValue(signalSourceEntityId, newEntityId);
			
		}
		
		// do for children entities also
		List<Entity> childEntityListForSource = sourceEntity.getChildEntityList();
		List<Entity> childEntityListForDestination = destinationEntity.getChildEntityList();
		
		for(int i=0;i<childEntityListForSource.size();i++){
			this.copyConnections(sourceProject, childEntityListForSource.get(i), destinationProject, childEntityListForDestination.get(i), destinationEntity.getId());
		}
	}
}
