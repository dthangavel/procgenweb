/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.core;

/**
 * @author DINESH THANGAVEL
 *
 */
public interface SignalBusPropertyChangeListener {
	public void propertyChange(SignalBusPropertyChangeEvent evt);
}
