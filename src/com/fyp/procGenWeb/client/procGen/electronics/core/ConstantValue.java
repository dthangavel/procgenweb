/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.core;

/**
 * @author DINESH THANGAVEL
 *
 */
public class ConstantValue implements ProcGenType{

	int constantValue;
	int lengthOfString; // length of the string eg constant value of 0 can be represented as "0000" or just "00"
	
	/**
	 * This class can be used to store constant values
	 */
	public ConstantValue(Integer value,int length) {
		this.constantValue = value;
		this.lengthOfString = length;
	}
	
	public int getIntegerValue(){
		return this.constantValue;
	}
	
	public int getStringLength(){
		return this.lengthOfString;
	}

}
