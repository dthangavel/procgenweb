/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.core;

/**
 * @author DINESH THANGAVEL
 *
 */
public enum EntityType {
	CustomType,
	AndGate,
	OrGate,
	Mux,
	Register;
}
