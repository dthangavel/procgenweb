package com.fyp.procGenWeb.client.procGen.electronics.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;
import com.fyp.procGenWeb.client.procGen.logicHelper.InvalidSignalException;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;
import com.fyp.procGenWeb.shared.dto.entities.ComplexMultiplexerDto;

/**
 *  The input names for complex multiplexer are of the format input0,input1....selectionInput0,selectionInput1
 * 
 * @author DINESH THANGAVEL
 *
 */
public class ComplexMultiplexer extends Entity {
	
	int noOfDataInputs;
	int noOfSelectionInputs;

	/**
	 * This constructor does not add inputs and outputs to the entity. They have to be added manually
	 * 
	 * @param name
	 * @param numberOfDataInputs
	 * @param noOfSelectionInputs
	 */
	public ComplexMultiplexer(String name,int numberOfDataInputs, int noOfSelectionInputs) {
		super(name);
		this.noOfDataInputs = numberOfDataInputs;
		this.noOfSelectionInputs = noOfSelectionInputs;
	}
	
	/**
	 * This version takes care of automatically adding inputs and outputs to the entity
	 * @param name
	 * @param numberOfDataInputs
	 * @param noOfSelectionInputs
	 * @param inputDataBusSize
	 * @throws ProcGenException
	 */
	public ComplexMultiplexer(String name,int numberOfDataInputs, int noOfSelectionInputs, int inputDataBusSize) throws ProcGenException {
		super(name);
		this.noOfDataInputs = numberOfDataInputs;
		this.noOfSelectionInputs = noOfSelectionInputs;
		
		for(int i =0;i<numberOfDataInputs;i++){
			this.addInput("input" + i, inputDataBusSize);
		}
		
		for(int i =0;i<noOfSelectionInputs;i++){
			this.addInput("selectionInput" + i, 1); // one because selection input is just true or false
		}
		
		this.addOutput("output", inputDataBusSize);
	}
	
	@Override
	public void defaultBehaviour() throws InvalidSignalException {
		for(int i =0;i<noOfSelectionInputs;i++){
			if(this.getInputByName("selectionInput" + i).getValue()[0] == Signal.HIGH){
				this.getOutputByName("output").setValue(this.getInputByName("input"+i).getValue());
				break;
			}
		}
	}
	
	@Override
	public Object convertToHdl(HdlConverter hdlConverter){
		return hdlConverter.convertComplexMultiplexer(this);
	}

	@Override
	public ComplexMultiplexerDto convertToEntityTransferObject(EntityTransferObject parent){
		ComplexMultiplexerDto complexMuxDto = new ComplexMultiplexerDto(this,parent);
		return complexMuxDto;	
	}

	public int getNumberOfDataInputs() {
		return this.noOfDataInputs;
	}

	public int getNumberOfSelectionInputs() {
		return this.noOfSelectionInputs;
	}
}
