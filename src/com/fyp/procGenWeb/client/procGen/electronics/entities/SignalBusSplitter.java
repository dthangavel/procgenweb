/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;
import com.fyp.procGenWeb.shared.dto.entities.SignalBusJointDto;
import com.fyp.procGenWeb.shared.dto.entities.SignalBusSplitterDto;

/**
 * @author DINESH THANGAVEL
 *
 */
public class SignalBusSplitter extends Entity {

	int inputBusWidth;
	int outputWidth;
	int startingIndex;
	

	/**
	 * For splitting STD_LOGIC_VECTOR (9 downto 0) into STD_LOGIC_VECTOR (8 downto 0)
	 * inputBusWidth = 10; outputWidth = 9; startingIndex 1
	 * @param name
	 * @throws ProcGenException 
	 */
	public SignalBusSplitter(String gateName,int inputBusWidth,int outputWidth,int startingIndex,boolean withIo) throws ProcGenException{
		
		super(gateName);
		this.inputBusWidth = inputBusWidth;
		this.outputWidth = outputWidth;
		this.startingIndex = startingIndex;
		
		if(withIo){
			this.addInput("input0", inputBusWidth);
			this.addOutput("output", outputWidth);
		}
		
		
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}

	@Override
	public void defaultBehaviour() throws ProcGenException {
		String input0Value = this.getInputByName("input0").getDigitalDisplayValue();
		assert(outputWidth+startingIndex <= inputBusWidth);
		String outputAsString = input0Value.substring(startingIndex, outputWidth+startingIndex); 
				
		this.getOutputByName("output").setValue(SignalBus.getSignalArrayFromString(outputAsString));
	}
	
	@Override
	public SignalBusSplitterDto convertToEntityTransferObject(EntityTransferObject parent){
		SignalBusSplitterDto signalBusSplitterDto = new SignalBusSplitterDto(this,parent);
		return signalBusSplitterDto;
		
	}

	@Override
	public Object convertToHdl(HdlConverter hdlConverter){
		return hdlConverter.convertSignalBusSplitter(this);
	}

	public int getStartingIndex() {
		
		return this.startingIndex;
	}
	
	public int getOutputWidth(){
		return this.outputWidth;
	}
}
