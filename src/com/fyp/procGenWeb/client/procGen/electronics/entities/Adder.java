/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;
import com.fyp.procGenWeb.shared.dto.entities.AdderDto;

/**
 * @author DINESH THANGAVEL
 *
 */
public class Adder extends Entity {

	/**
	 * @param name
	 * @throws ProcGenException 
	 */
	public Adder(String name,int noOfInputs,int signalBusWidth) throws ProcGenException {
		super(name);
		for(int i=0;i < noOfInputs;i++){
			this.addInput("input" + i, signalBusWidth);
		}
		
		// only one output for AND gate
		this.addOutput("output", signalBusWidth);
		this.addOutput("carry", 1);
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}

	/**
	 * Constructor for 2 input AND gate
	 * @param name
	 * @param input0BusWidth
	 * @param input1BusWidth
	 * @param withIo: specifies whether I/O has to be added by default
	 * @throws ProcGenException
	 */
	public Adder(String name,int noOfInputs,int input0BusWidth,int input1BusWidth,boolean withIo) throws ProcGenException{
		super(name);
		
//		assert(noOfInputs == 3);
		
		if (withIo) {
			this.addInput("input0", input0BusWidth);
			this.addInput("input1", input1BusWidth);
			
			if(noOfInputs == 3){
				this.addInput("input2", 1); // for carry_in
			}
			
			int outputBusWidth = Math.max(input0BusWidth, input1BusWidth);

			// only one output for ADDER gate
			this.addOutput("output", outputBusWidth);
			this.addOutput("carry", 1);
		}
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}
	
	/**
	 * @param id
	 * @param name
	 */
	private Adder(String id, String name) {
		super(id, name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void defaultBehaviour() throws ProcGenException {
		int sum = 0;
		SignalBus outputSignalBus = this.getOutputByName("output");
		for(SignalBus input:this.getInputPortList()){
			try{
				int inputValue = Integer.parseInt(input.getDigitalDisplayValue(),2);
				sum = sum + inputValue;
			}catch(NumberFormatException e){
				outputSignalBus.setValue(Signal.UNDEFINED);
				this.getOutputByName("carry").setValue(Signal.UNDEFINED);
				// TODO: consider displaying signal by signal
				return;
			}
		}
		
		String binaryString = Integer.toBinaryString(sum);
		String outputValue = "";
		String carryValue = "";
		if(binaryString.length() <= outputSignalBus.getBusWidth()){
		
			StringBuilder strb = new StringBuilder(binaryString);
			int diff = outputSignalBus.getBusWidth() - binaryString.length(); 
			for(int i=0;i<diff;i++){
				strb = strb.insert(0, '0');
			}
			outputValue = strb.toString();
			carryValue = String.valueOf('0');
			
		}
		else if(binaryString.length() == outputSignalBus.getBusWidth() + 1){
			outputValue = binaryString.substring(1);
			carryValue = binaryString.substring(1, 2);
		}
		else{
			throw new ProcGenException(Consts.ExceptionMessages.IMPOSSIBLE_STATE_FOR_ADDER);
		}
		
		outputSignalBus.setValue(SignalBus.getSignalArrayFromString(outputValue));
		this.getOutputByName("carry").setValue(SignalBus.getSignalArrayFromString(carryValue));
	}
	
	@Override
	public Object convertToHdl(HdlConverter hdlConverter){
		return hdlConverter.convertAdder(this);
	}

	@Override
	public AdderDto convertToEntityTransferObject(EntityTransferObject parent){
		AdderDto andDto = new AdderDto(this,parent);
		return andDto;
		
	}

}
