/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;
import com.fyp.procGenWeb.shared.dto.entities.AdderDto;
import com.fyp.procGenWeb.shared.dto.entities.SignalBusJointDto;

/**
 * @author DINESH THANGAVEL
 *	
 */

public class SignalBusJoint extends Entity{

	/**
	 * @throws ProcGenException 
	 * input1 msb and input0 lsb
	 */
	public SignalBusJoint(String gateName,int input0BusWidth,int input1BusWidth) throws ProcGenException {
		super(gateName);
		this.addInput("input0", input0BusWidth);
		this.addInput("input1", input1BusWidth);
		
		this.addOutput("output", input0BusWidth + input1BusWidth);
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}
	
	/*
	 *  Creates SignalBusJoint without input and output
	 */
	public SignalBusJoint(String gateName){
		super(gateName);
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}
	
	@Override
	public void defaultBehaviour() throws ProcGenException {
		String input0Value = this.getInputByName("input0").getDigitalDisplayValue();
		String input1Value = this.getInputByName("input1").getDigitalDisplayValue();
		
		String outputValue = input0Value + input1Value;
		
		this.getOutputByName("output").setValue(SignalBus.getSignalArrayFromString(outputValue));		
	}

	@Override
	public SignalBusJointDto convertToEntityTransferObject(EntityTransferObject parent){
		SignalBusJointDto signalBusJointDto = new SignalBusJointDto(this,parent);
		return signalBusJointDto;
	}
	
	@Override
	public Object convertToHdl(HdlConverter hdlConverter){
		return hdlConverter.convertSignalBusJoint(this);
	}
}
