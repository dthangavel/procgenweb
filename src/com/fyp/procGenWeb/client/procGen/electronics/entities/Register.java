/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.InvalidSignalException;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.simulation.ProjectSimulator;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;
import com.fyp.procGenWeb.shared.dto.entities.RegisterDto;

/**
 * @author DINESH THANGAVEL
 *
 */
public class Register extends Entity{
	
	float lastUpdateTime = -1;
	HashMap<String,String> outputToSetAfterFinalise = new HashMap<String,String>();
	
	public Register(String name,int width,String triggerType) throws ProcGenException {
		super(name);
		
		this.addInput("input0",width);
		this.addInput("enable", 1);
		this.addInput("clock", 1);
		
		this.addOutput("output", width);
		
		if(triggerType.compareToIgnoreCase("rising")==0){
			this.setEntityTriggerType(EntityTriggerType.RISING_EDGE_TRIGGERED);
		}
		
		else if(triggerType.compareToIgnoreCase("falling")==0){
			this.setEntityTriggerType(EntityTriggerType.FALLING_EDGE_TRIGGERED);
		}
		
		else{
			throw new ProcGenException(Consts.ExceptionMessages.INCORRECT_TRIGGER_TYPE);
		}
		
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}
	
	public Register(String name, int width, EntityTriggerType entityTriggerType) throws ProcGenException {
		super(name);
		
		this.addInput("input0",width);
		this.addInput("enable", 1);
		this.addInput("clock", 1);
		
		// only one output for Register
		this.addOutput("output", width);
		
		this.setEntityTriggerType(entityTriggerType);
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}

	public Register(String name, EntityTriggerType entityTriggerType) throws ProcGenException {
		super(name);
		
		this.setEntityTriggerType(entityTriggerType);
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}
	
	public void defaultBehaviour() throws InvalidSignalException{
		
		List<SignalBus> inputList = this.getInputPortList();
		assert(inputList.size()==3); // inputs should be the signal and the clock
		
		SignalBus clk = inputList.get(2);
		assert(clk.getBusWidth() == 1);
		
		SignalBus enable = inputList.get(1);
		assert(enable.getBusWidth() == 1);
		
		ProjectSimulator proSim = ElectronicsLogicFacade.getInstance().getActivePrjectInstance().getProjectSimulator();
		Float currentTime = proSim.getCurrentTime();
		
		if(clk.getValue()[0].equals(Signal.HIGH) && enable.getValue()[0].equals(Signal.HIGH) && (currentTime > lastUpdateTime||currentTime.equals(0))){
			SignalBus inputForRegister = this.getInputByName("input0");
			
			this.outputToSetAfterFinalise.put("output",inputForRegister.getDigitalDisplayValue());
			lastUpdateTime = currentTime;
		}
	}
	
	@Override
	public Object convertToHdl(HdlConverter hdlConverter){
		return hdlConverter.convertRegister(this);
	}
	
	@Override
	public RegisterDto convertToEntityTransferObject(EntityTransferObject parent){
		RegisterDto regDto = new RegisterDto(this,parent);
		return regDto;
		
	}
	
	@Override
	public void finaliseOutput() throws InvalidSignalException, ProcGenException{
		if (this.outputToSetAfterFinalise.containsKey("output")) {
			this.getOutputByName("output")
					.setValue(
							SignalBus
									.getSignalArrayFromString(this.outputToSetAfterFinalise
											.get("output")));
		}
	}
}
