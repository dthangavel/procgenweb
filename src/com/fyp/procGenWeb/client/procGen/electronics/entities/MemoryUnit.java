/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.entities;

import java.util.ArrayList;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;
import com.fyp.procGenWeb.shared.dto.entities.MemoryUnitDto;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class MemoryUnit extends Entity {

	List<String> storageUnit = new ArrayList<String>();
	MemoryUnitType memoryUnitType;
	int noOfExtraInOut = 0;

	/**
	 * @param name
	 */
	public MemoryUnit(String name, int noOfExtraInOut) {
		super(name);
		this.setHdlConversionType(HdlConversionType.InlineConversion);
		this.noOfExtraInOut = noOfExtraInOut;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param selectionBusWidth
	 * @param storageSize
	 * @param databusWidth
	 * @param memType
	 * @throws ProcGenException
	 */
	public MemoryUnit(String id, String name, int selectionBusWidth,
			int storageSize, int databusWidth, MemoryUnitType memType)
			throws ProcGenException {
		super(id, name);

		int sizeOfStorage = 1 << selectionBusWidth;

		if (storageSize > 0)
			sizeOfStorage = storageSize;

		if (memType.equals(MemoryUnitType.Rom)) {
			this.addInput("inputAddress", selectionBusWidth);
			this.addOutput("output", databusWidth);
			this.setMemoryUnitType(MemoryUnitType.Rom);
		} else if (memType.equals(MemoryUnitType.Ram)) {
			this.addInput("inputAddress", selectionBusWidth);
			this.addInput("writeAddress", selectionBusWidth);
			this.addInput("enable", 1);
			this.addInput("inputData", databusWidth);
			this.addOutput("output", databusWidth);
			this.setMemoryUnitType(MemoryUnitType.Ram);
		}

		// initialise RAM or ROM with undefined signal
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < databusWidth; i++) {
			str.append('Z');
		}

		for (int i = 0; i < storageSize; i++) {
			this.storageUnit.add(str.toString());
		}

		this.setHdlConversionType(HdlConversionType.InlineConversion);
		this.noOfExtraInOut = 0;
	}

	public MemoryUnit(String id, String name, int selectionBusWidth,
			int storageSize, int databusWidth, MemoryUnitType memType,
			int noOfExtraInOut) throws ProcGenException {
		this(id, name, selectionBusWidth, storageSize, databusWidth, memType);
		this.noOfExtraInOut = noOfExtraInOut;
		for (int i = 0; i < noOfExtraInOut; i++) {
			this.addInput("inputAddress" + (i + 1), selectionBusWidth);
			this.addOutput("output" + (i + 1), databusWidth);
		}
	}

	public void setMemoryUnitType(MemoryUnitType typeToSet) {
		this.memoryUnitType = typeToSet;
	}

	public MemoryUnitType getMemoryUnitType() {
		return this.memoryUnitType;
	}

	public int getSizeOfStorage() {
		return this.storageUnit.size();
	}

	public int getSizeOfInput() {
		return this.getInputPortList().get(0).getBusWidth();
	}

	public void programMemoryUnit(List<String> signalBusToProgram) {
		this.storageUnit.addAll(signalBusToProgram);
	}

	public List<String> getStorage() {
		return this.storageUnit;
	}

	@Override
	public void defaultBehaviour() throws ProcGenException {
		List<SignalBus> inputList = this.getInputPortList();
		List<SignalBus> outputList = this.getOutputPortList();

		// assert(outputList.size() == 1);

		// Write first before read
		if (this.memoryUnitType == MemoryUnitType.Ram) {
			SignalBus enable = this.getInputByName("enable");
			// only when enable is high you do the writing
			if (enable.getValue()[0] == Signal.HIGH) {
				SignalBus writeAddress = this.getInputByName("writeAddress");
				String digitalValue = writeAddress.getDigitalDisplayValue();
				int index = 0;
				try {
					index = Integer.parseInt(digitalValue, 2);
				} catch (NumberFormatException e) {
					index = 0; // mapping X state to 0;
				}

				SignalBus inputData = this.getInputByName("inputData");

				this.storageUnit.set(index, inputData.getDigitalDisplayValue());
			}
		}
		
		for (int i = 0; i < this.noOfExtraInOut + 1; i++) {
			SignalBus inputAddress = (i == 0) ? this
					.getInputByName("inputAddress") : this
					.getInputByName("inputAddress" + i);
			String digitalValue = inputAddress.getDigitalDisplayValue();
			int index = 0;
			try {
				index = Integer.parseInt(digitalValue, 2);
			} catch (NumberFormatException e) {
				// not throwing exception because it is possible that input is initially unitialised
				//throw new ProcGenException(Consts.ExceptionMessages.INPUT_IS_X);
			}

			SignalBus output = (i == 0) ? this.getOutputByName("output") : this
					.getOutputByName("output" + i);

			if (index >= this.storageUnit.size()) {
				throw new ProcGenException(
						Consts.ExceptionMessages.CANNOT_ACCESS_BEYOND_STORAGE_SIZE);
			}

			String valueToSet = this.storageUnit.get(index);
			output.setValue(SignalBus.getSignalArrayFromString(valueToSet));

		}
	}

	@Override
	public Object convertToHdl(HdlConverter hdlConverter) {
		return hdlConverter.convertMemoryUnit(this);
	}

	@Override
	public MemoryUnitDto convertToEntityTransferObject(
			EntityTransferObject parent) {
		MemoryUnitDto newMemUnit = new MemoryUnitDto(this, parent);
		return newMemUnit;
	}

	public enum MemoryUnitType {
		Ram, Rom
	}

	public int getNumberOfExtraInOut() {
		return this.noOfExtraInOut;
	}

	public int getOutputBusWidth() {
		return this.getOutputByName("output").getBusWidth();
	}
}
