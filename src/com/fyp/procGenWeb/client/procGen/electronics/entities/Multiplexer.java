/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.entities;

import java.util.HashMap;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.logicHelper.InvalidSignalException;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;
import com.fyp.procGenWeb.shared.dto.entities.MultiplexerDto;

/**
 * @author DINESH THANGAVEL
 *
 */
public class Multiplexer extends Entity{

	// key is the digital representation of the the selectionSignal
	HashMap<String,String> inputOutputMapping = new HashMap<String,String>();
	public Multiplexer(String name,int widthOfSelectionInput,int widthOfInputSignal) throws ProcGenException {
		super(name);
		
		// 2^widthOfSelectionInput
		int noOfInputs = 1<<widthOfSelectionInput;
		
		this.addInput("selectionInput", widthOfSelectionInput);
		
		for(int i =0;i<noOfInputs;i++){
			this.addInput("input" + i, widthOfInputSignal);
		}
		
		// only one output for MUX
		this.addOutput("output", widthOfInputSignal);

		this.setHdlConversionType(HdlConversionType.InlineConversion);

	}
	
	public Multiplexer(String name){
		super(name);
		this.setHdlConversionType(HdlConversionType.InlineConversion);
	}
	
	public void programMux(String selectionSignalBusValue, String inputSignalName){
		if(this.getInputPortNames().contains(inputSignalName)){
			inputOutputMapping.put(selectionSignalBusValue, inputSignalName);
		}
	}
	
	public HashMap<String,String> getMuxMapping(){
		return this.inputOutputMapping;
	}
	
	public void setMuxMapping(HashMap<String,String> mappingToSet){
		this.inputOutputMapping = mappingToSet;
	}
	
	@Override
	public void defaultBehaviour() throws InvalidSignalException{
		SignalBus selectionSignal = this.getInputByName("selectionInput");
		String selectionSignalValue = selectionSignal.getDigitalDisplayValue();
		
		if (this.inputOutputMapping.containsKey(selectionSignalValue)) {
			String inputSignalToAssign = this.inputOutputMapping
					.get(selectionSignalValue);

			SignalBus inputSignal = this.getInputByName(inputSignalToAssign);

			assert (this.getOutputPortList().size() == 1);

			this.getOutputPortList().get(0).setValue(inputSignal.getValue());
		}
	}

	@Override
	public Object convertToHdl(HdlConverter hdlConverter){
		return hdlConverter.convertMux(this);
	}
	
	@Override
	public MultiplexerDto convertToEntityTransferObject(EntityTransferObject parent){
		MultiplexerDto multDto = new  MultiplexerDto(this,parent);
		return multDto;
		
	}

}
