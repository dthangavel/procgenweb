/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.electronics.entities;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.EntityTransferObject;
import com.fyp.procGenWeb.shared.dto.entities.ComparatorDto;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class Comparator extends Entity {

	Type compType;

	public Comparator(String name, Type type, int inputBusWidth)
			throws ProcGenException {
		this(name, type);
		this.addInput("input0", inputBusWidth);
		this.addInput("input1", inputBusWidth);
		this.addOutput("output", 1);
	}

	/**
	 * Constructor that does not add input. Used by data transfer objects
	 * 
	 * @param name
	 * @param type
	 */
	public Comparator(String name, Type type) {
		super(name);
		this.compType = type;
	}

	public enum Type {
		Equals, LessThan, GreaterThan, LessThanEquals, GreaterThanEquals, NotEquals;
		Type(){}
	}

	@Override
	public void defaultBehaviour() {
		SignalBus input0 = this.getInputByName("input0");
		SignalBus input1 = this.getInputByName("input0");

		int input0Val = Integer.parseInt(input0.getDigitalDisplayValue(), 2);
		int input1Val = Integer.parseInt(input1.getDigitalDisplayValue(), 2);

		boolean result = computeResultBasedOnInput(input0Val, input1Val);
		if (result) {
			this.getOutputByName("output").setValue(Signal.HIGH);
		} else {
			this.getOutputByName("output").setValue(Signal.LOW);
		}
	}

	private boolean computeResultBasedOnInput(int input0Val, int input1Val) {
		if (this.compType == Type.Equals) {
			return input0Val == input1Val;
		}

		if (this.compType == Type.GreaterThan) {
			return input0Val > input1Val;
		}

		if (this.compType == Type.GreaterThanEquals) {
			return input0Val >= input1Val;
		}

		if (this.compType == Type.LessThan) {
			return input0Val < input1Val;
		}

		if (this.compType == Type.LessThanEquals) {
			return input0Val <= input1Val;
		}

		if (this.compType == Type.LessThanEquals) {
			return input0Val <= input1Val;
		}

		if (this.compType == Type.NotEquals) {
			return input0Val != input1Val;
		}

		return false;
	}

	public Type getType() {
		return this.compType;
	}

	@Override
	public Object convertToHdl(HdlConverter hdlConverter) {
		return hdlConverter.convertComparator(this); // TODO: yet to implement
	}

	@Override
	public ComparatorDto convertToEntityTransferObject(
			EntityTransferObject parent) {
		ComparatorDto comparatorDto = new ComparatorDto(this, parent);
		return comparatorDto;
	}
}
