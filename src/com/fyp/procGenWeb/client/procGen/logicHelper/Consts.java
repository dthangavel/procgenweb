/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logicHelper;

/**
 * @author DINESH THANGAVEL
 *
 */
public class Consts {

	public static class ConsoleUIConstants {
		public static final String PROMPT_ENTER_COMMAND = "Please Enter the Command:";
		public static final String ERROR = "ERROR:";
		public static final String SUCCESS = "SUCCESS:";
		public static final String NEW_ENTITY_HEADER = "New Entity Details:";
		public static final String PROMPT_NEW_ENTITY_NAME = " Please Enter the name of the entity";
		public static final String PROMPT_NEW_ENTITY_PARENT_ID = "Please Enter the parent ID of the entity"; // has to be removed for GUI
		public static final String PROMPT_NEW_ENTITY_NO_OF_INPUT_SIGNALS = "Please enter the number of inputs";
		public static final String PROMPT_NEW_ENTITY_NO_OF_OUTPUT_SIGNALS = "Please enter the number of outputs";
		public static final String PROMPT_NEW_ENTITY_INPUT_SIGNAL_NAME = "Please enter input port name";
		public static final String PROMPT_NEW_ENTITY_OUTPUT_SIGNAL_NAME = "Please enter output port name";
		public static final String PRINT_NOT_A_NUMBER = "It is not a number";
		public static final String PROMPT_NEW_INPUT_SIGNAL_BUS_WIDTH = "Enter the bus width size for input signal";
		public static final String PROMPT_NEW_OUTPUT_SIGNAL_BUS_WIDTH = "Enter the bus width size for output signal";
		public static final String SEPARATOR = "--------------------------------------------------------------------";
		public static final String ENTITY_NAME = "Entity Name:";
		public static final String ENTITY_ID = "Entity Id:";
		public static final String PARENT_ID = "Parent Id:";
		public static final String INPUT_PORTS = "Input Ports:";
		public static final String OUTPUT_PORTS = "Output Ports:";
		public static final String NO_ENTITY_IN_PROJECT = "No Entity present in the project";
		public static final String SPECIFY_INPUT_FILE = "Please specify file name";
		public static int NEW_ENTITY_ARGS_COUNT = 6;
	}
	
	public static class CommandInputText{
		public static final String UNDO = "undo";
		public static final String NEW_ENTITY = "new_entity";
		public static final String NEW_PROJECT = "new_project";
		public static final String DISPLAY_ENTITIES = "display_entities";
		public static final String IMPORT_VHDL = "import_vhdl";
		public static final String CONNECT = "connect";
		public static final String CLOSE_PROJECT = "close_project";
		public static final String NEW_AND_GATE = "new_and_gate";
		public static final String SIMULATE = "simulate";
		public static final String NEW_INPUT_SIMULATOR = "new_input_simulator";
		public static final String CHANGE_INPUT_FOR_SIMULATION = "change_sim_input";
		public static final String ADD_INPUT_STIMULUS = "add_input_stimulus";
		public static final String NEW_OR_GATE = "new_or_gate";
		public static final String NEW_REGISTER_COMMAND = "new_register";
		public static final String GET_SIGNALBUS_VALUE = "get_signalbus_value";
		public static final String CONNECT_TO_CLOCK = "connect_to_clock";
		public static final String EXPORT_TO_HDL = "export_to_hdl";
		public static final String NEW_MUX = "new_mux";
		public static final String ADD_SIGNAL_DEBUG_POINT = "new_signalbus_debug";
		public static final String NEW_MEMORY = "new_memory_unit";
		public static final String INSERT_CONTENTS_IN_MEM = "insert_contents_in_mem";
		public static final String CONNECT_TO_CONSTANT = "connect_to_constant";
		public static final String NEW_ADDER = "new_adder";
		public static final String SET_TRIGGER_TYPE = "set_trigger_type";
		public static final String INITIALISE_SIGNAL_BUS = "initialise_signal_bus";
		public static final String NEW_SIGNAL_BUS_SPLITTER = "new_signal_bus_splitter";
		public static final String NEW_SIGNAL_BUS_JOINT = "new_signal_bus_joint";
	}
	
	public static class CommandResults{
		public static final String UNDO_UNAVAILABLE = "UNDO Unavailable";
		public static final String SUCCESS_NEW_PROJECT_CREATION = "Successfully created new project ";
		public static final String CREATE_PROJECT_FIRST = "Please create project first";
		public static final String SUCCESS_NEW_ENTITY_CREATION = "Successfully created new entity with Id ";
		public static final String COMMAND_EXECUTED_SUCCESSFULLY = "Command Executed Sucessfully";
		public static final String SUCCESS_NEW_CONNECTION_CREATION = "Successfully created new Connection between ";
		public static final String SUCCESS_CLOSED_PROJECT = "Successfully closed the project ";
		public static final String SUCCESS_NEW_AND_CREATION = "Successfully created new AND gate with Id ";
		public static final String SUCCESS_SIMULATION_COMPLETED = "Succesfully simulated project";
		public static final String SUCCESS_NEW_INPUT_SIMULATOR = "Successfully created new input Simulator";
		public static final String INPUT_SIM_NOT_CONNECTED = "Please check output connection of input simulator ";
		public static final String ERROR_SIM_NOT_SUCCESSFUL = "Error occured while simulating";
		public static final String SUCCESS_ADDED_STIMULI_TO_SIMULATOR = "Successfully added stimuli to simulator";
		public static final String SUCCESS_NEW_OR_CREATION = "Successfully created new OR gate with Id ";
		public static final String SUCCESS_CHANGED_SIMULATOR_VALUE = "Successfully changed simulator value";
		public static final String CLOCK = "Clock-";
		public static final String SUCCESS_NEW_REGISTER_CREATION = "Successfully created new Register with Id ";
		public static final String SUCCESS_EXPORT_TO_VHDL = "Successfully exportd to VHDL";
		public static final String SUCCESS_NEW_MUX_CREATION = "Successfully created a new multiplexer";
		public static final String SUCCESS_ADDED_SIGNALBUS_TO_WATCH_LIST = "Successfully added signal bus to watch list";
		public static final String SUCCESS_NEW_MEM_UNIT_CREATION = "Successfully created a new memory unit with Id ";
		public static final String SUCCESS_PROGRAMMED_MEMORY = "Programmed memory unit";
		public static final String SUCCESS_CONSTANT_CONNECTION = "Successfully connected constant to signal bus ";
		public static final String SUCCESS_NEW_ADDER_CREATION = "Successfully created new Adder with Id ";
		public static final String SUCCESS_SET_ENTITY_TRIGGER_TYPE = "Successfully set entity trigger type";
		public static final String SUCCESS_INTIALISED_SIGNAL_BUS = "Successfully initialised signal bus";
		public static final String SUCCESS_NEW_SIGNAL_BUS_SPLITTER_CREATION = "Successfully created new signal bus splitter with Id ";
		public static final String SUCCESS_NEW_SIGNAL_BUS_JOINT_CREATION = "Successfully created new signal bus joint with Id ";
	}
	
	public static class ExceptionMessages{
		public static final String COMMAND_NOT_FOUND = "The command could not be found";
		public static final String UNEQUAL_LENGTH_SIGNAL_ASSIGNMENT = "The signal lengths do not match for assignment";
		public static final String INPUT_ALREADY_PRESENT = "Input with same name already present";
		public static final String OUTPUT_ALREADY_PRESENT = "Output with same name already present";
		public static final String SIGNAL_NOT_RECOGNISED = "Unable to recognise type of signal";
		public static final String CANNOT_CONNECT_EXCEPTION = "Unable to make a connection";
		public static final String CONNECTION_CREATION_ERROR= "New Entity is already present";
		public static final String ERROR_CREATING_ENTITY = "Unable to create entity";
		public static final String ERROR_CREATING_CONNECTION = "Unable to create connection";
		public static final String ERROR_CREATING_INPUT_SIM = "Unable to create input simulator";
		public static final String ENTITY_NOT_FOUND ="Entity with given Id could not be found";
		public static final String INPUT_NOT_RECOGNISED = "The Input could not be recognised";
		public static final String INCORRECT_TRIGGER_TYPE = "The trigger type could not be recognised";
		public static final String UNABLE_TO_WRITE_TO_FILE = "Unable to write to file ";
		public static final String UNCONNECTED_INPUT_FOUND_IN = " Unconnected Signal/s exists in entity ";
		public static final String INCORRECT_NO_OF_ARGUMENTS = "Incorrect Number of Arguments";
		public static final String CANNOT_FIND_CODE_EDITOR = "Cannot find the code editor";
		public static final String EDITOR_NOT_INSTANTIATED = "Editor not instantiated";
		public static final String CANNOT_BE_NULL = "Input cannot be null";
		public static final String ENTITY_NOT_INSTANCE_OF_MEMORY = "Entity is not a memory unit";
		public static final String UNABLE_TO_PARSE_VHDL = "Unable to parse vhdl code";
		public static final String ERROR_CONNECTING_CONSTANT = "Unable to connect constant";
		public static final String INDEX_NOT_FOUND = "Index not found";
		public static final String CIRCUIT_OSCILLATING = "Circuit Oscillating";
		public static final String IMPOSSIBLE_STATE_FOR_ADDER = "Impossible state for adder";
		public static final String TWO_INPUT_AND_GATE_SUPPORTED = "Only two input AndGate supported";
		public static final String INPUT_IS_X = "Input is X";
		public static final String CANNOT_ACCESS_BEYOND_STORAGE_SIZE = "Cannot access beyond storage size";
		public static final String INVALID_STARTING_INDEX = "Invalid starting index";
		public static final String UNABLE_TO_PARSE_BINARY_EXP = "Unable to identify binary expression";
		public static final String NOT_IMPLEMENTED = "Not implemented yet";
		public static final String CANNOT_BE_EMPTY = "Cannot be empty";
		public static final String NO_VHDL_ENTITY_PRESENT_FOR_IMPORT = "No Vhdl File present for import";
		public static String IMPROPER_PARSING = "Improper parsing exception";
	}
	
	public static class ErrorCodes{
		public static final String SIGNAL_NOT_RECOGNISED = "100 signal not recognised";
		public static final String UNEQUAL_LENGTH_SIGNAL_ASSIGNMENT = "101 unequal length signal assignment";
		public static final String SIGNAL_INDEX_NOT_FOUND = "102 signal index not found";
		public static final String ENTITY_ALREADY_PRESENT = "103 Entity already present";
		public static final String INVALID_COMMAND_ARGUMENT = "104 Invalid command argument";
		
	}
	
	public static class Misc{
		public static final String VHDL = "vhdl";
	}
}
