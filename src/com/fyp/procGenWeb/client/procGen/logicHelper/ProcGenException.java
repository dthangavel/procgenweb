/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logicHelper;

import java.io.Serializable;


/**
 * @author DINESH THANGAVEL
 *
 */

public class ProcGenException extends Exception implements Serializable{
	public String errorCode;
	
	public ProcGenException() {
		super();
	}

	public ProcGenException(String message) {
		super(message);
	}

	public ProcGenException(String errorcode,	String message) {
		super(message + "-"+ errorcode);
		errorCode = errorcode;
	}

//	public static String toStringWithStackTrace(Throwable e) {
//		StringWriter sw = new StringWriter();
//		e.printStackTrace(new PrintWriter(sw));
//		return "\n" + sw.toString();
//	}	
}
