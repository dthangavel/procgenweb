package com.fyp.procGenWeb.client.procGen.logicHelper;

import com.fyp.procGenWeb.client.display.presenter.SignalBusDebuggerPresenter;
import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusPropertyChangeEvent;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusPropertyChangeListener;
import com.fyp.procGenWeb.client.procGen.simulation.ProjectSimulator;

public class SignalBusDebugObserver implements SignalBusPropertyChangeListener {

	ProjectSimulator hostSimulator; // signal observer keeps track of hostSimulator so as to synchronize time with the simulator
	String nameOfObserver; // to help in debugging

	SignalBusDebuggerPresenter presenter = null;
	String entityId;
	String signalBusName;
		
	public SignalBusDebugObserver(String entityId, SignalBus signalBusToDebug,
			ProjectSimulator projectSimulator, SignalBusDebuggerPresenter presenter,String name) {

		this.hostSimulator = projectSimulator;
		signalBusToDebug.addChangeListener(this);
		this.nameOfObserver = name;
		this.presenter = presenter;	
		this.entityId = entityId;
		this.signalBusName = signalBusToDebug.getName();
	}

	@Override
	public void propertyChange(SignalBusPropertyChangeEvent evt) {

		float currentTime = hostSimulator.getCurrentTime();
		Signal[] oldValue = (Signal[])evt.getOldValue();
		Signal[] newValue = (Signal[])evt.getNewValue();

		SignalBus oldTmp = new SignalBus("oldTmp",oldValue.length);
		SignalBus newTmp = new SignalBus("newTmp",newValue.length);
		
		try {
			oldTmp.setValue(oldValue);
			newTmp.setValue(newValue);
		} catch (InvalidSignalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if(presenter != null){
			this.presenter.updateSignalBusValue(this.signalBusName,this.entityId, currentTime,oldTmp.getDigitalDisplayValue(),newTmp.getDigitalDisplayValue());
		}
	}

}
