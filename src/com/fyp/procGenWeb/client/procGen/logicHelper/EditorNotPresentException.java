/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logicHelper;


/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class EditorNotPresentException extends ProcGenException {

	public EditorNotPresentException(String editorNotInstantiated) {
		super(editorNotInstantiated);
	}
	
}
