/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.simulation;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity.EntityTriggerType;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusPropertyChangeEvent;
import com.fyp.procGenWeb.client.procGen.logicHelper.InvalidSignalException;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class EntitySimulator extends Simulator{

	Entity hostEntity = null;
	
	public EntitySimulator(Entity hostEntity){
		this.hostEntity = hostEntity;
	}
	
	public void processInputChange(SignalBusPropertyChangeEvent evt) throws ProcGenException{
		this.runSimulation();
	}
	
	public void runSimulation() throws ProcGenException {
		// If no sub-components execute the behaviour of the entity
		if(hostEntity.getChildEntityList().size()==0){
			hostEntity.defaultBehaviour();
		}
		
	}

	public boolean isClocked(){
		if(this.hostEntity.getEntityTriggerType() == null || this.hostEntity.getEntityTriggerType() == EntityTriggerType.NON_CLOCKED){
			return false;
		}
		
		return true;
	}

	/**
	 * This method is used only for sequential entities
	 * @throws ProcGenException 
	 * @throws InvalidSignalException 
	 */
	public void finalizeSignalOutputValues() throws InvalidSignalException, ProcGenException {
		this.hostEntity.finaliseOutput();
	}
	
}

