/**
 * This class is the trigger that has to be connected to the input of entity. 
 */
package com.fyp.procGenWeb.client.procGen.simulation;

import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;

/**
 * @author DINESH THANGAVEL
 *
 */
public class InputSimulator extends SignalBus{

	public InputSimulator(String name, int busWidth) {
		super(name, busWidth);
	}
	
}
