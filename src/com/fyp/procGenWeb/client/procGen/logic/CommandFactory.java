/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic;

import java.util.HashMap;

import com.fyp.procGenWeb.client.procGen.logic.commandslist.AddInputStimulusCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.AddSignalBusDebugPoint;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ChangeInputSimulatorValueCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.CloseProjectCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectToClockCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectToConstant;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ExportHdlCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.GetSignalBusValueCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.InitialiseSignalBusCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.InsertContentsInMemory;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewAdderCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewAndGateCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewEntityCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewInputSimulatorCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewMemoryUnitCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewMultiplexerCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewOrGateCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewProjectCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewRegisterCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewSignalBusJoint;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.NewSignalBusSplitter;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.SetEntityTriggerType;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.StartSimulation;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.UndoCommand;
import com.fyp.procGenWeb.client.procGen.logicHelper.CommandNotFoundException;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts.CommandInputText;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts.ExceptionMessages;


/**
 * @author DINESH THANGAVEL
 * 
 */
public class CommandFactory {

	private HashMap<String, Command> cmdMapper = new HashMap<String, Command>();

	CommandFactory() {
		// Register the commands available

		cmdMapper.put(CommandInputText.UNDO, (Command) new UndoCommand());
		cmdMapper.put(CommandInputText.NEW_PROJECT, new NewProjectCommand());
		cmdMapper.put(CommandInputText.NEW_ENTITY,  new NewEntityCommand());
//		cmdMapper.put(CommandInputText.DISPLAY_ENTITIES, (Command) new DisplayAllEntities());
//		cmdMapper.put(CommandInputText.IMPORT_VHDL, (Command) new ImportVhdlCode());
		cmdMapper.put(CommandInputText.CONNECT, new ConnectCommand());
		cmdMapper.put(CommandInputText.CLOSE_PROJECT, new CloseProjectCommand());
		cmdMapper.put(CommandInputText.NEW_AND_GATE, new NewAndGateCommand());
		cmdMapper.put(CommandInputText.NEW_OR_GATE, new NewOrGateCommand());
		cmdMapper.put(CommandInputText.NEW_INPUT_SIMULATOR,  new NewInputSimulatorCommand());
		cmdMapper.put(CommandInputText.SIMULATE, new StartSimulation());
		cmdMapper.put(CommandInputText.CHANGE_INPUT_FOR_SIMULATION, new ChangeInputSimulatorValueCommand());
		cmdMapper.put(CommandInputText.ADD_INPUT_STIMULUS, new AddInputStimulusCommand());
		cmdMapper.put(CommandInputText.NEW_REGISTER_COMMAND, new NewRegisterCommand());
		cmdMapper.put(CommandInputText.GET_SIGNALBUS_VALUE, (Command) new GetSignalBusValueCommand());
		cmdMapper.put(CommandInputText.CONNECT_TO_CLOCK, new ConnectToClockCommand());
		cmdMapper.put(CommandInputText.EXPORT_TO_HDL, (Command) new ExportHdlCommand());
		cmdMapper.put(CommandInputText.NEW_MUX, new NewMultiplexerCommand());
		cmdMapper.put(CommandInputText.ADD_SIGNAL_DEBUG_POINT, new AddSignalBusDebugPoint());
		cmdMapper.put(CommandInputText.NEW_MEMORY, new NewMemoryUnitCommand());
		cmdMapper.put(CommandInputText.INSERT_CONTENTS_IN_MEM, new InsertContentsInMemory());
		cmdMapper.put(CommandInputText.CONNECT_TO_CONSTANT, new ConnectToConstant());
		cmdMapper.put(CommandInputText.NEW_ADDER, new NewAdderCommand());
		cmdMapper.put(CommandInputText.SET_TRIGGER_TYPE, new SetEntityTriggerType());
		cmdMapper.put(CommandInputText.INITIALISE_SIGNAL_BUS, new InitialiseSignalBusCommand());
		cmdMapper.put(CommandInputText.NEW_SIGNAL_BUS_SPLITTER, new NewSignalBusSplitter());
		cmdMapper.put(CommandInputText.NEW_SIGNAL_BUS_JOINT, new NewSignalBusJoint());
	}

	public Command makeCommand(String userInputText)
			throws CommandNotFoundException {

		assert (userInputText.length() > 0);

		if (cmdMapper.containsKey(userInputText)) {
			return cmdMapper.get(userInputText);
		}

		// if not command found throw exception
		throw new CommandNotFoundException(ExceptionMessages.COMMAND_NOT_FOUND + " " + userInputText);
	}
}
