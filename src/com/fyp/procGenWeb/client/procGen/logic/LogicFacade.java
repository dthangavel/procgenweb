/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic;

import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class LogicFacade {
	CommandFactory cmdGenerator = new CommandFactory();
	CommandManager cmdManager = new CommandManager();
	
	public String processInput(String userInput) throws ProcGenException{
		
		if(userInput.length() == 0)
			return "";
		
		// TODO: comment identified when only in the beginning
		if(userInput.length() > 2 && userInput.substring(0, 2).equals("//")){
			return "";
		}
		
		String userInputParts[] = userInput.trim().split("\\s+");
		String commandAndArguments[] = userInput.split(userInputParts[0]);
		String arguments = "";
		if(commandAndArguments.length > 1){
			arguments = commandAndArguments[1].trim();
		}
		
		Command cmdToExecute = cmdGenerator.makeCommand(userInputParts[0].toLowerCase());
		String feedbackToUI = cmdManager.executeCommand(cmdToExecute,arguments);
		return userInput + ":-" + feedbackToUI;
	}
	
}
