/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Connection;
import com.fyp.procGenWeb.client.procGen.electronics.core.ConnectionType;
import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusObserver;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusUniqueLocater;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity.EntityTriggerType;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.simulation.ProjectSimulator;

/**
 * @author DINESH THANGAVEL
 *
 */
public class ConnectToClockCommand implements Command{

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		Project projectInstance = activeAppDetails.getActivePrjectInstance();
		if(projectInstance==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		assert(splitArguments.length == 2);
		
		String entityId = splitArguments[0];
		String signalName = splitArguments[1];
		
		return establishClockConnection(projectInstance, entityId, signalName);
	}

	/**
	 * @param projectInstance
	 * @param entityId
	 * @param signalName
	 * @return
	 * @throws ProcGenException
	 */
	public String establishClockConnection(Project projectInstance,
			String entityId, String signalName) throws ProcGenException {
		EntityManager em = projectInstance.getEntityManager();
		Entity entityReqd = em.getEntityById(entityId);
		
		if(entityReqd == null)
			throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
		
		SignalBus reqdSignalBus = entityReqd.getInputByName(signalName); 
		
		if(reqdSignalBus == null){ // if not in input check output
			reqdSignalBus = entityReqd.getOutputByName(signalName);
		}
		
		if(reqdSignalBus == null)
			throw new ProcGenException(Consts.ExceptionMessages.SIGNAL_NOT_RECOGNISED + ": " + signalName);
		
		ProjectSimulator prjSim = projectInstance.getProjectSimulator();
		
		HashMap<SignalBus,SignalBusObserver> prjClkObserverMap = prjSim.getSignalObserverMap();
		
		SignalBusObserver prjClkTrigger = null;
		EntityTriggerType destinationEntityTriggerType = entityReqd.getEntityTriggerType();
		
		Connection newConnectionToAdd = null;
		
		if(destinationEntityTriggerType.equals(EntityTriggerType.RISING_EDGE_TRIGGERED) ){
			if (prjClkObserverMap.containsKey(prjSim.getRisingEdgeTriggerSignal())) {

				prjClkTrigger = prjClkObserverMap.get(prjSim
						.getRisingEdgeTriggerSignal());
			}
			
			else{
				prjClkTrigger = new SignalBusObserver(prjSim.getRisingEdgeTriggerSignal(),prjSim,"project-clk-observer");
				prjClkObserverMap.put(prjSim.getRisingEdgeTriggerSignal(), prjClkTrigger);
			}
			
			newConnectionToAdd = new Connection("",entityReqd.getId(),prjSim.getRisingEdgeTriggerSignal(),reqdSignalBus,ConnectionType.DIRECT_CONNECTION);
		}
		
		else if(destinationEntityTriggerType.equals(EntityTriggerType.FALLING_EDGE_TRIGGERED)){
			if (prjClkObserverMap.containsKey(prjSim.getFallingEdgeTriggerSignal())) {

				prjClkTrigger = prjClkObserverMap.get(prjSim
						.getFallingEdgeTriggerSignal());
			}
			
			else{
				prjClkTrigger = new SignalBusObserver(prjSim.getFallingEdgeTriggerSignal(),prjSim,"project-clk-observer");
				prjClkObserverMap.put(prjSim
						.getFallingEdgeTriggerSignal(), prjClkTrigger);
			}
			
			newConnectionToAdd = new Connection("",entityReqd.getId(),prjSim.getFallingEdgeTriggerSignal(),reqdSignalBus,ConnectionType.DIRECT_CONNECTION);
		}
		
		else{
				throw new ProcGenException(Consts.ExceptionMessages.INCORRECT_TRIGGER_TYPE + "for entity " + entityReqd.getName() + ":" + entityReqd.getId());
		}

			
			prjClkTrigger.addEntitySimulatorListener(entityReqd.getEntitySimulator());
			List<Connection> connectionsUpdatedByClock = prjClkTrigger.getConnectionsUpdatedByObserver();
			
			
			if(connectionsUpdatedByClock == null){
				connectionsUpdatedByClock = new ArrayList<Connection>();
			}
			
			connectionsUpdatedByClock.add(newConnectionToAdd);
			prjClkTrigger.updateConnectionList(connectionsUpdatedByClock);
			
			projectInstance.getProjectSimulator().addNewSignalBusConnectionToProjectClock(new SignalBusUniqueLocater(entityId,signalName));
			
		return Consts.CommandResults.SUCCESS_NEW_CONNECTION_CREATION + Consts.CommandResults.CLOCK + entityReqd.getName() + ":" + reqdSignalBus.getName();
	}

}
