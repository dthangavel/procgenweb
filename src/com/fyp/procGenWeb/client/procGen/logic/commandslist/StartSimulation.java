/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.simulation.ProjectSimulator;

/**
 * @author DINESH THANGAVEL
 *
 */
/*
 * arguments isDegubEnabled,isAutomaticInputChangeEnabled,timeToSimulate
 * 
 */
public class StartSimulation implements Command{
	
	@Override
	public String execute(String arguments) throws ProcGenException {

		boolean isDebugEnabled = false;
		boolean isAutomaticInputChangeEnabled = false;
		float timeToSimulate = 1000;
		float clockPeriod = 200;
		
		String[] splitArguments = arguments.split("\\s+");
		
		if(splitArguments.length > 0 ){
			try{
				int debugEnable = Integer.parseInt(splitArguments[0]);
				if(debugEnable == 1){
					isDebugEnabled = true;
				}
			}
			catch(NumberFormatException e){
				throw e;
			}
		}
		
		if(splitArguments.length > 1 ){
			try{
				int automatedInputEnable = Integer.parseInt(splitArguments[1]);
				if(automatedInputEnable == 1){
					isAutomaticInputChangeEnabled = true;
				}
			}
			catch(NumberFormatException e){
				throw e;
			}
		}
		
		if(splitArguments.length > 2 ){
			try{
				timeToSimulate = Float.parseFloat(splitArguments[2]);
			}
			catch(NumberFormatException e){
				throw e;
			}
		}
		
		if(splitArguments.length > 3 ){
			try{
				clockPeriod = Float.parseFloat(splitArguments[3]);
			}
			catch(NumberFormatException e){
				throw e;
			}
		}
		
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		Project activeProjectInstance = activeAppDetails.getActivePrjectInstance();
		if(activeProjectInstance==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}

//		initialiseConnectionTriggersInProject(activeProjectInstance);
		ProjectSimulator projectSim = activeProjectInstance.getProjectSimulator();
		
		
		projectSim.runSimulation(timeToSimulate, clockPeriod,isDebugEnabled,isAutomaticInputChangeEnabled);
		return Consts.CommandResults.SUCCESS_SIMULATION_COMPLETED;
	}
	
//	private void initialiseConnectionTriggersInProject(Project activeProjectInstance){
//		
//		ProjectConnectionManager pcm = activeProjectInstance.getConnectionManager();
//		EntityManager em = activeProjectInstance.getEntityManager();
//		List<Entity> baseEntityList = em.getBaseEntities();
//		
//		for(Entity baseEntity : baseEntityList){
//			HashMap<String,List<Connection>> entityConnectionDetails = pcm.getConnectionForEntity(baseEntity.getId());
//			List<SignalBus> signalsInEntity = baseEntity.getOutputPortList();
//			for(SignalBus outputSignal: signalsInEntity){
//				SignalBusObserver busMonitor = new SignalBusObserver(outputSignal,activeProjectInstance.getProjectSimulator(),baseEntity.getId()+"-"+outputSignal.getName());
//				
//				List<Connection> connectionListForOutputSignal= entityConnectionDetails.get(outputSignal.getName());
//				if(connectionListForOutputSignal!=null){
//					busMonitor.addConnectionListToUpdate(connectionListForOutputSignal);
//				for(Connection signalConnection:connectionListForOutputSignal){
//					String destinationEntityId = signalConnection.getDestinationEntityId();
//					Entity destinationEntity = em.getEntityById(destinationEntityId);
//					
//					if(destinationEntity != null){
//						// TODO: take care of case where one output is connected to multiple inputs
//						busMonitor.addEntitySimulatorListener(destinationEntity.getEntitySimulator());
//					}
//				}
//				}
//				activeProjectInstance.getProjectSimulator().getSignalObserverMap().add(busMonitor);
//			}
//		}
//	}

}
