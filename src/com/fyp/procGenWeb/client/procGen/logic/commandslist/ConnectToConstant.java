/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ConnectionManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class ConnectToConstant implements Command{

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		Project projectInstance = activeAppDetails.getActivePrjectInstance();
		if(projectInstance==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		assert(splitArguments.length == 3);
		
		String entityId = splitArguments[0];
		String signalName = splitArguments[1];
		String constantAsDecimalString  = splitArguments[2];
		
		return establishConstantConnection(projectInstance, entityId, signalName,constantAsDecimalString);

	}

	/**
	 * 
	 * @param projectInstance
	 * @param entityId
	 * @param signalName
	 * @param constantAsDecimalString
	 * @return
	 * @throws ProcGenException
	 */
	public String establishConstantConnection(Project projectInstance,
			String entityId, String signalName, String constantAsDecimalString) throws ProcGenException {
		
		EntityManager em = projectInstance.getEntityManager();
		Entity entityReqd = em.getEntityById(entityId);
		
		if(entityReqd == null)
			throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
		
		SignalBus reqdSignalBus = entityReqd.getInputByName(signalName); 
		
		if(reqdSignalBus == null){ // if not in input check output
			reqdSignalBus = entityReqd.getOutputByName(signalName);
		}
		
		if(reqdSignalBus == null)
			throw new ProcGenException(Consts.ExceptionMessages.SIGNAL_NOT_RECOGNISED);
		
		ConnectionManager cm = null;
		if(entityId.contains("-")){ // child entity
			cm = entityReqd.getParent().getEntityConnectionManager();
		}
		
		else{
			cm = projectInstance.getConnectionManager();
		}
		
		int valueToSet = Integer.parseInt(constantAsDecimalString, 10);
		String binaryString = Integer.toBinaryString(valueToSet);

		if(binaryString.length() > reqdSignalBus.getBusWidth()){
			throw new ProcGenException(Consts.ExceptionMessages.UNEQUAL_LENGTH_SIGNAL_ASSIGNMENT + ": " + reqdSignalBus.getName());
		}
		
		if(binaryString.length() <reqdSignalBus.getBusWidth()){
			StringBuilder strb = new StringBuilder(binaryString);
			
			int diff = reqdSignalBus.getBusWidth() - binaryString.length(); 
			for(int i=0;i<diff;i++){
				strb = strb.insert(0, '0');
			}
			binaryString = strb.toString();
		}
		reqdSignalBus.setValue(SignalBus.getSignalArrayFromString(binaryString));
		
		cm.addConnectionToConstantMap(entityReqd, reqdSignalBus, valueToSet);
		
		Project activeProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance();
		
		projectInstance.getProjectSimulator().addEntitySimulatorToEvaluate(entityReqd.getEntitySimulator());
		
		//TODO: think about commenting this line
		//entityReqd.defaultBehaviour();
		
		return Consts.CommandResults.SUCCESS_CONSTANT_CONNECTION + entityId + " :" + signalName + " value" + constantAsDecimalString;
	}

}
