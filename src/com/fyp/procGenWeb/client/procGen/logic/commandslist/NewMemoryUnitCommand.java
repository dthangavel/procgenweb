/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit.MemoryUnitType;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
/*
 *  Arguments: name, storage size, inputAddressBusSize, outputBus size,parentId, memType
 */
public class NewMemoryUnitCommand implements Command {

	@Override
	public String execute(String arguments) throws ProcGenException {
		// TODO Auto-generated method stub
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		String memName = splitArguments[0];
		int noOfStorageUnits  = 1;
		int sizeOfInputBus = 1;
		int widthOfOutputBus = 1;
		try {
			noOfStorageUnits = Integer.parseInt(splitArguments[1]);
			sizeOfInputBus = Integer.parseInt(splitArguments[2]);
			widthOfOutputBus = Integer.parseInt(splitArguments[3]);
		} catch(NumberFormatException e){
			throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
		}
		
		String parentId = "0";
		if(splitArguments.length > 4){
			parentId = splitArguments[4];
		}

		String memType = "Ram";

		
		MemoryUnit newMemUnit = null ;
		if(splitArguments.length > 5){
			memType = splitArguments[5];
			
			int extraInOut = 0;
			
			try {
				if (splitArguments.length > 6) {
					extraInOut = Integer.parseInt(splitArguments[6]);
				}
			} catch (NumberFormatException e) {
				throw new ProcGenException(
						Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
			}
			
			if(memType.equals("Rom")){
				newMemUnit = new MemoryUnit("", memName, sizeOfInputBus,
						noOfStorageUnits,widthOfOutputBus,MemoryUnitType.Rom,extraInOut);
			}
			else if(memType.equals("Ram")){
				newMemUnit = new MemoryUnit("", memName, sizeOfInputBus,
						noOfStorageUnits,widthOfOutputBus,MemoryUnitType.Ram,extraInOut);
			}
			else{
				throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
			}
		}
		
		if(parentId.length()>0 && !parentId.equals("0")){
			EntityManager entityManager = activeAppDetails.getActivePrjectInstance().getEntityManager();
			Entity parentEntity = entityManager.getEntityById(parentId);
			if(parentEntity != null)
				newMemUnit.setParent(parentEntity);
			else{
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
			}
		}

		String entityId = activeAppDetails.getActivePrjectInstance().getEntityManager().addEntity(newMemUnit);

		return Consts.CommandResults.SUCCESS_NEW_MEM_UNIT_CREATION + entityId;
	}

}
