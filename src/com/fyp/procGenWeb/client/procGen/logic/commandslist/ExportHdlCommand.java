/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.communicator.HdlTranslationRpcCommunicator;
import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class ExportHdlCommand implements Command{

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		Project activeProject = activeAppDetails.getActivePrjectInstance();
		
		//ElectronicsToHdlConverter hdlConverter; 
		String[] splitArguments = arguments.split("\\s+");
		String hdlLanguage; // TODO: choose language
		if(splitArguments.length != 1){
			throw new ProcGenException(Consts.ExceptionMessages.INCORRECT_NO_OF_ARGUMENTS);
		}
		
		
//		if(splitArguments[0] == Consts.Misc.VHDL)
//			hdlConverter = new ElectronicsToVhdlConverter(activeProject);
//		else
//			throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
		HdlTranslationRpcCommunicator hdlCommunicator = new HdlTranslationRpcCommunicator();
		
		hdlCommunicator.sendProjectDetailsToServer(activeProject);
		
		return Consts.CommandResults.SUCCESS_EXPORT_TO_VHDL;
	}

}
