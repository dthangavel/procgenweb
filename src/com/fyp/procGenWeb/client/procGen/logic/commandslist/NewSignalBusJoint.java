package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusJoint;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusSplitter;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

public class NewSignalBusJoint implements Command {

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		if(splitArguments.length != 4){
			throw new ProcGenException(Consts.ExceptionMessages.INCORRECT_NO_OF_ARGUMENTS);
		}
		
		String name = splitArguments[0];
		String input0WidthAsString = splitArguments[1];
		String input1WidthAsString = splitArguments[2];
		String parentId = splitArguments[3];
		
		SignalBusJoint newSignalBusJoint = null;
		
		int input0Width = 0;
		int input1Width = 0;
		
		try{
			input0Width = Integer.parseInt(input0WidthAsString);
			input1Width = Integer.parseInt(input1WidthAsString);
			
			newSignalBusJoint = new SignalBusJoint(name,input0Width,input1Width);
		}
		
		catch(NumberFormatException e){
			throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
		}
		
		
		if(parentId.length()>0 && !parentId.equals("0")){
			EntityManager entityManager = activeAppDetails.getActivePrjectInstance().getEntityManager();
			Entity parentEntity = entityManager.getEntityById(parentId);
			if(parentEntity != null)
				newSignalBusJoint.setParent(parentEntity);
			else{
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
			}
		}
		
		String entityId = activeAppDetails.getActivePrjectInstance().getEntityManager().addEntity(newSignalBusJoint);

		return Consts.CommandResults.SUCCESS_NEW_SIGNAL_BUS_JOINT_CREATION + entityId;
	}

}
