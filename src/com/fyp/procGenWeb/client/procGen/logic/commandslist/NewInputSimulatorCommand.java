/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusObserver;
import com.fyp.procGenWeb.client.procGen.logic.UndoableCommand;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.simulation.InputSimulator;
import com.fyp.procGenWeb.client.procGen.simulation.ProjectSimulator;

/**
 * @author DINESH THANGAVEL
 *
 */
public class NewInputSimulatorCommand implements UndoableCommand{
	
	@Override
	public String execute(String arguments) throws ProcGenException {
		// parentid should be 0 or ""
		String[] splitArgs = arguments.split("\\s+");
		String nameOfInputSimulator = splitArgs[0];
		int widthOfSimulator = 0 ;
		try{
			widthOfSimulator = Integer.parseInt(splitArgs[1]);
		}
				
		catch(NumberFormatException e){
			throw new ProcGenException(Consts.ErrorCodes.INVALID_COMMAND_ARGUMENT,Consts.ExceptionMessages.ERROR_CREATING_INPUT_SIM);
		}

		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		Project activeProjectInstance = activeAppDetails.getActivePrjectInstance();
		if(activeProjectInstance==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		EntityManager em = activeProjectInstance.getEntityManager();
		String destinationEntityId = splitArgs[2];
		String signalName = splitArgs[3];
		
		Entity entityToConnect = em.getEntityById(destinationEntityId);
		if(entityToConnect == null)
			throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
		
		SignalBus portToConnect = entityToConnect.getInputByName(signalName);
		if(portToConnect == null)
			throw new ProcGenException(Consts.ExceptionMessages.SIGNAL_NOT_RECOGNISED);
			
		InputSimulator newInputSimulator = new InputSimulator(nameOfInputSimulator, widthOfSimulator);
		ProjectSimulator proSim =activeProjectInstance.getProjectSimulator(); 
		proSim.addInputSimulator(newInputSimulator, portToConnect);

		newInputSimulator.setValue(Signal.LOW);

		portToConnect.setValue(Signal.LOW);
		//entityToConnect.defaultBehaviour();
		
		SignalBusObserver newSignalBusObserver = new SignalBusObserver(newInputSimulator,activeProjectInstance.getProjectSimulator(),newInputSimulator.getName());
		
		// hook up the entity simulator to the signal observer
		newSignalBusObserver.addEntitySimulatorListener(entityToConnect.getEntitySimulator());
		proSim.getSignalObserverMap().put(newInputSimulator, newSignalBusObserver);
		
		return Consts.CommandResults.SUCCESS_NEW_INPUT_SIMULATOR;
	}

	@Override
	public String undo() {
		// TODO Auto-generated method stub
		return null;
	}

}
