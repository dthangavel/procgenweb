/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityType;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Adder;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Multiplexer;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class NewAdderCommand implements Command {

	/**
	 * 
	 */
	
	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		String name = splitArguments[0];
		String widthOfInput0AsString = splitArguments[1];
		String widthOfInput1AsString = splitArguments[2];
		String parentId = splitArguments[3];
		
		int numOfInputs = 2;
		if(splitArguments.length == 5){
			boolean isCarryPresent = Boolean.parseBoolean(splitArguments[4]);
			if(isCarryPresent){
				numOfInputs = 3;
			}
		}
		
		assert(splitArguments.length <= 5);

		Adder newAdder = null;
		
		int input0Width = 0;
		int input1Width = 0;
		
		try{
			input0Width = Integer.parseInt(widthOfInput0AsString);
			input1Width = Integer.parseInt(widthOfInput1AsString);
			newAdder = new Adder(name,numOfInputs,input0Width,input1Width,true);
		}
		
		catch(NumberFormatException e){
			throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
		}
		
		
		if(parentId.length()>0 && !parentId.equals("0")){
			EntityManager entityManager = activeAppDetails.getActivePrjectInstance().getEntityManager();
			Entity parentEntity = entityManager.getEntityById(parentId);
			if(parentEntity != null)
				newAdder.setParent(parentEntity);
			else{
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
			}
		}
		
		String entityId = activeAppDetails.getActivePrjectInstance().getEntityManager().addEntity(newAdder);

		return Consts.CommandResults.SUCCESS_NEW_ADDER_CREATION + entityId;
	}

}
