/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityType;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Register;
import com.fyp.procGenWeb.client.procGen.logic.UndoableCommand;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class NewRegisterCommand implements UndoableCommand {

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		int widthOfInput = 1;
		String parentId = ""; 
		
		if(splitArguments.length > 0){
			widthOfInput = Integer.parseInt(splitArguments[0]);
			// TODO: catch number format exception
		}
		
		String triggerType ="";
		if(splitArguments.length > 1){
			triggerType = splitArguments[1];
		}
		
		String registerName = "";
		if(splitArguments.length > 2)
			registerName = splitArguments[2];
		
		if(registerName == null || registerName == "" )
			registerName = "Register";
		
		if(splitArguments.length > 3){
			parentId = splitArguments[3];
		}
		Register newRegister = new Register(registerName,widthOfInput,triggerType);
		newRegister.setEntityType(EntityType.Register);
		
		if(parentId.length()>0 && !parentId.equals("0")){
			EntityManager entityManager = activeAppDetails.getActivePrjectInstance().getEntityManager();
			Entity parentEntity = entityManager.getEntityById(parentId);
			if(parentEntity != null)
				newRegister.setParent(parentEntity);
			else{
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
			}
		}
		
		String entityId = activeAppDetails.getActivePrjectInstance().getEntityManager().addEntity(newRegister);
		return Consts.CommandResults.SUCCESS_NEW_REGISTER_CREATION + entityId;
	}

	@Override
	public String undo() {
		// TODO Auto-generated method stub
		return null;
	}

}
