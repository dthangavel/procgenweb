/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity.EntityTriggerType;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class SetEntityTriggerType implements Command {


	@Override
	public String execute(String arguments) throws ProcGenException {
		
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		if(splitArguments.length < 2){
			throw new ProcGenException(Consts.ExceptionMessages.INCORRECT_NO_OF_ARGUMENTS);
		}
		
		String entityId = splitArguments[0];
		String triggerType = splitArguments[1];
		
		EntityManager em = activeAppDetails.getActivePrjectInstance().getEntityManager();
		Entity entityToSet = em.getEntityById(entityId);
		
		if(entityToSet == null)
			throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
		
		
		if(triggerType.compareToIgnoreCase("rising")==0){
			entityToSet.setEntityTriggerType(EntityTriggerType.RISING_EDGE_TRIGGERED);
		}
		
		else if(triggerType.compareToIgnoreCase("falling")==0){
			entityToSet.setEntityTriggerType(EntityTriggerType.FALLING_EDGE_TRIGGERED);
		}
		
		else if(triggerType.compareToIgnoreCase("non_clocked")==0){
			entityToSet.setEntityTriggerType(EntityTriggerType.NON_CLOCKED);
		}
		
		else{
			throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED + " : " + triggerType);
		}
		
		return Consts.CommandResults.SUCCESS_SET_ENTITY_TRIGGER_TYPE;
	}

}
