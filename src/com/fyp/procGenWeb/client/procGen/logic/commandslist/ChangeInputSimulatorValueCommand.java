/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.simulation.InputSimulator;
import com.fyp.procGenWeb.client.procGen.simulation.ProjectSimulator;

/**
 * @author DINESH THANGAVEL
 *
 */
public class ChangeInputSimulatorValueCommand implements Command{

	@Override
	public String execute(String arguments) throws ProcGenException {
		
		String[] splitArgs = arguments.split("\\s+");
		String nameOfInputSimulator = splitArgs[0];
		String valueToSetAsString = splitArgs[1];
		
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		Project activeProjectInstance = activeAppDetails.getActivePrjectInstance();
		ProjectSimulator prjSim = activeProjectInstance.getProjectSimulator();
		
		SignalBus signalToModify = null;
		if(nameOfInputSimulator.length() > 0){
			signalToModify = prjSim.getSignalBusConnectedToInputSimulator(nameOfInputSimulator);
			if(signalToModify == null){
				return Consts.CommandResults.INPUT_SIM_NOT_CONNECTED;
			}
		}
		
		SignalBus newTemporarySignalBus = new SignalBus("temp",valueToSetAsString);
		InputSimulator inputSimToChange = prjSim.getInputSimulator(nameOfInputSimulator);
		assert(inputSimToChange != null);

		signalToModify.setValue(newTemporarySignalBus.getValue());
		inputSimToChange.setValue(newTemporarySignalBus.getValue());
		
		return Consts.CommandResults.SUCCESS_CHANGED_SIMULATOR_VALUE;
	}

}
