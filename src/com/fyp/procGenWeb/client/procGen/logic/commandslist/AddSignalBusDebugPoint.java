/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.display.presenter.SignalBusDebuggerPresenter;
import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */


/**
 *  Arguments entityId, SignalName
 */

public class AddSignalBusDebugPoint implements Command {

	@Override
	public String execute(String arguments) throws ProcGenException {
		
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		String entityId = splitArguments[0];
		String signalNameAsString = splitArguments[1];
		
		Entity reqdEntity = activeAppDetails.getActivePrjectInstance().getEntityManager().getEntityById(entityId);
		
		if(reqdEntity == null)
			throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
		
		SignalBus signalBusToWatch =  reqdEntity.getInputByName(signalNameAsString);
		
		if(signalBusToWatch == null)
			signalBusToWatch = reqdEntity.getOutputByName(signalNameAsString);
		
		if(signalBusToWatch == null)
			throw new ProcGenException(Consts.ExceptionMessages.SIGNAL_NOT_RECOGNISED);
		
		SignalBusDebuggerPresenter presenterInstance = SignalBusDebuggerPresenter.getSignalBusDebuggerPresenterInstance();
		presenterInstance.registerDebugPoint(signalBusToWatch,entityId, activeAppDetails.getActivePrjectInstance());
		
		return Consts.CommandResults.SUCCESS_ADDED_SIGNALBUS_TO_WATCH_LIST;
	}

}
