/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusUniqueLocater;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.InvalidSignalException;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class InitialiseSignalBusCommand implements Command {

	@Override
	public String execute(String arguments) throws ProcGenException {

		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade
				.getInstance();
		if (activeAppDetails.getActivePrjectInstance() == null) {
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}

		String[] splitArguments = arguments.split("\\s+");

		if (splitArguments.length != 3) {
			throw new ProcGenException(
					Consts.ExceptionMessages.INCORRECT_NO_OF_ARGUMENTS);
		}

		String entityId = splitArguments[0];
		String signalBusName = splitArguments[1];
		String digitalValue = splitArguments[2];

		return initialiseSignalBusStartValue(activeAppDetails.getActivePrjectInstance(), entityId,
				signalBusName, digitalValue);
	}

	/**
	 * @param projectInstance
	 * @param entityId
	 * @param signalBusName
	 * @param digitalValue
	 * @return
	 * @throws ProcGenException
	 * @throws InvalidSignalException
	 */
	public String initialiseSignalBusStartValue(
			Project projectInstance, String entityId,
			String signalBusName, String digitalValue) throws ProcGenException,
			InvalidSignalException {
		EntityManager entityManager = projectInstance.getEntityManager();
		Entity entityToInitialise = entityManager.getEntityById(entityId);
		if (entityToInitialise != null)
			entityToInitialise.initialiseOutputSignal(signalBusName,
					digitalValue);
		else {
			throw new ProcGenException(
					Consts.ExceptionMessages.ENTITY_NOT_FOUND + " " + entityId);
		}

		projectInstance.getProjectSimulator().addNewInitialisationValue(new SignalBusUniqueLocater(entityId,signalBusName,Integer.valueOf(digitalValue,2)));
		
		return Consts.CommandResults.SUCCESS_INTIALISED_SIGNAL_BUS;
	}

}
