/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import java.util.ArrayList;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class InsertContentsInMemory implements Command {

	/**
	 * 
	 */
	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		// TODO: clean the arguments validation
		String[] splitArguments = arguments.split("\\s+");
		
		String entityId = splitArguments[0];
		Entity memUnit = activeAppDetails.getActivePrjectInstance().getEntityManager().getEntityById(entityId);
		
		if(memUnit == null)
			throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
		
		if(!(memUnit instanceof MemoryUnit)){
			throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_INSTANCE_OF_MEMORY);
		}
		
		List<String> signalBusToAdd = new ArrayList<String>();
		
		
		for(int i = 1;i<splitArguments.length;i++){
			String signalValue = splitArguments[i];
			signalBusToAdd.add(signalValue);
		}
		
		if(signalBusToAdd.size() > 0){
			((MemoryUnit)memUnit).getStorage().clear();
			((MemoryUnit)memUnit).programMemoryUnit(signalBusToAdd);
		}
		
		Project prj = activeAppDetails.getActivePrjectInstance();
		prj.getProjectSimulator().addEntitySimulatorToEvaluate(memUnit.getEntitySimulator());
		
		// removing it because this method can also be called in server when no simulation is needed
		//memUnit.defaultBehaviour();
		
		return Consts.CommandResults.SUCCESS_PROGRAMMED_MEMORY;
	}

}
