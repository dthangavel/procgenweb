/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityType;
import com.fyp.procGenWeb.client.procGen.electronics.entities.AndGate;
import com.fyp.procGenWeb.client.procGen.logic.UndoableCommand;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
/*
 * Args: name, no of inputs, parent id, bus width
 */
public class NewAndGateCommand implements UndoableCommand{

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		// TODO: clean the arguments validation
		String[] splitArguments = arguments.split("\\s+");
		String nameOfGate = "And";
		String parentId = ""; 
		String busWidthAsString = "1";
		
		int noOfInputs = 2;
		if(splitArguments[0].length() > 0){
			nameOfGate = splitArguments[0];
		}	
		
		if(splitArguments.length > 1){
			try{
				noOfInputs = Integer.parseInt(splitArguments[1]);
			}
			catch(NumberFormatException e){
				throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
			}
		}
		
		if(splitArguments.length > 2){
			parentId = splitArguments[2];
		}
		
		if(splitArguments.length > 3){
			busWidthAsString = splitArguments[3];
		}
		
		int busWidth = 1;
		
		try{
			busWidth =Integer.parseInt(busWidthAsString);
		}
		catch(NumberFormatException e){
			throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
		}
		
		AndGate newAndGate = new AndGate("", nameOfGate,noOfInputs,busWidth);
		if(parentId.length()>0 && !parentId.equals("0")){
			EntityManager entityManager = activeAppDetails.getActivePrjectInstance().getEntityManager();
			Entity parentEntity = entityManager.getEntityById(parentId);
			if(parentEntity != null)
				newAndGate.setParent(parentEntity);
			else{
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
			}
		}
		newAndGate.setEntityType(EntityType.AndGate);
		String entityId = activeAppDetails.getActivePrjectInstance().getEntityManager().addEntity(newAndGate);

		return Consts.CommandResults.SUCCESS_NEW_AND_CREATION + entityId;
	}

	@Override
	public String undo() {
		// TODO Auto-generated method stub
		return null;
	}

}
