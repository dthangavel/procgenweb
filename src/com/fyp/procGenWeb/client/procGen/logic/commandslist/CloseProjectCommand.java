/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts.CommandResults;

/**
 * @author DINESH THANGAVEL
 *
 */
public class CloseProjectCommand implements Command {

	@Override
	public String execute(String arguments) throws ProcGenException {
		// TODO ask for save etc
		ElectronicsLogicFacade eleLogic = ElectronicsLogicFacade.getInstance();
		String projectName = eleLogic.closeActiveProject();
		return CommandResults.SUCCESS_CLOSED_PROJECT + projectName;
	}

}
