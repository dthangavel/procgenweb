/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusSplitter;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class NewSignalBusSplitter implements Command {

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		String name = splitArguments[0];
		String inputBusWidthAsString = splitArguments[1];
		String outputBusWidthAsString = splitArguments[2];
		String startingIndexAsString = splitArguments[3];
		String parentId = splitArguments[4];
		
		SignalBusSplitter newSignalBusSplitter = null;
		
		int inputWidth = 0;
		int outputWidth = 0;
		int startingIndex = 0;
		
		try{
			inputWidth = Integer.parseInt(inputBusWidthAsString);
			outputWidth = Integer.parseInt(outputBusWidthAsString);
			startingIndex = Integer.parseInt(startingIndexAsString);
			
			if(inputWidth - startingIndex < outputWidth){
				throw new ProcGenException(Consts.ExceptionMessages.INVALID_STARTING_INDEX);
			}
			
			newSignalBusSplitter = new SignalBusSplitter(name,inputWidth,outputWidth,startingIndex,true);
		}
		
		catch(NumberFormatException e){
			throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
		}
		
		
		if(parentId.length()>0 && !parentId.equals("0")){
			EntityManager entityManager = activeAppDetails.getActivePrjectInstance().getEntityManager();
			Entity parentEntity = entityManager.getEntityById(parentId);
			if(parentEntity != null)
				newSignalBusSplitter.setParent(parentEntity);
			else{
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
			}
		}
		
		String entityId = activeAppDetails.getActivePrjectInstance().getEntityManager().addEntity(newSignalBusSplitter);

		return Consts.CommandResults.SUCCESS_NEW_SIGNAL_BUS_SPLITTER_CREATION + entityId;
	}
}
