/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class GetSignalBusValueCommand implements Command{

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		assert(splitArguments.length == 2);
		
		String entityId = splitArguments[0];
		String signalName = splitArguments[1];
		
		EntityManager em = activeAppDetails.getActivePrjectInstance().getEntityManager();
		Entity entityReqd = em.getEntityById(entityId);
		
		SignalBus reqdSignalBus = entityReqd.getInputByName(signalName); 
		
		if(reqdSignalBus == null){
			reqdSignalBus = entityReqd.getOutputByName(signalName);
		}
		
		if(reqdSignalBus != null)
			return reqdSignalBus.getDisplayValue();
		
		return Consts.ExceptionMessages.SIGNAL_NOT_RECOGNISED;
	}
	
}
