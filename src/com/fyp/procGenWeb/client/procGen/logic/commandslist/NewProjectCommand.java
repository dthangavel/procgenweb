/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.logic.Command;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class NewProjectCommand implements Command {

	@Override
	public String execute(String args) throws ProcGenException {
		ElectronicsLogicFacade eleLogic = ElectronicsLogicFacade.getInstance();
		if(args.length() ==0)
			throw new ProcGenException("Argument cannot be empty for new_project");
		assert(args.length() > 0);
		eleLogic.createNewProject(args);
		return Consts.CommandResults.SUCCESS_NEW_PROJECT_CREATION + args;
	}
	
}
