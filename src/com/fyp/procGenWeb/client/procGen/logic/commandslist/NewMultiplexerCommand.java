/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic.commandslist;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityType;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Multiplexer;
import com.fyp.procGenWeb.client.procGen.logic.UndoableCommand;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
/*
 * arguments: name, selectionWidthAsString, inputBusWidthAsString, parentId
 * 
 */
public class NewMultiplexerCommand implements UndoableCommand{

	@Override
	public String execute(String arguments) throws ProcGenException {
		ElectronicsLogicFacade activeAppDetails = ElectronicsLogicFacade.getInstance();
		if(activeAppDetails.getActivePrjectInstance()==null){
			return Consts.CommandResults.CREATE_PROJECT_FIRST;
		}
		
		String[] splitArguments = arguments.split("\\s+");
		
		String name = splitArguments[0];
		String selectionWidthAsString = splitArguments[1];
		String inputBusWidthAsString = splitArguments[2];
		String parentId = splitArguments[3];
		
		assert((splitArguments.length - 4)%2 == 0);

		Multiplexer newMux = null;
		
		int selectionWidth = 0;
		int inputBusWidth = 0;
		
		try{
		selectionWidth = Integer.parseInt(selectionWidthAsString);
		inputBusWidth = Integer.parseInt(inputBusWidthAsString);
		newMux = new Multiplexer(name,selectionWidth,inputBusWidth);
		}
		
		catch(NumberFormatException e){
			throw new ProcGenException(Consts.ExceptionMessages.INPUT_NOT_RECOGNISED);
		}
		
		for(int i = 4;i< splitArguments.length ; i++){
			if(splitArguments[i].length() != selectionWidth)
				throw new ProcGenException(Consts.ExceptionMessages.UNEQUAL_LENGTH_SIGNAL_ASSIGNMENT + ": " +splitArguments[i]);
			
			SignalBus tempSignalBus = new SignalBus("temp",splitArguments[i]);
			i++;
			newMux.programMux(tempSignalBus.getDigitalDisplayValue(), splitArguments[i]);
		}
		
		if(parentId.length()>0 && !parentId.equals("0")){
			EntityManager entityManager = activeAppDetails.getActivePrjectInstance().getEntityManager();
			Entity parentEntity = entityManager.getEntityById(parentId);
			if(parentEntity != null)
				newMux.setParent(parentEntity);
			else{
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND);
			}
		}
		newMux.setEntityType(EntityType.Mux);
		String entityId = activeAppDetails.getActivePrjectInstance().getEntityManager().addEntity(newMux);

		return Consts.CommandResults.SUCCESS_NEW_MUX_CREATION + entityId;
	}

	@Override
	public String undo() {
		// TODO Auto-generated method stub
		return null;
	}

}
