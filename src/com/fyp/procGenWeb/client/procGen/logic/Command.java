/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic;

import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public interface Command {
  public String execute(String arguments) throws ProcGenException;
}
