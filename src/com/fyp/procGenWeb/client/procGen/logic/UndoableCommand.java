/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.logic;

/**
 * @author DINESH THANGAVEL
 *
 */
public interface UndoableCommand extends Command{
	public String undo();
}
