/*This interface is meant to be used for converting Progen Entity to any HDL*
 * 
 */
package com.fyp.procGenWeb.client.procGen.hdlConversion;

import com.fyp.procGenWeb.client.procGen.electronics.entities.Adder;
import com.fyp.procGenWeb.client.procGen.electronics.entities.AndGate;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Comparator;
import com.fyp.procGenWeb.client.procGen.electronics.entities.ComplexMultiplexer;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Multiplexer;
import com.fyp.procGenWeb.client.procGen.electronics.entities.OrGate;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Register;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusJoint;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusSplitter;


/**
 * @author DINESH THANGAVEL
 *
 */
public interface HdlConverter {

	Object convertAndGate(AndGate andGate);

	Object convertMux(Multiplexer multiplexer);

	Object convertOrGate(OrGate orGate);

	Object convertRegister(Register register);

	Object convertMemoryUnit(MemoryUnit memoryUnit);

	Object convertAdder(Adder adder);

	Object convertSignalBusSplitter(SignalBusSplitter signalBusSplitter);

	Object convertSignalBusJoint(SignalBusJoint signalBusJoint);

	Object convertComparator(Comparator comparator);

	Object convertComplexMultiplexer(ComplexMultiplexer complexMultiplexer);
}
