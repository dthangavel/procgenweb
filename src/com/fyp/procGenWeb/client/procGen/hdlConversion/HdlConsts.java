/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.hdlConversion;

/**
 * @author DINESH THANGAVEL
 *
 */
public class HdlConsts {
	public enum HdlLanguage{
		VHDL ;
	}
	
	public enum HdlConversionType{
		SeparateEntity,InlineConversion ;
	}
}
