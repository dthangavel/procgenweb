/**
 * 
 */
package com.fyp.procGenWeb.client.procGen.uiManager;

import java.io.IOException;
import java.util.HashMap;

import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;

/**
 * @author DINESH THANGAVEL
 * 
 */

/*
 * The purpose of this class is to retrieve the entity details from the user and
 * send it to the project.
 */
public class EntityDetailsRetriever {

	// This inner class will be used as a data transfer object and hence all the
	// attributes are public
	public static class EntityDetailsFromUser {
		// TODO: validate these parameters
		public String nameOfEntity;
		public String parentOfEntityId;
		public int noOfInputSignals;
		public int noOfOutputSignals;
		public HashMap<String, Integer> inputSignalNames = new HashMap<String, Integer>();
		public HashMap<String, Integer> outputSignalNames = new HashMap<String, Integer>();

	}

	static public EntityDetailsFromUser retrieveEntityDetails(UI ui)
			throws IOException {
		EntityDetailsFromUser newEntityDetails = new EntityDetailsRetriever.EntityDetailsFromUser();
		ui.printMessage(Consts.ConsoleUIConstants.NEW_ENTITY_HEADER);

		// TODO Current Assumption only word is entered and inputs are valid. So
		// validate them

		getEntityNameFromUser(ui,newEntityDetails);

		getEntityParentFromUser(ui,newEntityDetails);

		getNoOfInputSignals(ui,newEntityDetails);

		getNoOfOutputSignals(ui,newEntityDetails);

		getInputOutputSignalDetails(ui,newEntityDetails);

		return newEntityDetails;
	}

	private static void getInputOutputSignalDetails(UI ui,
			EntityDetailsFromUser newEntityDetails) throws IOException {
		for (int i = 0; i < newEntityDetails.noOfInputSignals; i++) {
			ui.printMessage(Consts.ConsoleUIConstants.PROMPT_NEW_ENTITY_INPUT_SIGNAL_NAME);
			String inputSignalName = null;
			while (true) {
				inputSignalName = ui.readInput();
				if (inputSignalName.length() > 0) {
					break;
					// TODO Check if name is already present
				}
			}
			int inputBusWidth = getInputSignalBusWidth(ui,newEntityDetails);

			newEntityDetails.inputSignalNames.put(inputSignalName,inputBusWidth);
		}

		for (int i = 0; i < newEntityDetails.noOfOutputSignals; i++) {
			ui
					.printMessage(Consts.ConsoleUIConstants.PROMPT_NEW_ENTITY_OUTPUT_SIGNAL_NAME);
			
			String outputSignalName;
			
			while (true) {
				outputSignalName = ui.readInput();
				if (outputSignalName.length() > 0)
					break;
				//TODO: check if name is already present
			}
			int outputBusWidth = getOutputSignalBusWidth(ui,newEntityDetails);

			newEntityDetails.outputSignalNames.put(outputSignalName,
					outputBusWidth);
		}
	}

	private static int getInputSignalBusWidth(UI ui,
			EntityDetailsFromUser newEntityDetails) throws IOException {

		ui
				.printMessage(Consts.ConsoleUIConstants.PROMPT_NEW_INPUT_SIGNAL_BUS_WIDTH);

		String busWidthAsString = ui.readInput();
		try {
			int busWidth = Integer.valueOf(busWidthAsString);
			return busWidth;

		} catch (NumberFormatException e) {
			ui
					.printMessage(Consts.ConsoleUIConstants.PRINT_NOT_A_NUMBER);
			return getInputSignalBusWidth(ui,newEntityDetails);
		}
	}

	private static int getOutputSignalBusWidth(UI ui,
			EntityDetailsFromUser newEntityDetails) throws IOException {

		ui
				.printMessage(Consts.ConsoleUIConstants.PROMPT_NEW_OUTPUT_SIGNAL_BUS_WIDTH);

		String busWidthAsString = ui.readInput();
		try {
			int busWidth = Integer.valueOf(busWidthAsString);
			return busWidth;

		} catch (NumberFormatException e) {
			ui
					.printMessage(Consts.ConsoleUIConstants.PRINT_NOT_A_NUMBER);
			return getOutputSignalBusWidth(ui,newEntityDetails);
		}
	}

	private static void getNoOfOutputSignals(UI ui,
			EntityDetailsFromUser newEntityDetails) throws IOException {
		ui
				.printMessage(Consts.ConsoleUIConstants.PROMPT_NEW_ENTITY_NO_OF_OUTPUT_SIGNALS);

		try {
			newEntityDetails.noOfOutputSignals = Integer.valueOf(ui
					.readInput());
		} catch (NumberFormatException e) {

			ui
					.printMessage(Consts.ConsoleUIConstants.PRINT_NOT_A_NUMBER);

			getNoOfOutputSignals(ui,newEntityDetails);
		}
	}

	private static void getNoOfInputSignals(UI ui,
			EntityDetailsFromUser newEntityDetails) throws IOException {
		ui
				.printMessage(Consts.ConsoleUIConstants.PROMPT_NEW_ENTITY_NO_OF_INPUT_SIGNALS);

		try {
			newEntityDetails.noOfInputSignals = Integer.valueOf(ui
					.readInput());

		} catch (NumberFormatException e) {

			ui
					.printMessage(Consts.ConsoleUIConstants.PRINT_NOT_A_NUMBER);
			getNoOfInputSignals(ui,newEntityDetails);
		}
	}

	private static void getEntityParentFromUser(UI ui,
			EntityDetailsFromUser newEntityDetails) throws IOException {
		ui
				.printMessage(Consts.ConsoleUIConstants.PROMPT_NEW_ENTITY_PARENT_ID);

		newEntityDetails.parentOfEntityId = ui.readInput();
	}

	private static void getEntityNameFromUser(UI ui,
			EntityDetailsFromUser newEntityDetails) throws IOException {
		ui
				.printMessage(Consts.ConsoleUIConstants.PROMPT_NEW_ENTITY_NAME);

		newEntityDetails.nameOfEntity = ui.readInput();
	}
}
