/**
 * 
 */
package com.fyp.procGenWeb.client.display.views;

import java.util.HashMap;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author DINESH THANGAVEL
 *
 */
public class SignalBusDebuggerUI {

	/**
	 * 
	 */
	
	HashMap<String,Label> signalBusDebugLabels = new HashMap<String,Label> ();
	
	public SignalBusDebuggerUI() {
		Label debuggerHead = new Label("Signal_name Time Old_value new_value");
		RootPanel.get("signal_debugger").add(debuggerHead);
	}

	public void addNewSignalBusObserveUnit(String signalName){
		Label debuggerHead = new Label(signalName);
		debuggerHead.getElement().setId(signalName);
		RootPanel.get("signal_debugger").add(debuggerHead);
	}
	
	public void updateSignalBusValue(String signalBusName,float time, String oldValue,String newValue){
			Label labelToUpdate = new Label(signalBusName + " " + time + " " + oldValue + " " + newValue);
			RootPanel.get("signal_debugger").add(labelToUpdate);
	}
	
}
