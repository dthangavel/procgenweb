/**
 * 
 */
package com.fyp.procGenWeb.client.display.views;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class ProjectView {

	private static ProjectView projectPresenterInstance = null;
	Tree projectTree = null;

	/**
	 * 
	 */
	public static ProjectView getInstance() {
		if (projectPresenterInstance == null)
			projectPresenterInstance = new ProjectView();
		return projectPresenterInstance;
	}

	private ProjectView() {
		// TODO Auto-generated constructor stub

		ElectronicsLogicFacade electronicsFacade = ElectronicsLogicFacade
				.getInstance();
		Project activeProject = electronicsFacade.getActivePrjectInstance();
		this.displayProjectAsTree(activeProject);
	}

	public void updateProjectPresenter() {
		ElectronicsLogicFacade electronicsFacade = ElectronicsLogicFacade
				.getInstance();
		Project activeProject = electronicsFacade.getActivePrjectInstance();

		if (projectTree != null) {
			RootPanel.get("prj_tree").remove(projectTree);
		}

		this.displayProjectAsTree(activeProject);
	}

	private void displayProjectAsTree(Project activeProject) {

		if (activeProject == null)
			return;

		projectTree = new Tree();

		TreeItem root = new TreeItem();
		root.setText(activeProject.getName());

		for (Entity baseEntity : activeProject.getEntityManager()
				.getBaseEntities()) {
			TreeItem baseEntityItem = new TreeItem();
			baseEntityItem.setText(baseEntity.getName() + ":"
					+ baseEntity.getId());
			root.addItem(baseEntityItem);

			evaluateChildEntities(baseEntityItem, baseEntity);
		}

		projectTree.addItem(root);

		projectTree.addSelectionHandler(new SelectionHandler<TreeItem>(){

			@Override
			public void onSelection(SelectionEvent<TreeItem> event) {
				String selectionName = event.getSelectedItem().getText();
				Project activeProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance(); 
				
				if(selectionName.equals(activeProject.getName())){
					return;
				}
				
				String[] splitString = selectionName.split(":");
				assert(splitString.length == 2);
				String id = splitString[1];
				
				Entity entityToDisplay;
				try {
					entityToDisplay = activeProject.getEntityManager().getEntityById(id);
					
					ConnectionsView connectionView = ConnectionsView.getInstance();
					connectionView.updateConnectionViewForEntity(entityToDisplay);
				} catch (ProcGenException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}});
		
		// Add it to the root panel.
		RootPanel.get("prj_tree").add(projectTree);

	}

	private void evaluateChildEntities(TreeItem baseEntityItem,
			Entity baseEntity) {

		TreeItem childEntityItem = null;

		for (Entity childEntity : baseEntity.getChildEntityList()) {
			childEntityItem = new TreeItem();
			childEntityItem.setText(childEntity.getName() + ":"
					+ childEntity.getId());
			baseEntityItem.addItem(childEntityItem);
			evaluateChildEntities(childEntityItem,childEntity);
		}
	}

}
