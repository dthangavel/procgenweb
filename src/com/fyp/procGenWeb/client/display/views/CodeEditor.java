package com.fyp.procGenWeb.client.display.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
/**
 * @author DINESH THANGAVEL
 *
 */
public class CodeEditor extends Composite implements HasText {

	private static CodeEditorUiBinder uiBinder = GWT
			.create(CodeEditorUiBinder.class);

	interface CodeEditorUiBinder extends UiBinder<Widget, CodeEditor> {
	}

	public CodeEditor() {
		initWidget(uiBinder.createAndBindUi(this));	
		editor.setStyleName("Editor");
		editor.setFocus(false);
	}


	@UiField
	TextArea editor;
	TextAreaElement hostElement;
	
	public void onLoad(){
		hostElement = editor.getElement().cast();
		loadCodeMirrorCode(hostElement);
	}

	public native void loadCodeMirrorCode(TextAreaElement hostElement)/*-{

	  $wnd.editor = $wnd.CodeMirror.fromTextArea(hostElement, {
   	  mode:  "text/x-vhdl",
   	  lineNumbers:true,
   	  lineWrapping:true,
   	});
	}-*/;
	
	public native void setText(String text)/*-{
		$wnd.editor.setValue(text);
	}-*/; 
	
	public native String getText()/*-{
		return $wnd.editor.getValue();
	}-*/;

}
