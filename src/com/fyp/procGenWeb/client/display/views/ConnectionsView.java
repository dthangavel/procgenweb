/**
 * 
 */
package com.fyp.procGenWeb.client.display.views;

import java.util.Comparator;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Connection;
import com.fyp.procGenWeb.client.procGen.electronics.core.ConnectionManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusConstantMapping;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class ConnectionsView {

	private static ConnectionsView connectionViewInstance = null;
	CellTable<Connection> signalConnectionTable = null;
	CellTable<SignalBusConstantMapping> constantConnectionTable = null;

	private ConnectionsView() {
		// TODO Auto-generated constructor stub
	}

	public static ConnectionsView getInstance() {

		if (connectionViewInstance == null) {
			connectionViewInstance = new ConnectionsView();
		}
		return connectionViewInstance;
	}

	public void updateConnectionViewForEntity(Entity entityToDisplay) {
		ConnectionManager parentConnectionManager;
		if (entityToDisplay.getParent() == null) {
			parentConnectionManager = ElectronicsLogicFacade.getInstance()
					.getActivePrjectInstance().getConnectionManager();
		}

		else {
			parentConnectionManager = entityToDisplay.getParent()
					.getEntityConnectionManager();
		}

		List<Connection> connectionList = parentConnectionManager
				.getAllConnectionsInEntityAsList(entityToDisplay.getId());
		List<Connection> connectionListInEntity = entityToDisplay
				.getEntityConnectionManager().getAllConnectionsInEntityAsList(
						entityToDisplay.getId());

		for(Connection c:connectionListInEntity){
			connectionList.add(c);
		}
		
		displayConnectionList(connectionList, Type.INTERNAL);

		List<SignalBusConstantMapping> constantConnectionList = parentConnectionManager
				.getConstantConnectionsInEntityAsList(entityToDisplay.getId());
		displayConstantConnectionList(constantConnectionList);

	}

	private void displayConstantConnectionList(
			List<SignalBusConstantMapping> constantConnectionList) {

		if (constantConnectionTable != null) {
			RootPanel.get("connection_info").remove(constantConnectionTable);
			constantConnectionTable = null;
		}
		
		if (constantConnectionList == null) {
			return;
		}

		// Create a CellTable.
		constantConnectionTable = new CellTable<SignalBusConstantMapping>();

		// Create sourceSignalName column.
		TextColumn<SignalBusConstantMapping> sourceSignalName = new TextColumn<SignalBusConstantMapping>() {
			@Override
			public String getValue(SignalBusConstantMapping mapping) {
				return mapping.getSignalBusName();
			}
		};

		// Make the sourceSignalName column sortable.
		sourceSignalName.setSortable(true);

		// Create destinationColumn column.
		TextColumn<SignalBusConstantMapping> destinationColumn = new TextColumn<SignalBusConstantMapping>() {
			@Override
			public String getValue(SignalBusConstantMapping mapping) {
				return String.valueOf(mapping.getSignalBusValue());
			}
		};

		// Add the columns.
		constantConnectionTable.addColumn(sourceSignalName, "Source Signal");
		constantConnectionTable.addColumn(destinationColumn, "Constant value");

		// Create a data provider.
		ListDataProvider<SignalBusConstantMapping> dataProvider = new ListDataProvider<SignalBusConstantMapping>();

		// Connect the table to the data provider.
		dataProvider.addDataDisplay(constantConnectionTable);

		// Add the data to the data provider, which automatically pushes it to
		// the
		// widget.
		List<SignalBusConstantMapping> list = dataProvider.getList();
		for (SignalBusConstantMapping contact : constantConnectionList) {
			list.add(contact);
		}

		// Add a ColumnSortEvent.ListHandler to connect sorting to the
		// java.util.List.
		ListHandler<SignalBusConstantMapping> columnSortHandler = new ListHandler<SignalBusConstantMapping>(
				list);
		columnSortHandler.setComparator(sourceSignalName,
				new Comparator<SignalBusConstantMapping>() {
					public int compare(SignalBusConstantMapping o1,
							SignalBusConstantMapping o2) {
						if (o1 == o2) {
							return 0;
						}

						// Compare the name columns.
						if (o1 != null) {
							return (o2 != null) ? o1.getSignalBusName()
									.compareTo(o2.getSignalBusName()) : 1;
						}
						return -1;
					}
				});
		constantConnectionTable.addColumnSortHandler(columnSortHandler);

		// We know that the data is sorted alphabetically by default.
		constantConnectionTable.getColumnSortList().push(sourceSignalName);

		VerticalPanel panel = new VerticalPanel();
		panel.setBorderWidth(1);
		panel.setWidth("400");
		panel.add(constantConnectionTable);

		constantConnectionTable.getElement().setId("constant_connection_table");

		// Add it to the root panel.
		RootPanel.get("connection_info").add(constantConnectionTable);

	}

	private void displayConnectionList(List<Connection> connectionList,
			Type external) {

		if (signalConnectionTable != null) {
			RootPanel.get("connection_info").remove(signalConnectionTable);
			signalConnectionTable = null;
		}

		// Create a CellTable.
		signalConnectionTable = new CellTable<Connection>(connectionList.size());

		// Create sourceSignalName column.
		TextColumn<Connection> sourceSignalName = new TextColumn<Connection>() {
			@Override
			public String getValue(Connection connection) {
				return connection.getInputSignal().getName();
			}
		};

		// Make the sourceSignalName column sortable.
		sourceSignalName.setSortable(true);

		// Create destinationColumn column.
		TextColumn<Connection> destinationColumn = new TextColumn<Connection>() {
			@Override
			public String getValue(Connection connection) {
				return connection.getDestinationEntityId() + ":"
						+ connection.getOutputSignal().getName();
			}
		};

		// Add the columns.
		signalConnectionTable.addColumn(sourceSignalName, "Source Signal");
		signalConnectionTable.addColumn(destinationColumn,
				"Destination Entity Id:Signal");

		// Create a data provider.
		ListDataProvider<Connection> dataProvider = new ListDataProvider<Connection>();

		// Connect the table to the data provider.
		dataProvider.addDataDisplay(signalConnectionTable);

		// Add the data to the data provider, which automatically pushes it to
		// the
		// widget.
		List<Connection> list = dataProvider.getList();
		for (Connection contact : connectionList) {
			list.add(contact);
		}

		// Add a ColumnSortEvent.ListHandler to connect sorting to the
		// java.util.List.
		ListHandler<Connection> columnSortHandler = new ListHandler<Connection>(
				list);
		columnSortHandler.setComparator(sourceSignalName,
				new Comparator<Connection>() {
					public int compare(Connection o1, Connection o2) {
						if (o1 == o2) {
							return 0;
						}

						// Compare the name columns.
						if (o1 != null) {
							return (o2 != null) ? o1.getInputSignal().getName()
									.compareTo(o2.getInputSignal().getName())
									: 1;
						}
						return -1;
					}
				});
		signalConnectionTable.addColumnSortHandler(columnSortHandler);

		// We know that the data is sorted alphabetically by default.
		signalConnectionTable.getColumnSortList().push(sourceSignalName);

		VerticalPanel panel = new VerticalPanel();
		panel.setBorderWidth(1);
		panel.setWidth("400");
		panel.add(signalConnectionTable);

		signalConnectionTable.getElement().setId("connection_table");

		// Add it to the root panel.
		RootPanel.get("connection_info").add(signalConnectionTable);
	}

	private enum Type {
		INTERNAL, EXTERNAL;
	}
}
