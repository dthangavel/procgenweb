/**
 * 
 */
package com.fyp.procGenWeb.client.display.presenter;

import java.util.ArrayList;
import java.util.List;

import com.fyp.procGenWeb.client.display.views.SignalBusDebuggerUI;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.logicHelper.SignalBusDebugObserver;

/**
 * @author DINESH THANGAVEL
 *
 */
public class SignalBusDebuggerPresenter {

	/**
	 * 
	 */
	private static SignalBusDebuggerPresenter signalBusDebuggerPresenterInstance = null;
	
	Project activeProject;
	List<SignalBusDebugObserver> signalBusDebugPoints = new ArrayList<SignalBusDebugObserver>();
	SignalBusDebuggerUI debuggerUI;
	
	private SignalBusDebuggerPresenter(SignalBusDebuggerUI debuggerUI) {
		this.debuggerUI = debuggerUI;
	}
	
	public static SignalBusDebuggerPresenter getSignalBusDebuggerPresenterInstance(SignalBusDebuggerUI debuggerUi){
		if(signalBusDebuggerPresenterInstance == null)
			signalBusDebuggerPresenterInstance = new SignalBusDebuggerPresenter(debuggerUi);
		
		return signalBusDebuggerPresenterInstance;
	}
	
	public static SignalBusDebuggerPresenter getSignalBusDebuggerPresenterInstance(){
		return signalBusDebuggerPresenterInstance;
	}
	
	public void registerDebugPoint(SignalBus signalBusToDebug,String entityId,Project activeProject){
		this.signalBusDebugPoints.add(new SignalBusDebugObserver(entityId,signalBusToDebug,activeProject.getProjectSimulator(),this,"uiSignalBusObserver_" + signalBusToDebug.getName()+"_" + entityId));
		this.debuggerUI.addNewSignalBusObserveUnit(signalBusToDebug.getName()+ "_"+ entityId);
	}

	public void updateSignalBusValue(String nameOfSignalBus, String entityId, float currentTime, String oldValue, String newValue){
		this.debuggerUI.updateSignalBusValue(nameOfSignalBus + "_" + entityId, currentTime, oldValue, newValue);
	}
}
