/**
 * 
 */
package com.fyp.procGenWeb.client.display.presenter;

import java.util.HashMap;

import com.fyp.procGenWeb.client.display.views.CodeEditor;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.EditorNotPresentException;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class CodeEditorPresenter {
	
	private static CodeEditorPresenter codeEditorPresenterInstance = null;
	CodeEditor hdlCodeEditor = null;
	HashMap<String,String> editorFiles;
	HashMap<String,String> editorFilesToRead = new HashMap<String,String>();
	
	private CodeEditorPresenter(CodeEditor codeEditor){
		this.hdlCodeEditor = codeEditor;
	}
	
	public static CodeEditorPresenter getCodeEditorPresenterInstance(CodeEditor hdlCodeEditor){
		if(codeEditorPresenterInstance == null)
			codeEditorPresenterInstance = new CodeEditorPresenter(hdlCodeEditor);
		
		return codeEditorPresenterInstance;
	}
	
	public static CodeEditorPresenter getCodeEditorPresenterInstance(){
		return codeEditorPresenterInstance;
	}
	
	public void updateEditorDisplayCode(String fileName){
		String fileContent = editorFiles.get(fileName);
		hdlCodeEditor.setText(fileContent);
	}
	
	public boolean isFilePresentInEditorFiles(String fileName){
		if(this.hdlCodeEditor == null)
			return false;
		if (this.editorFiles != null) {
			return this.editorFiles.containsKey(fileName);
		}
		
		return false;
	}
	
	public void updateCodeResource(HashMap<String,String> newEditorFiles) throws ProcGenException{
		if(this.hdlCodeEditor == null)
			throw new EditorNotPresentException(Consts.ExceptionMessages.EDITOR_NOT_INSTANTIATED);
		this.editorFiles = newEditorFiles;
	}
	
	/**
	 *  Please note that this method will overwrite existing code
	 * @param sourceCode
	 */
	public void updateReadEditor(HashMap<String,String> sourceCode){
		this.editorFilesToRead = sourceCode;
	}
	
	public void readEditorCode(String fileName){
		String code = this.hdlCodeEditor.getText();
		this.editorFilesToRead.put(fileName, code);
	}
	
	public HashMap<String,String> getVhdlFilesToImport(){
		return this.editorFilesToRead;
	}
}
