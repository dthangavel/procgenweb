/**
 * 
 */
package com.fyp.procGenWeb.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fyp.procGenWeb.client.display.presenter.CodeEditorPresenter;
import com.fyp.procGenWeb.client.display.presenter.SignalBusDebuggerPresenter;
import com.fyp.procGenWeb.client.display.views.CodeEditor;
import com.fyp.procGenWeb.client.display.views.SignalBusDebuggerUI;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;

/**
 * @author DINESH THANGAVEL
 *
 */
public class SimMeasure implements EntryPoint {

	static ArrayList<String> testCaseRecord = new ArrayList<String>(); 
	
	final Label checkBoxLabel = new Label("Auto Mode");
	final CheckBox autoManualCheckBox = new CheckBox();
	
	final Label enterCaseNumber = new Label("Enter the case to simulate");
	final TextBox caseToSimulateTxtBox = new TextBox();
	final Button caseToSimulateButton = new Button("Load simulation test case");
	
	final Label enterNoOfTimesToRepeat = new Label("Enter no of runs");
	final TextBox noOfTimesTxtBox = new TextBox();
	final Button noOfTimesButton = new Button("Load no of runs");
	
	final Label displayFileNameLabel = new Label("File name to display");
	final TextBox fileNameToDisplayTextBox = new TextBox();
	Button displayFileButton = new Button("Display file");
	
	final Label inputLabel = new Label("Input Text Area");
	final TextArea inputTxtBox = new TextArea();
	final Label outputLabel = new Label("Output Text Area");
	final TextArea outputConsole = new TextArea();
	
	Button executeButton = new Button("Execute Commands");
	
	List<Long> commandStats = new ArrayList<Long>();
	
	int noOfCommands;
	
	@Override
	public void onModuleLoad() {
		testCaseRecord.add("new_project testProject \n new_and_gate \n new_input_simulator simulator1 1 1 input0 \n new_input_simulator simulator2 1 1 input1 \n add_input_stimulus 2 200 'change_sim_input simulator1 1' 200 'change_sim_input simulator2 1' \n add_input_stimulus 1 400 'change_sim_input simulator1 0'\n add_input_stimulus 1 600 'change_sim_input simulator1 1'\n simulate 0 1\n export_to_hdl vhdl\n close_project\n");
		testCaseRecord.add("new_project testProject \n new_and_gate \n new_input_simulator simulator1 1 1 input0 \n new_input_simulator simulator2 1 1 input1 \n add_input_stimulus 2 5 'change_sim_input simulator1 1' 5 'change_sim_input simulator2 1' \n add_input_stimulus 1 10 'change_sim_input simulator1 0'\n add_input_stimulus 1 15 'change_sim_input simulator1 1'\n add_input_stimulus 1 20 'change_sim_input simulator1 0'\n add_input_stimulus 1 25 'change_sim_input simulator1 1'\n simulate 0 1\n export_to_hdl vhdl\n close_project\n");
		
		autoManualCheckBox.setValue(true, true);
		autoManualCheckBox.addValueChangeHandler(new ValueChangeHandler<Boolean>(){

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if(autoManualCheckBox.getValue() == true){
					enterNoOfTimesToRepeat.setVisible(true);
					noOfTimesTxtBox.setVisible(true);
					noOfTimesButton.setVisible(true);
					executeButton.setVisible(false);
				}
				
				else{
					enterNoOfTimesToRepeat.setVisible(false);
					noOfTimesTxtBox.setVisible(false);
					noOfTimesButton.setVisible(false);
					executeButton.setVisible(true);
				}
			}
			
		});

		
		caseToSimulateButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				String testCaseNoAsString = caseToSimulateTxtBox.getValue();
				int testCaseNo = 0;
				try{
					testCaseNo = Integer.parseInt(testCaseNoAsString);
					if(testCaseNo >= testCaseRecord.size())
						displayException("Testcase not present with no: " + testCaseNo);
					inputTxtBox.setText(testCaseRecord.get(testCaseNo));
				}
				catch(NumberFormatException e){
					displayException(testCaseNoAsString +" -not a number ");
				}
			}
		});
		
		
		noOfTimesButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				String noOfTimesAsString = noOfTimesTxtBox.getText();
				
				int noOfTimes = 1;
				try{
					noOfTimes = Integer.parseInt(noOfTimesAsString);
					automateTestRun(noOfTimes);
				}
				catch(NumberFormatException e){
					displayException(noOfTimesAsString +" -not a number ");
				}
					
			}
		});
		
		displayFileButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				CodeEditorPresenter codeEditorPresenter = CodeEditorPresenter.getCodeEditorPresenterInstance();
				
				String fileNameToDisplay = fileNameToDisplayTextBox.getText();
				if(codeEditorPresenter.isFilePresentInEditorFiles(fileNameToDisplay))
					codeEditorPresenter.updateEditorDisplayCode(fileNameToDisplay);
				
				else{
					// Error
					outputConsole.setText("File does not exist");
				}
			}
			
		});
		
		outputConsole.isReadOnly();
		
		executeButton.getElement().setId("execute");
		executeButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				LogicFacade l = new LogicFacade();
				try {
					String commandsToProcess = inputTxtBox.getText();
					String commandList[] = commandsToProcess.split("\n");
					int noOfCommands = commandList.length;
							
					Date startTimeDate = new Date();
					long startTimeBeforeCommandStart = startTimeDate.getTime();
					long startTimeBeforeSimulate = 0;
					long endTime = 0;
					String result = "";
					
					for(int i=0;i<noOfCommands;i++){
						long commandStartTime = new Date().getTime();
						if(commandList[i].contains("simulate"))
							startTimeBeforeSimulate = new Date().getTime();
						result = result + l.processInput(commandList[i]);
						endTime = new Date().getTime();
						
						Label timeTakenForCommand = new Label("Time taken for command " + commandList[i] + ": " + (endTime - commandStartTime)*1000  + "ns");
						
						RootPanel.get().add(timeTakenForCommand);
						
						if (autoManualCheckBox.getValue() == true) {
							long oldTime = commandStats.get(i);
							commandStats.set(i, (endTime - commandStartTime)
									+ oldTime);
						}
						outputConsole.setText(result);
					}
					
					if(endTime != 0){
						Label totalTimeForCommandsLabel = new Label("Time taken for executing all commands: " + (endTime - startTimeBeforeCommandStart)*1000 + " ns");
						Label timeForSimulation = new Label("Time taken for simulation: " + (endTime-startTimeBeforeSimulate) + "ms" );
						Label divider = new Label("------------------------------------------------------------");
						
						RootPanel.get().add(totalTimeForCommandsLabel);
						RootPanel.get().add(timeForSimulation);
						RootPanel.get().add(divider);
					}
				} catch (ProcGenException e) {
					Label exceptionLbl = new Label(e.getMessage());
					RootPanel.get().add(exceptionLbl);
				}
								
			}});
		
		executeButton.setVisible(false);
		
		int width = 1000;
		int height = 300;
		
		RootPanel.get().add(checkBoxLabel);
		RootPanel.get().add(autoManualCheckBox);
		
		RootPanel.get().add(enterCaseNumber);
		RootPanel.get().add(caseToSimulateTxtBox);
		RootPanel.get().add(caseToSimulateButton);

		RootPanel.get().add(enterNoOfTimesToRepeat);
		RootPanel.get().add(noOfTimesTxtBox);
		RootPanel.get().add(noOfTimesButton);
		
		RootPanel.get().add(inputLabel);
		RootPanel.get().add(inputTxtBox);
		RootPanel.get().add(executeButton);
		RootPanel.get().add(outputLabel);
		RootPanel.get().add(outputConsole);
		
		RootPanel.get().add(displayFileNameLabel);
		RootPanel.get().add(fileNameToDisplayTextBox);
		RootPanel.get().add(displayFileButton);
		
		CodeEditor hdlViewer = new CodeEditor();
		CodeEditorPresenter codeEditorPresenter = CodeEditorPresenter.getCodeEditorPresenterInstance(hdlViewer);
		RootPanel.get().add(hdlViewer);
		
		SignalBusDebuggerUI debugUI= new SignalBusDebuggerUI();
		SignalBusDebuggerPresenter debuggerPresenter = SignalBusDebuggerPresenter.getSignalBusDebuggerPresenterInstance(debugUI);

	}
	
	public void automateTestRun(int noOfTimes){
		
		String commandsToProcess = inputTxtBox.getText();
		String commandList[] = commandsToProcess.split("\n");
		noOfCommands = commandList.length;
		
		for(int i=0;i<noOfCommands;i++){
			commandStats.add((long) 0);
		}	
		
		for(int i=0;i<noOfTimes;i++){
			executeButton.click();
		}
		
		for(int i=0;i<noOfCommands;i++){
			Label statsForCommandI = new Label(" Net Average time taken for " + commandList[i] +":" + (long)((commandStats.get(i)*1000)/noOfTimes) + " ns");
			RootPanel.get().add(statsForCommandI);
		}
		
		Label divider = new Label("------------------------------------------------------------");
		RootPanel.get().add(divider);
		commandStats.clear();	
	}
	
	private native void displayException(String message)/*-{
			$wnd.alert(message);
		}-*/;

}
