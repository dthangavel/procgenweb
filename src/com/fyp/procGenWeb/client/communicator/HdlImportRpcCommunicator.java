/**
 * 
 */
package com.fyp.procGenWeb.client.communicator;

import java.util.HashMap;
import java.util.List;

import com.fyp.procGenWeb.client.ImportHdlService;
import com.fyp.procGenWeb.client.ImportHdlServiceAsync;
import com.fyp.procGenWeb.client.display.views.ProjectView;
import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author DINESH THANGAVEL
 *
 */
public class HdlImportRpcCommunicator {

	public void sendHdlFileDetailsToServer(HashMap<String,String> hdlFiles){
		ImportHdlServiceAsync importService = GWT.create(ImportHdlService.class);
		importService.transferHdlFiles(hdlFiles,  new AsyncCallback<ProjectTransferObject>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to import:" + caught.getMessage());
				
			}

			@Override
			public void onSuccess(ProjectTransferObject result) {
				ElectronicsLogicFacade logicInstance = ElectronicsLogicFacade.getInstance();
				try {
					Project convertedProject = result.convertToProject();
					List<Entity> newEntitiesToAddList = convertedProject.getEntityManager().getBaseEntities();
					Project activePrj = logicInstance.getActivePrjectInstance();
					for(Entity newEntityToAdd: newEntitiesToAddList){
						activePrj.copyEntity(convertedProject, newEntityToAdd, activePrj, "");
					}
					//logicInstance.setActiveProjectInstance(convertedProject);
				} catch (ProcGenException e) {
					Window.alert("error converting the project: " + e.getMessage());
					e.printStackTrace();
				}
				
				ProjectView prjPresenter = ProjectView.getInstance();
				prjPresenter.updateProjectPresenter();
			}});
	}

}
