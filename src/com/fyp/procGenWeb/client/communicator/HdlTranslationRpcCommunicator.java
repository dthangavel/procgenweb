/**
 * 
 */
package com.fyp.procGenWeb.client.communicator;

import java.util.HashMap;

import com.fyp.procGenWeb.client.HdlTranslationService;
import com.fyp.procGenWeb.client.HdlTranslationServiceAsync;
import com.fyp.procGenWeb.client.display.presenter.CodeEditorPresenter;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.EditorNotPresentException;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author DINESH THANGAVEL
 *
 */
public class HdlTranslationRpcCommunicator {
	static String projectName; 
	
	public void sendProjectDetailsToServer(Project prj){
		projectName = prj.getName();
		HdlTranslationServiceAsync translationService = GWT.create(HdlTranslationService.class);
		ProjectTransferObject projectToTransfer = new ProjectTransferObject(prj); 
		translationService.transferProjectDetails(projectToTransfer, new AsyncCallback<HashMap<String,String>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
			
					Window.alert("failed to translate: " + caught.getMessage());
			}

			@Override
			public void onSuccess(HashMap<String, String> result) {
				// TODO Auto-generated method stub
				
						
				HashMap<String,String> generatedVhdl = result;
								
				CodeEditorPresenter codeEditorPresenter = CodeEditorPresenter.getCodeEditorPresenterInstance();
				if(codeEditorPresenter == null){
					// TODO: remove this try catch
					try {
						throw new EditorNotPresentException(Consts.ExceptionMessages.CANNOT_FIND_CODE_EDITOR);
					} catch (ProcGenException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					codeEditorPresenter.updateCodeResource(generatedVhdl);
				} catch (ProcGenException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				codeEditorPresenter.updateEditorDisplayCode(projectName);
				
				// updating the read Editor. This will overwrite any user modifications
//				CodeEditorPresenter presenterInstance = CodeEditorPresenter.getCodeEditorPresenterInstance();
//				presenterInstance.updateReadEditor(result);
			}
			
		});
	}
}
