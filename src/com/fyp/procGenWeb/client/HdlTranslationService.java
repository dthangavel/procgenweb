/**
 * 
 */
package com.fyp.procGenWeb.client;

import java.util.HashMap;

import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author DINESH THANGAVEL
 *
 */
@RemoteServiceRelativePath("translatehdl")
public interface HdlTranslationService extends RemoteService {
	public HashMap<String, String> transferProjectDetails(ProjectTransferObject projectToTransfer);
}
