/**
 * 
 */
package com.fyp.procGenWeb.client;

import com.fyp.procGenWeb.client.communicator.HdlImportRpcCommunicator;
import com.fyp.procGenWeb.client.display.presenter.CodeEditorPresenter;
import com.fyp.procGenWeb.client.display.presenter.SignalBusDebuggerPresenter;
import com.fyp.procGenWeb.client.display.views.CodeEditor;
import com.fyp.procGenWeb.client.display.views.ConnectionsView;
import com.fyp.procGenWeb.client.display.views.ProjectView;
import com.fyp.procGenWeb.client.display.views.SignalBusDebuggerUI;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;

/**
 * @author DINESH THANGAVEL
 *
 */
public class ProcGenWeb implements EntryPoint {
	
	ProjectView presenter = null;

	@Override
	public void onModuleLoad() {
		final Label inputLabel = new Label("Input Text Area");
		final TextArea inputTxtBox = new TextArea();
		final Label outputLabel = new Label("Output Text Area");
		final TextArea outputConsole = new TextArea();
		

		final Label displayFileNameLabel = new Label("File name to display");
		displayFileNameLabel.setStyleName("InputLabelStyle");
		final TextBox fileNameToDisplayTextBox = new TextBox();
		Button displayFileButton = new Button("Display file");
		displayFileButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				CodeEditorPresenter codeEditorPresenter = CodeEditorPresenter.getCodeEditorPresenterInstance();
				
				String fileNameToDisplay = fileNameToDisplayTextBox.getText();
				if(codeEditorPresenter.isFilePresentInEditorFiles(fileNameToDisplay))
					codeEditorPresenter.updateEditorDisplayCode(fileNameToDisplay);
				
				else{
					// Error
					outputConsole.setText("File does not exist");
				}
			}
			
		});
		
		outputConsole.setReadOnly(true);
		
		Button readFileButton = new Button("Read File");
		readFileButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				CodeEditorPresenter codeEditorPresenter = CodeEditorPresenter.getCodeEditorPresenterInstance();
				
				String fileNameToDisplay = fileNameToDisplayTextBox.getText();
				
				codeEditorPresenter.readEditorCode(fileNameToDisplay);
			}
			
		});

		Button importHdlButton = new Button("Import Hdl");
		importHdlButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				HdlImportRpcCommunicator communicator = new  HdlImportRpcCommunicator();
				CodeEditorPresenter presenterInstance = CodeEditorPresenter.getCodeEditorPresenterInstance();
				
				communicator.sendHdlFileDetailsToServer(presenterInstance.getVhdlFilesToImport());
				
			}});

		
		Button button = new Button("Execute Commands");
		button.getElement().setId("execute");
		button.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				LogicFacade l = new LogicFacade();
				try {
					String commandsToProcess = inputTxtBox.getText();
					String commandList[] = commandsToProcess.split("\n");
					int noOfCommands = commandList.length;
					for(int i=0;i<noOfCommands;i++){
						String result = l.processInput(commandList[i]);
						outputConsole.setText(result);
					}
					
					if(presenter != null)
						presenter.updateProjectPresenter();
					
				} catch (ProcGenException e) {
					Window.alert(e.getMessage());
				}
				
//				greetingService.greetServer("clicked", new AsyncCallback<String>() {
//							public void onFailure(Throwable caught) {
//								// Show the RPC error message to the user
//								Label lbl = new Label("Faliure occurred");
//								RootPanel.get().add(lbl);
//							}
//
//							public void onSuccess(String result) {
//								Label lbl = new Label("Success occurred:" + result);
//								RootPanel.get().add(lbl);
//							}
//						});
				
			}});
		

		
		int width = 1000;
		int height = 300;
		
		RootPanel.get("temp_inout").add(inputLabel);
		RootPanel.get("temp_inout").add(inputTxtBox);
		RootPanel.get("temp_inout").add(button);
		RootPanel.get("temp_inout").add(outputLabel);
		RootPanel.get("temp_inout").add(outputConsole);
		
		RootPanel.get("app_area").add(displayFileNameLabel);
		RootPanel.get("app_area").add(fileNameToDisplayTextBox);
		RootPanel.get("app_area").add(displayFileButton);
		RootPanel.get("app_area").add(readFileButton);
		RootPanel.get("app_area").add(importHdlButton);
		
		displayFileNameLabel.getElement().setId("file_name_display");
		fileNameToDisplayTextBox.getElement().setId("file_input_txt_box");
		
		CodeEditor hdlViewer = new CodeEditor();
		CodeEditorPresenter codeEditorPresenter = CodeEditorPresenter.getCodeEditorPresenterInstance(hdlViewer);
		RootPanel.get("app_area").add(hdlViewer);
	
		
		presenter = ProjectView.getInstance();
		ConnectionsView connectionViewer = ConnectionsView.getInstance();
		
		SignalBusDebuggerUI debugUI= new SignalBusDebuggerUI();
		SignalBusDebuggerPresenter debuggerPresenter = SignalBusDebuggerPresenter.getSignalBusDebuggerPresenterInstance(debugUI);
	}

}
