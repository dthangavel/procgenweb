/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Connection;
import com.fyp.procGenWeb.client.procGen.electronics.core.ConnectionManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsToHdlConverter;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusConstantMapping;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusUniqueLocater;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.logicHelper.UnconnectedPortException;

import de.upb.hni.vmagic.AssociationElement;
import de.upb.hni.vmagic.VhdlFile;
import de.upb.hni.vmagic.builtin.StdLogic1164;
import de.upb.hni.vmagic.concurrent.ComponentInstantiation;
import de.upb.hni.vmagic.concurrent.ConcurrentStatement;
import de.upb.hni.vmagic.concurrent.ConditionalSignalAssignment;
import de.upb.hni.vmagic.declaration.BlockDeclarativeItem;
import de.upb.hni.vmagic.declaration.Component;
import de.upb.hni.vmagic.declaration.SignalDeclaration;
import de.upb.hni.vmagic.expression.Expression;
import de.upb.hni.vmagic.libraryunit.Architecture;
import de.upb.hni.vmagic.libraryunit.Entity;
import de.upb.hni.vmagic.libraryunit.LibraryClause;
import de.upb.hni.vmagic.libraryunit.LibraryUnit;
import de.upb.hni.vmagic.libraryunit.UseClause;
import de.upb.hni.vmagic.literal.CharacterLiteral;
import de.upb.hni.vmagic.literal.StringLiteral;
import de.upb.hni.vmagic.object.Constant;
import de.upb.hni.vmagic.object.Signal;
import de.upb.hni.vmagic.object.VhdlObjectProvider;
import de.upb.hni.vmagic.output.VhdlOutput;
import de.upb.hni.vmagic.util.VhdlCollections;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class ElectronicsToVhdlConverter implements ElectronicsToHdlConverter{
	
	// This keeps track of whether ports have been connected or left open
	private HashMap<String,Boolean> unconnectedPortTracker = new HashMap<String,Boolean>();
	EntityToVhdlConverter entityToVhdlConverter  = new EntityToVhdlConverter();
	Project project;
	Signal clk;
	
	int componentInstanceCount = 0;
	HashMap<String,String> generatedVhdlCode = new HashMap<String,String>(); // key is the entity name, value is the converted vhdl code
	public ElectronicsToVhdlConverter(Project project){
		this.project = project;
	}
	
	public HashMap<String,String> convertProjectToHdl(Project project) throws ProcGenException {
		generatedVhdlCode.clear();
		VhdlFile outputFile = createVhdlFileForBaseProject(project);
		String convertedVhdl = VhdlOutput.toVhdlString(outputFile);
		generatedVhdlCode.put(project.getName(), convertedVhdl);
		return generatedVhdlCode;
	}

	/*
	 * Adds vhdl header elements
	 * 
	 */
	private VhdlFile createBasicVhdlFile() {
		VhdlFile file = new VhdlFile();

		// Add Elements
		file.getElements().add(new LibraryClause("IEEE"));
		file.getElements().add(StdLogic1164.USE_CLAUSE);
		file.getElements().add(new UseClause("ieee.std_logic_unsigned.all"));

		return file;
	}

	/*
	 * This method converts the project into an entity
	 */
	private VhdlFile createVhdlFileForBaseProject(Project project) throws ProcGenException {

		VhdlFile file = this.createBasicVhdlFile();
		Entity hostProjectEntity = createBaseProject(project);
		file.getElements().add(hostProjectEntity);
		file.getElements().add(
				createProjectArchitecture(hostProjectEntity, project));
		return file;
	}

	private Entity createVMagicEntityForProgenEntity(
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity entityToConvert) {
		Entity entity = new Entity(entityToConvert.getName() + "_"
				+ entityToConvert.getIdForHdlCode());
		for (SignalBus inputSignal : entityToConvert.getInputPortList()) {
			Signal input = null;
			if (inputSignal.getBusWidth() > 1) {
				input = new Signal(
						entityToConvert.getName() + "_" + entityToConvert.getIdForHdlCode() + "_"  + inputSignal.getName(),
						Signal.Mode.IN,
						StdLogic1164.STD_LOGIC_VECTOR(inputSignal.getBusWidth()));
			} else if (inputSignal.getBusWidth() == 1) {
				input = new Signal(entityToConvert.getName() + "_" + entityToConvert.getIdForHdlCode() + "_" + inputSignal.getName(), Signal.Mode.IN,
						StdLogic1164.STD_LOGIC);
			}

			if (input != null) {
				entity.getPort().add(input);
			}
		}
		
		for (SignalBus outputSignal : entityToConvert.getOutputPortList()) {
			Signal output = null;
			if (outputSignal.getBusWidth() > 1) {
				output = new Signal(
						entityToConvert.getName() + "_" + entityToConvert.getIdForHdlCode() + "_" + outputSignal.getName(),
						Signal.Mode.OUT,
						StdLogic1164.STD_LOGIC_VECTOR(outputSignal.getBusWidth()));
			} else if (outputSignal.getBusWidth() == 1) {
				output = new Signal(entityToConvert.getName() + "_" + entityToConvert.getIdForHdlCode() + "_" + outputSignal.getName(), Signal.Mode.OUT,
						StdLogic1164.STD_LOGIC);
			}

			if (output != null) {
				entity.getPort().add(output);
			}
		}

		
		return entity;
	}

	/*
	 * creates the architecture for the entity representing entire project
	 */
	private LibraryUnit createProjectArchitecture(Entity entity,
			Project project) throws ProcGenException {

		EntityManager em = project.getEntityManager();
		Architecture architecture = new Architecture("rtl", entity);

		List<com.fyp.procGenWeb.client.procGen.electronics.core.Entity> baseEntitiesList = em
				.getBaseEntities();
		
		for (com.fyp.procGenWeb.client.procGen.electronics.core.Entity baseEntity : baseEntitiesList) {
			// for entities that are wanted in a separate file -component declarations needed
			convertEntityToVhdl(architecture,entityToVhdlConverter, baseEntity);
			
			convertAllConnectionsInHomeProject(project, architecture, baseEntity);	
		}
		
		// connect the entities to project clock
		for(SignalBusUniqueLocater sigLocater:this.project.getProjectSimulator().getSignalBusListConnectedToClock()){
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity destEntity = this.project.getEntityManager().getEntityById(sigLocater.getEntityId());
			SignalBus port = destEntity.getSignalBusByName(sigLocater.getSignalBusName());
			String signalName = destEntity.getName() + "_" + destEntity.getIdForHdlCode() + "_" + sigLocater.getSignalBusName();
			
			Signal signal;
			
			if(port.getBusWidth() > 1){
				signal = new Signal(signalName,StdLogic1164.STD_LOGIC_VECTOR(port.getBusWidth()));
			}
			
			else{
				signal = new Signal(signalName,StdLogic1164.STD_LOGIC);
			}
			
			ConditionalSignalAssignment newAssignment = new ConditionalSignalAssignment(signal, clk);
			architecture.getStatements().add(newAssignment);
		}

		return architecture;
	}

	private void convertEntityToVhdl(Architecture architecture, EntityToVhdlConverter entityToVhdlConverter,
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity baseEntity) throws ProcGenException {
		
		// create a new file if the type is Separate Entity
		
		if(baseEntity.getHdlConversionType().equals(HdlConversionType.SeparateEntity))
		{
			// recursive function that creates vhdl file for all sub-entities
			createVhdlFileForEntity(baseEntity);
			Entity vMagicEntity = this
					.createVMagicEntityForProgenEntity(baseEntity);
			Component componentDeclaration = new Component(vMagicEntity);

			// component instantiations
			ComponentInstantiation compInstance = new ComponentInstantiation(
					componentDeclaration.getIdentifier() + "_" +(++componentInstanceCount), componentDeclaration);
			
			for (VhdlObjectProvider<Signal> sProvider : compInstance.getComponent().getPort()) {
	            List<Signal> signals = VhdlCollections.getAll(sProvider.getVhdlObjects(), Signal.class);
	            for (Signal s : signals) {
	                compInstance.getPortMap().add(new AssociationElement(s.getIdentifier(), s));
	            }

	        }
			
			architecture.getDeclarations().add(componentDeclaration);
			architecture.getStatements().add(compInstance);
		}
		
		// Write in the same file if the type is Inline conversion
		
		else if(baseEntity.getHdlConversionType().equals(HdlConversionType.InlineConversion)){
			
			Object elementToAdd= baseEntity.convertToHdl(entityToVhdlConverter);
			
			if(elementToAdd != null){
				for(ConcurrentStatement statementToAdd:(List<ConcurrentStatement>)elementToAdd)
					architecture.getStatements().add(statementToAdd);
			}
			
			List<BlockDeclarativeItem> declarationItemsToAddList = this.getOtherBlockDeclarationsNeededForEntityCreation();
			
			if (declarationItemsToAddList != null) {
				// add declarations that have to be added for the enitity
				// creation
				for (BlockDeclarativeItem declarationItem : declarationItemsToAddList) {
					architecture.getDeclarations().add(declarationItem);
				}
			}
			
			entityToVhdlConverter.clearBlockDeclarativeItems();
		}
		
		for(SignalBus port: baseEntity.getInputPortList()){
			String signalName = baseEntity.getName() + "_" + baseEntity.getIdForHdlCode() + "_" + port.getName();
			Signal signal = null;
			if(port.getBusWidth() > 1){
				signal = new Signal(signalName,StdLogic1164.STD_LOGIC_VECTOR(port.getBusWidth()));
			}
			
			else{
				signal = new Signal(signalName,StdLogic1164.STD_LOGIC);
			}
			
			
			SignalBusUniqueLocater sigBusLocater = new SignalBusUniqueLocater(baseEntity.getId(),port.getName());
			if(this.project.getProjectSimulator().hasInitialValue(sigBusLocater)){
				int value = this.project.getProjectSimulator().getInitialValue(baseEntity.getId(),port.getName());
				if(port.getBusWidth() >1){
					signal.setDefaultValue(new StringLiteral(SignalBus.getStringForInteger(value,port.getBusWidth())));
				}
				else{
					signal.setDefaultValue(new CharacterLiteral(Integer.toBinaryString(value).toCharArray()[0]));
				}
			}
			
			SignalDeclaration newSignalDeclarationToAdd = new SignalDeclaration(signal);
			architecture.getDeclarations().add(newSignalDeclarationToAdd);
		}
		
		for(SignalBus port: baseEntity.getOutputPortList()){
			String signalName = baseEntity.getName() + "_" + baseEntity.getIdForHdlCode() + "_" + port.getName();
			Signal signal = null;
			if(port.getBusWidth() > 1){
				signal = new Signal(signalName,StdLogic1164.STD_LOGIC_VECTOR(port.getBusWidth()));
			}
			
			else{
				signal = new Signal(signalName,StdLogic1164.STD_LOGIC);
			}
			
			SignalBusUniqueLocater sigBusLocater = new SignalBusUniqueLocater(baseEntity.getId(),port.getName());
			if(this.project.getProjectSimulator().hasInitialValue(sigBusLocater)){
				int value = this.project.getProjectSimulator().getInitialValue(baseEntity.getId(),port.getName());
				if(port.getBusWidth() >1){
					signal.setDefaultValue(new StringLiteral(SignalBus.getStringForInteger(value,port.getBusWidth())));
				}
				else{
					signal.setDefaultValue(new CharacterLiteral(Integer.toBinaryString(value).toCharArray()[0]));
				}
			}

			SignalDeclaration newSignalDeclarationToAdd = new SignalDeclaration(signal);
			architecture.getDeclarations().add(newSignalDeclarationToAdd);
		}
		
		convertAllConstantConnectionsInEntityToVhdl(baseEntity,architecture);
	}

	public void convertAllConnectionsInHomeProject(Project project,
			Architecture architecture,
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity baseEntity) throws ProcGenException {
		// convert connection to signal assignment
		ConnectionManager cm = project.getConnectionManager();
		List<Connection> allConnectionsList = cm.getAllConnectionsInEntityAsList(baseEntity.getId());
		List<ConditionalSignalAssignment> vhdlStatementsList = convertConnectionToVhdl(allConnectionsList,project);
		for(ConditionalSignalAssignment assignment: vhdlStatementsList){
			architecture.getStatements().add(assignment);
		}
	}

	private List<ConditionalSignalAssignment> convertConnectionToVhdl(List<Connection> allConnectionsList,Project project) throws ProcGenException {
	
		EntityManager em = project.getEntityManager();
		List<ConditionalSignalAssignment> assignmentList = new ArrayList<ConditionalSignalAssignment>();
		for(Connection connectionToConvert: allConnectionsList){
			
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity sourceEntity = em.getEntityById(connectionToConvert.getSourceEntityId());
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity destinationEntity = em.getEntityById(connectionToConvert.getDestinationEntityId());

			if(sourceEntity == null){
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND + connectionToConvert.getSourceEntityId());
			}
			
			if(destinationEntity == null){
				throw new ProcGenException(Consts.ExceptionMessages.ENTITY_NOT_FOUND + connectionToConvert.getDestinationEntityId());
			}
			
			
			String sourceSignalName = sourceEntity.getName() + "_"  + connectionToConvert.getSourceEntityId().replace("-", "_") + "_" + connectionToConvert.getInputSignal().getName();
			String destinationSignalName = destinationEntity.getName() + "_" + connectionToConvert.getDestinationEntityId().replace("-", "_") + "_" + connectionToConvert.getOutputSignal().getName();
			
			Signal sourceSignal = new Signal(sourceSignalName,null);
			Signal destinationSignal = new Signal(destinationSignalName,null);
			
			ConditionalSignalAssignment signalAssignment = new ConditionalSignalAssignment(
					destinationSignal,sourceSignal);
			assignmentList.add(signalAssignment);
//			VhdlOutput.print(signalAssignment);
			
			// update that the port has been connected
			unconnectedPortTracker.put(connectionToConvert.getSourceEntityId() + "-" + connectionToConvert.getInputSignal().getName(), false);
			unconnectedPortTracker.put(connectionToConvert.getDestinationEntityId() + "-" + connectionToConvert.getOutputSignal().getName(), false);
		}
		
		return assignmentList;
		
	}

	private void createVhdlFileForEntity(
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity entityToConvert) throws ProcGenException {

		VhdlFile file = this.createBasicVhdlFile();
		Entity vMagicEntity = this
				.createVMagicEntityForProgenEntity(entityToConvert);
		file.getElements().add(vMagicEntity);
		// file.getElements().add(createArchitecture(hostProjectEntity,project,outputFolderPath));

		Architecture architecture = new Architecture("rtl", vMagicEntity);
		for (com.fyp.procGenWeb.client.procGen.electronics.core.Entity childEntity:entityToConvert.getChildEntityList()){

			convertEntityToVhdl(architecture,
					entityToVhdlConverter, childEntity);
		}
		
		convertAllConnectionsInEntityToVhdl(entityToConvert,architecture);
		//convertAllConstantConnectionsInEntityToVhdl(entityToConvert,architecture);
		file.getElements().add(architecture);
		
		try{
			// check whether all ports have been connected and resolve them if they are not connected
			resolveUnconnectedSignals(entityToConvert);
		}
		
		catch(UnconnectedPortException e){
			// TODO : ask the user interface to handle the problem
		}
		
		String convertedVhdl = VhdlOutput.toVhdlString(file);
		this.generatedVhdlCode.put(entityToConvert.getName() + "_"+entityToConvert.getIdForHdlCode(), convertedVhdl);

	}

	private void convertAllConstantConnectionsInEntityToVhdl(
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity entityToConvert,
			Architecture architecture) throws ProcGenException {
		com.fyp.procGenWeb.client.procGen.electronics.core.Entity parentEntity = entityToConvert.getParent();
		
		ConnectionManager cm;
		if(parentEntity != null){
		cm = parentEntity.getEntityConnectionManager();
		}else{
			cm = this.project.getConnectionManager();
		}
			List<SignalBusConstantMapping> list = cm.getConstantConnectionsInEntityAsList(entityToConvert.getId());
			if(list == null)
				return;
			for(SignalBusConstantMapping mapping:list){
				String mappingName = mapping.getSignalBusName();
				int value = mapping.getSignalBusValue();
				
				String valueToSet;
				valueToSet = Integer.toBinaryString(value);
				
				if(valueToSet.isEmpty()){
					throw new ProcGenException(Consts.ExceptionMessages.CANNOT_BE_EMPTY);
				}

				String valueOfExactLength = SignalBus.getStringForInteger(value,mapping.getSignalBusWidth());
				
				Expression exp = mapping.getSignalBusWidth()>1? new StringLiteral(valueOfExactLength): new CharacterLiteral(valueToSet.charAt(0));
				Signal destinationSignal = new Signal(entityToConvert.getName() + "_" + entityToConvert.getIdForHdlCode() + "_" + mappingName,null);
				
				ConditionalSignalAssignment signalAssignment = new ConditionalSignalAssignment(
						destinationSignal,exp);
				architecture.getStatements().add(signalAssignment);
			}
				
	}

	private void convertAllConnectionsInEntityToVhdl(
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity entityToConvert,
			Architecture architecture) throws ProcGenException {
		
		ConnectionManager cm = entityToConvert.getEntityConnectionManager();
		List<Connection> allConnectionsList = cm.getAllConnectionsInEntityAsList(entityToConvert.getId());
		
		// add connections details about children
		for(com.fyp.procGenWeb.client.procGen.electronics.core.Entity childEntity: entityToConvert.getChildEntityList()){
			allConnectionsList.addAll(cm.getAllConnectionsInEntityAsList(childEntity.getId()));
		}
		
		List<ConditionalSignalAssignment> vhdlStatementsList = convertConnectionToVhdl(allConnectionsList,project);
		for(ConditionalSignalAssignment assignment: vhdlStatementsList){
			architecture.getStatements().add(assignment);
		}

		
	}

	private Entity createBaseProject(Project project) {

		Entity entity = new Entity(project.getName());
		clk = new Signal("CLK", Signal.Mode.IN,
				StdLogic1164.STD_LOGIC);
		entity.getPort().add(clk);
		return entity;
	}

	private void resolveUnconnectedSignals(com.fyp.procGenWeb.client.procGen.electronics.core.Entity entityToCheck) throws ProcGenException{
		
		assert(entityToCheck != null);
		List<String> unconnectedPortNames = new ArrayList<String>();
		
		for(String portName :entityToCheck.getAllPortsName()){
			if(unconnectedPortTracker.containsKey(entityToCheck.getId() + "-" + portName));
				boolean isPortConnected = true;
			if(!isPortConnected){
				unconnectedPortNames.add(portName);
			}
		}
		
		if(unconnectedPortNames.size() > 0){
			throw new UnconnectedPortException(unconnectedPortNames.size() + Consts.ExceptionMessages.UNCONNECTED_INPUT_FOUND_IN + entityToCheck.getName(),unconnectedPortNames);
		}
	}
	
	private List<BlockDeclarativeItem> getOtherBlockDeclarationsNeededForEntityCreation(){
		return this.entityToVhdlConverter.getBlockDeclarativeItems();
	}
}
