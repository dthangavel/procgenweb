/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity.EntityTriggerType;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Adder;
import com.fyp.procGenWeb.client.procGen.electronics.entities.AndGate;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Comparator;
import com.fyp.procGenWeb.client.procGen.electronics.entities.ComplexMultiplexer;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit.MemoryUnitType;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Multiplexer;
import com.fyp.procGenWeb.client.procGen.electronics.entities.OrGate;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Register;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusJoint;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusSplitter;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConverter;

import de.upb.hni.vmagic.AssociationElement;
import de.upb.hni.vmagic.Choice;
import de.upb.hni.vmagic.Choices;
import de.upb.hni.vmagic.Range;
import de.upb.hni.vmagic.Range.Direction;
import de.upb.hni.vmagic.WaveformElement;
import de.upb.hni.vmagic.builtin.Standard;
import de.upb.hni.vmagic.builtin.StdLogic1164;
import de.upb.hni.vmagic.builtin.StdLogicUnsigned;
import de.upb.hni.vmagic.concurrent.ConcurrentStatement;
import de.upb.hni.vmagic.concurrent.ConditionalSignalAssignment;
import de.upb.hni.vmagic.concurrent.ConditionalSignalAssignment.ConditionalWaveformElement;
import de.upb.hni.vmagic.concurrent.ProcessStatement;
import de.upb.hni.vmagic.concurrent.SelectedSignalAssignment;
import de.upb.hni.vmagic.concurrent.SelectedSignalAssignment.SelectedWaveform;
import de.upb.hni.vmagic.declaration.BlockDeclarativeItem;
import de.upb.hni.vmagic.declaration.ConstantDeclaration;
import de.upb.hni.vmagic.declaration.FunctionDeclaration;
import de.upb.hni.vmagic.declaration.SignalDeclaration;
import de.upb.hni.vmagic.expression.Add;
import de.upb.hni.vmagic.expression.Aggregate;
import de.upb.hni.vmagic.expression.Aggregate.ElementAssociation;
import de.upb.hni.vmagic.expression.And;
import de.upb.hni.vmagic.expression.Concatenate;
import de.upb.hni.vmagic.expression.Equals;
import de.upb.hni.vmagic.expression.Expression;
import de.upb.hni.vmagic.expression.Expressions;
import de.upb.hni.vmagic.expression.FunctionCall;
import de.upb.hni.vmagic.expression.Or;
import de.upb.hni.vmagic.expression.Parentheses;
import de.upb.hni.vmagic.literal.CharacterLiteral;
import de.upb.hni.vmagic.literal.StringLiteral;
import de.upb.hni.vmagic.object.ArrayElement;
import de.upb.hni.vmagic.object.Constant;
import de.upb.hni.vmagic.object.Signal;
import de.upb.hni.vmagic.object.Slice;
import de.upb.hni.vmagic.statement.IfStatement;
import de.upb.hni.vmagic.statement.SequentialStatement;
import de.upb.hni.vmagic.statement.SignalAssignment;
import de.upb.hni.vmagic.type.ConstrainedArray;
import de.upb.hni.vmagic.type.SubtypeIndication;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class EntityToVhdlConverter implements HdlConverter {

	List<BlockDeclarativeItem> declarationsToAdd = new ArrayList<BlockDeclarativeItem>();

	@Override
	public Object convertAndGate(AndGate andGate) {

		List<ConcurrentStatement> expressionToReturn = new ArrayList<ConcurrentStatement>();
		
		int noOfInputs = andGate.getNumberOfInputs();

		SignalBus input1 = andGate.getInputByName("input0");
		String input1Name = andGate.getName() + "_" + andGate.getIdForHdlCode()
				+ "_" + input1.getName();
		Signal andInput1 = new Signal(input1Name, null,
				input1.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input1
						.getBusWidth()) : StdLogic1164.STD_LOGIC);

		And andStatement = null;

		for (int i = 1; i < noOfInputs; i++) {

			SignalBus input2 = andGate.getInputByName("input" + i);

			String input2Name = andGate.getName() + "_"
					+ andGate.getIdForHdlCode() + "_" + input2.getName();

			Signal andInput2 = new Signal(input2Name, null,
					input2.getBusWidth() > 1 ? StdLogic1164
							.STD_LOGIC_VECTOR(input2.getBusWidth())
							: StdLogic1164.STD_LOGIC);

			if (andStatement == null)
				andStatement = new And(andInput1, andInput2);

			else
				andStatement = new And(andStatement, andInput2);

		}

		SignalBus output1 = andGate.getOutputPortList().get(0);
		String outputName = andGate.getName() + "_" + andGate.getIdForHdlCode()
				+ "_" + output1.getName();
		Signal andOutput1 = new Signal(outputName, null,
				output1.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(output1.getBusWidth())
						: StdLogic1164.STD_LOGIC);

		ConditionalSignalAssignment andGateAssignment = new ConditionalSignalAssignment(
				andOutput1, andStatement);
		
		expressionToReturn.add(andGateAssignment);
		return expressionToReturn;
	}

	@Override
	public Object convertMux(Multiplexer multiplexer) {
		List<ConcurrentStatement> expressionToReturn = new ArrayList<ConcurrentStatement>();

		SignalBus selectionBus = multiplexer.getInputByName("selectionInput");
		SignalBus outputBus = multiplexer.getOutputPortList().get(0);
		int inputOutputWidth = outputBus.getBusWidth();

		String selectionBusName = multiplexer.getName() + "_"
				+ multiplexer.getIdForHdlCode() + "_" + selectionBus.getName();
		String outputBusName = multiplexer.getName() + "_"
				+ multiplexer.getIdForHdlCode() + "_" + outputBus.getName();

		Signal selectionSignal = new Signal(selectionBusName, null,
				selectionBus.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(selectionBus.getBusWidth())
						: StdLogic1164.STD_LOGIC);
		Signal outputBusSignal = new Signal(outputBusName, null,
				outputBus.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(outputBus.getBusWidth())
						: StdLogic1164.STD_LOGIC);

		SelectedSignalAssignment selectStatement = new SelectedSignalAssignment(
				selectionSignal, outputBusSignal);

		HashMap<String, String> muxMapping = multiplexer.getMuxMapping();

		Iterator<String> itr = muxMapping.keySet().iterator();
		while (itr.hasNext()) {
			String selectionInput = itr.next();
			String signalName = muxMapping.get(selectionInput);

			String inputBusName = multiplexer.getName() + "_"
					+ multiplexer.getIdForHdlCode() + "_" + signalName;

			Signal input0BusSignal = new Signal(inputBusName, null,
					inputOutputWidth > 1 ? StdLogic1164
							.STD_LOGIC_VECTOR(inputOutputWidth)
							: StdLogic1164.STD_LOGIC);

			List<WaveformElement> wv = new ArrayList<WaveformElement>();
			List<Choice> choiceList = new ArrayList<Choice>();

			wv.add(new WaveformElement(input0BusSignal));
			Choice c = null;

			if (selectionBus.getBusWidth() > 1) {
				c = new StringLiteral(selectionInput);
			} else {
				c = new CharacterLiteral(selectionInput.charAt(0));
			}
			choiceList.add(c);

			SelectedWaveform choiceWaveForm = new SelectedWaveform(wv,
					choiceList);

			selectStatement.getSelectedWaveforms().add(choiceWaveForm);
		}

		if (muxMapping.size() < (1 << selectionBus.getBusWidth())) {
			List<WaveformElement> wv = new ArrayList<WaveformElement>();
			List<Choice> choiceList = new ArrayList<Choice>();
			Choice c = Choices.OTHERS;
			choiceList.add(c);
			WaveformElement waveForm = new WaveformElement(
					StdLogic1164.STD_LOGIC_U);
			wv.add(waveForm);

			SelectedWaveform choiceWaveForm = new SelectedWaveform(wv,
					choiceList);
			selectStatement.getSelectedWaveforms().add(choiceWaveForm);
		}
		
		expressionToReturn.add(selectStatement);

		return expressionToReturn;
	}

	@Override
	public Object convertOrGate(OrGate orGate) {

		List<ConcurrentStatement> expressionToReturn = new ArrayList<ConcurrentStatement>();
		
		int noOfInputs = orGate.getNumberOfInputs();

		SignalBus input1 = orGate.getInputByName("input0");
		String input1Name = orGate.getName() + "_" + orGate.getIdForHdlCode()
				+ "_" + input1.getName();
		Signal andInput1 = new Signal(input1Name, null,
				input1.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input1
						.getBusWidth()) : StdLogic1164.STD_LOGIC);

		Or orStatement = null;

		for (int i = 1; i < noOfInputs; i++) {

			SignalBus input2 = orGate.getInputByName("input" + i);

			String input2Name = orGate.getName() + "_"
					+ orGate.getIdForHdlCode() + "_" + input2.getName();

			Signal andInput2 = new Signal(input2Name, null,
					input2.getBusWidth() > 1 ? StdLogic1164
							.STD_LOGIC_VECTOR(input2.getBusWidth())
							: StdLogic1164.STD_LOGIC);

			if (orStatement == null)
				orStatement = new Or(andInput1, andInput2);

			else
				orStatement = new Or(orStatement, andInput2);

		}

		SignalBus output1 = orGate.getOutputPortList().get(0);
		String outputName = orGate.getName() + "_" + orGate.getIdForHdlCode()
				+ "_" + output1.getName();
		Signal andOutput1 = new Signal(outputName, null,
				output1.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(output1.getBusWidth())
						: StdLogic1164.STD_LOGIC);

		ConditionalSignalAssignment orGateAssignment = new ConditionalSignalAssignment(
				andOutput1, orStatement);
		
		expressionToReturn.add(orGateAssignment);
		return expressionToReturn;
	}

	@Override
	public Object convertRegister(Register register) {

		List<ConcurrentStatement> expressionToReturn = new ArrayList<ConcurrentStatement>();
		
		SignalBus input0 = register.getInputByName("input0");
		SignalBus enable = register.getInputByName("enable");
		SignalBus clock = register.getInputByName("clock");
		SignalBus output = register.getOutputByName("output");

		String input0Name = register.getName() + "_"
				+ register.getIdForHdlCode() + "_" + input0.getName();
		String enableName = register.getName() + "_"
				+ register.getIdForHdlCode() + "_" + enable.getName();
		String clockName = register.getName() + "_"
				+ register.getIdForHdlCode() + "_" + clock.getName();
		String outputName = register.getName() + "_"
				+ register.getIdForHdlCode() + "_" + output.getName();

		Signal regInput0 = new Signal(input0Name, null,
				input0.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input0
						.getBusWidth()) : StdLogic1164.STD_LOGIC);
		Signal regEnable = new Signal(enableName, null);
		Signal regClock = new Signal(clockName, null);
		Signal regOutput = new Signal(outputName, null,
				output.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(output
						.getBusWidth()) : StdLogic1164.STD_LOGIC);

		Expression clkCondition;
		if (register.getEntityTriggerType() == EntityTriggerType.RISING_EDGE_TRIGGERED)
			clkCondition = Expressions.risingEdge(regClock);
		else
			clkCondition = Expressions.fallingEdge(regClock);

		ProcessStatement process = new ProcessStatement(register.getName()
				+ "_" + register.getIdForHdlCode() + "_" + "process");

		IfStatement statement = new IfStatement(clkCondition);

		Expression writeEnableCondition = new Equals(regEnable,
				StdLogic1164.STD_LOGIC_1);
		IfStatement writeEnableIf = new IfStatement(writeEnableCondition);

		SequentialStatement signalAssignment = new SignalAssignment(regOutput,
				regInput0);
		writeEnableIf.getStatements().add(signalAssignment);
		statement.getStatements().add(writeEnableIf);

		process.getSensitivityList().add(regClock);
		process.getStatements().add(statement);

		expressionToReturn.add(process);
		return expressionToReturn;
	}

	@Override
	public Object convertMemoryUnit(MemoryUnit memoryUnit) {
		int storageSize = memoryUnit.getSizeOfStorage();
		int inputSize = memoryUnit.getSizeOfInput();
		int outputBusWidth = memoryUnit.getOutputBusWidth();
		boolean isRam = false;
		// TODO: ROM/RAM name not working when using convert to integer

		List<ConcurrentStatement> expressionToReturn = new ArrayList<ConcurrentStatement>();
		
		SubtypeIndication type = outputBusWidth >1? StdLogic1164.STD_LOGIC_VECTOR(outputBusWidth):StdLogic1164.STD_LOGIC;
		
		// TODO : Check if the type declaration has already been done
		ConstrainedArray array = new ConstrainedArray("MEM_"
				+ memoryUnit.getIdForHdlCode() + "_" + storageSize + "x" + outputBusWidth,
				type, new Range(
						0, Range.Direction.TO,storageSize - 1 ));
		this.declarationsToAdd.add(array);

		Signal newSignalInRam = null;
		Constant newRomInstance = null;
		if (memoryUnit.getMemoryUnitType().equals(MemoryUnitType.Ram)) {
			newSignalInRam = new Signal(memoryUnit.getName() + "_"
					+ memoryUnit.getIdForHdlCode(), array);
			
			if(memoryUnit.getStorage().size()>0){
				 Aggregate s = new Aggregate();
				for (String storageValue : memoryUnit.getStorage()) {
					Expression exp;
					if(outputBusWidth >1){
						exp = new StringLiteral(storageValue);
					}
					else{
						exp = new CharacterLiteral(storageValue.charAt(0));
					}
					s.createAssociation(exp);
				}
				newSignalInRam.setDefaultValue(s);
			}
			
			SignalDeclaration newSignalDeclaration = new SignalDeclaration(
					newSignalInRam);
			
			
			
			this.declarationsToAdd.add(newSignalDeclaration);
			isRam = true;
		}

		else if (memoryUnit.getMemoryUnitType().equals(MemoryUnitType.Rom)) {

			ArrayList<ElementAssociation> expList = new ArrayList<ElementAssociation>();
			Aggregate ag = new Aggregate();
			
			List<String> contents =  memoryUnit.getStorage();
			
			for(String content:contents){
				Expression exp;
				if(outputBusWidth >1){
					exp = new StringLiteral(content);
				}
				else{
					exp = new CharacterLiteral(content.charAt(0));
				}
				ag.createAssociation(exp);
			}
			
			//ag.createAssociation(Aggregate.OTHERS(new HexLiteral(4)));
			
			//expList.add(new ElementAssociation(Aggregate.OTHERS(new HexLiteral(4))));
						
			newRomInstance = new Constant(memoryUnit.getName() + "_"
					+ memoryUnit.getIdForHdlCode(), array,
					ag);

			
			ConstantDeclaration newDeclaration = new ConstantDeclaration(
					newRomInstance);

			this.declarationsToAdd.add(newDeclaration);
			isRam = false;
		}
		
		// The assignments that have to be added to the architecture
		SignalBus input1 = memoryUnit.getInputByName("inputAddress");
		String input1Name = memoryUnit.getName() + "_" + memoryUnit.getIdForHdlCode()
				+ "_" + input1.getName();
		
		
		SignalBus output1 = memoryUnit.getOutputPortList().get(0);
		String outputName = memoryUnit.getName() + "_" + memoryUnit.getIdForHdlCode()
				+ "_" + output1.getName();
		
		Signal memoryUnitInput1 = new Signal(input1Name, null,
				input1.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input1
						.getBusWidth()) : StdLogic1164.STD_LOGIC);
		
		Signal memoryOutput1 = new Signal(outputName, null,
				output1.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(output1.getBusWidth())
						: StdLogic1164.STD_LOGIC);
		
		AssociationElement asocElement = new AssociationElement(memoryUnitInput1);
		ArrayList<AssociationElement> Asoclist = new ArrayList<AssociationElement>();
		Asoclist.add(asocElement);
		
		FunctionDeclaration fDeclaration = new FunctionDeclaration("CONV_INTEGER", Standard.INTEGER);
		FunctionCall f = new FunctionCall(fDeclaration);
		f.getParameters().add(asocElement);
		
		ArrayElement arrayElement;
		if(isRam){
			arrayElement = new ArrayElement(newSignalInRam,f);
		}
		else{
			arrayElement = new ArrayElement(newRomInstance,f);
		}
		WaveformElement wv = new WaveformElement(arrayElement);
		
		List<WaveformElement> list = new ArrayList<WaveformElement>();
		list.add(wv);
		ConditionalWaveformElement cwf = new ConditionalWaveformElement(list);
		ConditionalSignalAssignment outputSignalAssignment = new ConditionalSignalAssignment(
				memoryOutput1, cwf);
		
		expressionToReturn.add(outputSignalAssignment);
		
		if(isRam){
		
			assignDataToBeWrittenToRam(memoryUnit, expressionToReturn,
					newSignalInRam);
		}
		
		addExtraInputOutputMemoryVhdlStatements(memoryUnit, isRam,
				expressionToReturn, newSignalInRam, newRomInstance);
		
		return expressionToReturn;
	}

	/**
	 * @param memoryUnit
	 * @param expressionToReturn
	 * @param arrayElement
	 */
	private void assignDataToBeWrittenToRam(MemoryUnit memoryUnit,
			List<ConcurrentStatement> expressionToReturn,Signal newSignalInRam) {
		
		SignalBus input1 = memoryUnit.getInputByName("writeAddress");
		String input1Name = memoryUnit.getName() + "_" + memoryUnit.getIdForHdlCode()
				+ "_" + input1.getName();
		
		Signal memoryUnitInput1 = new Signal(input1Name, null,
				input1.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input1
						.getBusWidth()) : StdLogic1164.STD_LOGIC);
		
		AssociationElement asocElement = new AssociationElement(memoryUnitInput1);
		ArrayList<AssociationElement> Asoclist = new ArrayList<AssociationElement>();
		Asoclist.add(asocElement);
		
		FunctionDeclaration fDeclaration = new FunctionDeclaration("CONV_INTEGER", Standard.INTEGER);
		FunctionCall f = new FunctionCall(fDeclaration);
		f.getParameters().add(asocElement);
		
		ArrayElement arrayElement = new ArrayElement(newSignalInRam,f);;
		
		SignalBus inputData = memoryUnit.getInputByName("inputData");
		String inputDataName = memoryUnit.getName() + "_" + memoryUnit.getIdForHdlCode()
				+ "_" + inputData.getName();
		Signal memoryUnitInputData = new Signal(inputDataName, null,
				inputData.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(inputData
						.getBusWidth()) : StdLogic1164.STD_LOGIC);
		
		
		SignalBus enable = memoryUnit.getInputByName("enable");
		String enableName = memoryUnit.getName() + "_" + memoryUnit.getIdForHdlCode()
				+ "_" + enable.getName();
		Signal memoryUnitInputEnable = new Signal(enableName, null,
				enable.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(enable
						.getBusWidth()) : StdLogic1164.STD_LOGIC);
		
		WaveformElement ramDataAssignWaveform = new WaveformElement(memoryUnitInputData);
		List<WaveformElement> wvList = new ArrayList<WaveformElement>();
		wvList.add(ramDataAssignWaveform);
		
		Equals cond = new Equals(memoryUnitInputEnable, new CharacterLiteral('1'));
		
		ConditionalWaveformElement conditionalWaveForm = new ConditionalWaveformElement(wvList, cond);
		ConditionalSignalAssignment ramDataAssignment =  new ConditionalSignalAssignment(
				arrayElement, conditionalWaveForm);
		expressionToReturn.add(ramDataAssignment);
	}

	/**
	 * @param memoryUnit
	 * @param isRam
	 * @param expressionToReturn
	 * @param newSignalInRam
	 * @param newRomInstance
	 * @param fDeclaration
	 * @param f
	 * @param list
	 */
	private void addExtraInputOutputMemoryVhdlStatements(MemoryUnit memoryUnit,
			boolean isRam, List<ConcurrentStatement> expressionToReturn,
			Signal newSignalInRam, Constant newRomInstance)
			 {
		for(int i = 0;i<memoryUnit.getNumberOfExtraInOut();i++){
			SignalBus extraInput = memoryUnit.getInputByName("inputAddress" + (i + 1));
			String extraInputName = memoryUnit.getName() + "_" + memoryUnit.getIdForHdlCode()
					+ "_" + extraInput.getName();
			
			
			SignalBus extraOutput = memoryUnit.getOutputByName("output" + (i+1));
			String extraOutputName = memoryUnit.getName() + "_" + memoryUnit.getIdForHdlCode()
					+ "_" + extraOutput.getName();
			
			Signal memoryExtaInput = new Signal(extraInputName, null,
					extraInput.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(extraInput
							.getBusWidth()) : StdLogic1164.STD_LOGIC);
			
			Signal memoryExtraOutput = new Signal(extraOutputName, null,
					extraOutput.getBusWidth() > 1 ? StdLogic1164
							.STD_LOGIC_VECTOR(extraOutput.getBusWidth())
							: StdLogic1164.STD_LOGIC);
			
			AssociationElement asocExtraElement = new AssociationElement(memoryExtaInput);
			ArrayList<AssociationElement> AsocExtralist = new ArrayList<AssociationElement>();
			AsocExtralist.add(asocExtraElement);
			
			FunctionDeclaration fExtraDeclaration = new FunctionDeclaration("CONV_INTEGER", Standard.INTEGER);
			FunctionCall fExtra = new FunctionCall(fExtraDeclaration);
			fExtra.getParameters().add(asocExtraElement);
			
			ArrayElement arrayExtraElement;
			if(isRam){
				arrayExtraElement = new ArrayElement(newSignalInRam,fExtra);
			}
			else{
				arrayExtraElement = new ArrayElement(newRomInstance,fExtra);
			}
			WaveformElement wvExtra = new WaveformElement(arrayExtraElement);
			
			List<WaveformElement> listExtra = new ArrayList<WaveformElement>();
			listExtra.add(wvExtra);
			ConditionalWaveformElement cwfExtra = new ConditionalWaveformElement(listExtra);
			ConditionalSignalAssignment outputExtraSignalAssignment = new ConditionalSignalAssignment(
					memoryExtraOutput, cwfExtra);
			
			expressionToReturn.add(outputExtraSignalAssignment);
		}
	}

	public List<BlockDeclarativeItem> getBlockDeclarativeItems() {
		return this.declarationsToAdd;
	}

	public void clearBlockDeclarativeItems() {
		this.declarationsToAdd = new ArrayList<BlockDeclarativeItem>();
	}

	@Override
	public Object convertAdder(Adder adder) {
		
		List<ConcurrentStatement> expressionToReturn = new ArrayList<ConcurrentStatement>();
		
		SignalBus input0 = adder.getInputByName("input0");
		String input0Name = adder.getName() + "_" + adder.getIdForHdlCode()
				+ "_" + input0.getName();
		Signal adderInput0 = new Signal(input0Name, null,
				input0.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input0
						.getBusWidth()) : StdLogic1164.STD_LOGIC);

		SignalBus input1 = adder.getInputByName("input1");
		String input1Name = adder.getName() + "_" + adder.getIdForHdlCode()
				+ "_" + input1.getName();
		Signal adderInput1 = new Signal(input1Name, null,
				input1.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input1
						.getBusWidth()) : StdLogic1164.STD_LOGIC);

		Parentheses left = new Parentheses(new Concatenate(
				StdLogic1164.STD_LOGIC_0, adderInput0));
		Parentheses right = new Parentheses(new Concatenate(
				StdLogic1164.STD_LOGIC_0, adderInput1));

		Add addExp = new Add(left, right);
		if (adder.getInputPortList().size() == 3) { // carry_in is present
			SignalBus input2 = adder.getInputByName("input2");
			String input2Name = adder.getName() + "_" + adder.getIdForHdlCode()
					+ "_" + input2.getName();
			Signal adderInput2 = new Signal(input2Name, null,
					input2.getBusWidth() > 1 ? StdLogic1164
							.STD_LOGIC_VECTOR(input2.getBusWidth())
							: StdLogic1164.STD_LOGIC);

			addExp = new Add(addExp, adderInput2);
		}

		int maxSumSize = Math.max(input0.getBusWidth(),
				input1.getBusWidth());
		Signal wideSum = new Signal("S_wider", null,
				StdLogic1164.STD_LOGIC_VECTOR(maxSumSize + 1));

		ConditionalSignalAssignment wideSumAssignment = new ConditionalSignalAssignment(
				wideSum, addExp);

		this.declarationsToAdd.add(new SignalDeclaration(wideSum));

		SignalBus output1 = adder.getOutputByName("output");
		String outputName = adder.getName() + "_" + adder.getIdForHdlCode()
				+ "_" + output1.getName();
		Signal adderOutput = new Signal(outputName, null,
				output1.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(output1.getBusWidth())
						: StdLogic1164.STD_LOGIC);
		
		SignalBus carryOut = adder.getOutputByName("carry");
		String carryOutName = adder.getName() + "_" + adder.getIdForHdlCode()
				+ "_" + carryOut.getName();
		Signal carry = new Signal(carryOutName, null,
				carryOut.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(carryOut.getBusWidth())
						: StdLogic1164.STD_LOGIC);

		Expression outputSlice;
		if(maxSumSize > 1){
		outputSlice =  new Slice<Signal>(wideSum,new Range(maxSumSize-1, Direction.DOWNTO, 0));
		}else{
			outputSlice =  new ArrayElement<Signal>(wideSum,maxSumSize-1);
		}
		ArrayElement<Signal> carrySlice = new ArrayElement<Signal>(wideSum,maxSumSize);
		
		ConditionalSignalAssignment outputAssignment = new ConditionalSignalAssignment(
				adderOutput, outputSlice);
		
		ConditionalSignalAssignment carryAssignment = new ConditionalSignalAssignment(
				carry, carrySlice);
		
		expressionToReturn.add(wideSumAssignment);
		expressionToReturn.add(outputAssignment);
		expressionToReturn.add(carryAssignment);
		
		return expressionToReturn;
	}

	@Override
	public Object convertSignalBusSplitter(SignalBusSplitter signalBusSplitter) {
		
		List<ConcurrentStatement> expressionToReturn = new ArrayList<ConcurrentStatement>();
		
		SignalBus input0 = signalBusSplitter.getInputByName("input0");
		String input0Name = signalBusSplitter.getName() + "_" + signalBusSplitter.getIdForHdlCode()
				+ "_" + input0.getName();
		Signal signalBusSplitterInput0 = new Signal(input0Name, null,
				input0.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input0
						.getBusWidth()) : StdLogic1164.STD_LOGIC);
		
		int start = signalBusSplitter.getStartingIndex();
		
		SignalBus output1 = signalBusSplitter.getOutputPortList().get(0);
		String outputName = signalBusSplitter.getName() + "_" + signalBusSplitter.getIdForHdlCode()
				+ "_" + output1.getName();
		Signal signalBusSplitterOutput1 = new Signal(outputName, null,
				output1.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(output1.getBusWidth())
						: StdLogic1164.STD_LOGIC);
		
		assert(output1.getBusWidth() <= input0.getBusWidth() - start);
		ConditionalSignalAssignment outputAssignment = null;
		
		if (output1.getBusWidth() > 1) {
			Slice<Signal> outputSlice = new Slice<Signal>(
					signalBusSplitterInput0, new Range(input0.getBusWidth()
							- start - 1, Direction.DOWNTO, input0.getBusWidth()
							- start-signalBusSplitter.getOutputWidth()));
			outputAssignment = new ConditionalSignalAssignment(
					signalBusSplitterOutput1, outputSlice);
		}
		
		else{
			ArrayElement<Signal> carrySlice = new ArrayElement<Signal>(signalBusSplitterInput0,start);
			outputAssignment = new ConditionalSignalAssignment(
					signalBusSplitterOutput1, carrySlice);
		}
		expressionToReturn.add(outputAssignment);
		return expressionToReturn;
	}

	@Override
	public Object convertSignalBusJoint(SignalBusJoint signalBusJoint) {
		List<ConcurrentStatement> expressionToReturn = new ArrayList<ConcurrentStatement>();
		
		SignalBus input0 = signalBusJoint.getInputByName("input0");
		String input0Name = signalBusJoint.getName() + "_" + signalBusJoint.getIdForHdlCode()
				+ "_" + input0.getName();
		
		Signal signalBusJointInput0 = new Signal(input0Name, null,
				input0.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input0
						.getBusWidth()) : StdLogic1164.STD_LOGIC);
		
		SignalBus input1 = signalBusJoint.getInputByName("input1");
		String input1Name = signalBusJoint.getName() + "_" + signalBusJoint.getIdForHdlCode()
				+ "_" + input1.getName();
		Signal signalBusJointInput1 = new Signal(input1Name, null,
				input1.getBusWidth() > 1 ? StdLogic1164.STD_LOGIC_VECTOR(input1
						.getBusWidth()) : StdLogic1164.STD_LOGIC);
		
		
		SignalBus output1 = signalBusJoint.getOutputByName("output");
		String outputName = signalBusJoint.getName() + "_" + signalBusJoint.getIdForHdlCode()
				+ "_" + output1.getName();
		Signal signalBusJointOutput = new Signal(outputName, null,
				output1.getBusWidth() > 1 ? StdLogic1164
						.STD_LOGIC_VECTOR(output1.getBusWidth())
						: StdLogic1164.STD_LOGIC);
		
		Concatenate concatExpression = new Concatenate(signalBusJointInput0, signalBusJointInput1);
		
		ConditionalSignalAssignment outputAssignment = new ConditionalSignalAssignment(
				signalBusJointOutput, concatExpression);
		
		expressionToReturn.add(outputAssignment);
		
		return expressionToReturn;
	}

	@Override
	public Object convertComparator(Comparator comparator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object convertComplexMultiplexer(
			ComplexMultiplexer complexMultiplexer) {
		// TODO Auto-generated method stub
		return null;
	}
}
