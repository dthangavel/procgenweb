/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport;

import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.ConstantValue;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.ProcGenType;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusUniqueLocater;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Adder;
import com.fyp.procGenWeb.client.procGen.electronics.entities.AndGate;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Comparator;
import com.fyp.procGenWeb.client.procGen.electronics.entities.OrGate;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusJoint;
import com.fyp.procGenWeb.client.procGen.electronics.entities.SignalBusSplitter;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectToConstant;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

import de.upb.hni.vmagic.DiscreteRange;
import de.upb.hni.vmagic.Range;
import de.upb.hni.vmagic.expression.BinaryExpression;
import de.upb.hni.vmagic.expression.Expression;
import de.upb.hni.vmagic.expression.ExpressionKind;
import de.upb.hni.vmagic.expression.ExpressionVisitor;
import de.upb.hni.vmagic.expression.Literal;
import de.upb.hni.vmagic.expression.Name;
import de.upb.hni.vmagic.expression.Parentheses;
import de.upb.hni.vmagic.literal.CharacterLiteral;
import de.upb.hni.vmagic.literal.DecimalLiteral;
import de.upb.hni.vmagic.literal.StringLiteral;
import de.upb.hni.vmagic.object.ArrayElement;
import de.upb.hni.vmagic.object.Constant;
import de.upb.hni.vmagic.object.Signal;
import de.upb.hni.vmagic.object.Slice;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class ExpressionProcessor extends ExpressionVisitor {

	Project activeProject;
	Entity parentEntity;
	private ProcGenType processedType = null;

	VMagicFileProcessor fileProcessor;

	/**
	 * @param concurrentStatementProcessor
	 * 
	 */
	public ExpressionProcessor(Project activeProject,
			VMagicFileProcessor fileProcessor, Entity parentEntity) {
		this.activeProject = activeProject;
		this.fileProcessor = fileProcessor;
		this.parentEntity = parentEntity;
	}

	protected void visitBinaryExpression(BinaryExpression expression) {

		Project currentProject = this.activeProject;

		ProcGenType leftProcessedType;
		ProcGenType rightProcessedType;

		visit(expression.getLeft());

		leftProcessedType = this.processedType;

		visit(expression.getRight());

		rightProcessedType = this.processedType;

		// clear processedType for setting new type
		processedType = null;

		// both left and right side of the expression are constants
		if (leftProcessedType instanceof ConstantValue
				&& rightProcessedType instanceof ConstantValue) {

			try {
				evaluateExpressionType(expression.getExpressionKind(),
						(ConstantValue) leftProcessedType,
						(ConstantValue) rightProcessedType);
			} catch (Exception e) {

				e.printStackTrace();
			}
			return;
		}

		try {
			Entity newEntityToAdd = instantiateAppropriateEntity(
					expression.getExpressionKind(), leftProcessedType,
					rightProcessedType);

			this.processedType = newEntityToAdd;
		} catch (ProcGenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param entityIdLeft
	 * @param entityIdRight
	 * @param currentProject
	 * @param newEntityToAdd
	 * @return
	 * @throws ProcGenException
	 */
	private Entity establishConnectionAfterIdentifyingInputType(
			Project currentProject, ProcGenType leftProcessedType,
			ProcGenType rightProcessedType, Entity newEntityToAdd)
			throws ProcGenException {

		String idAfterAdding = "";

		if (newEntityToAdd != null) {
			EntityManager em = this.activeProject.getEntityManager();

			idAfterAdding = em.addEntity(newEntityToAdd);

			if (leftProcessedType instanceof Entity
					&& ((Entity) leftProcessedType).getId().length() > 0) {
				String entityIdLeft = ((Entity) leftProcessedType).getId();

				ConnectCommand newConnectCommandToExecute = new ConnectCommand();
				newConnectCommandToExecute.establishConnection(entityIdLeft,
						idAfterAdding, "output", "input0", currentProject);
			}

			else if (leftProcessedType instanceof ConstantValue) {
				Integer value = ((ConstantValue) leftProcessedType)
						.getIntegerValue();
				ConnectToConstant constantConnectionCommand = new ConnectToConstant();
				constantConnectionCommand.establishConstantConnection(
						currentProject, idAfterAdding, "input0",
						value.toString());
			}

			else if (leftProcessedType instanceof SignalBusUniqueLocater) {
				ConnectCommand newConnectCommandToExecute = new ConnectCommand();

				String sourceEntityId = ((SignalBusUniqueLocater) leftProcessedType)
						.getEntityId();
				String sourceSignal = ((SignalBusUniqueLocater) leftProcessedType)
						.getSignalBusName();

				newConnectCommandToExecute.establishConnection(sourceEntityId,
						idAfterAdding, sourceSignal, "input0", currentProject);
			}

			else if (leftProcessedType instanceof DisconnectedSignalBus) {
				if (!((DisconnectedSignalBus) leftProcessedType).isConstant) {
					SignalBusUniqueLocater newSignalBusLocater = new SignalBusUniqueLocater(
							idAfterAdding, "input0");
					this.fileProcessor.signalToBusMap.put(
							((DisconnectedSignalBus) leftProcessedType)
									.getOriginalVmagicSignal(),
							newSignalBusLocater);
				}
				else{// it is a constant 
					int value = ((DisconnectedSignalBus)leftProcessedType).getConstantValue();
							ConnectToConstant constantConnectionCommand = new ConnectToConstant();
					constantConnectionCommand.establishConstantConnection(
							currentProject, idAfterAdding, "input0",
							String.valueOf(value));		
				}
			}

			// ---------------------------------------------
			if (rightProcessedType instanceof Entity
					&& ((Entity) rightProcessedType).getId().length() > 0) {

				String entityIdRight = ((Entity) rightProcessedType).getId();

				ConnectCommand newConnectCommandToExecute = new ConnectCommand();
				newConnectCommandToExecute.establishConnection(entityIdRight,
						idAfterAdding, "output", "input1", currentProject);
			}

			else if (rightProcessedType instanceof ConstantValue) {
				Integer value = ((ConstantValue) rightProcessedType)
						.getIntegerValue();
				ConnectToConstant constantConnectionCommand = new ConnectToConstant();
				constantConnectionCommand.establishConstantConnection(
						currentProject, idAfterAdding, "input1",
						value.toString());
			}

			else if (rightProcessedType instanceof SignalBusUniqueLocater) {
				ConnectCommand newConnectCommandToExecute = new ConnectCommand();

				String sourceEntityId = ((SignalBusUniqueLocater) rightProcessedType)
						.getEntityId();
				String sourceSignal = ((SignalBusUniqueLocater) rightProcessedType)
						.getSignalBusName();

				newConnectCommandToExecute.establishConnection(sourceEntityId,
						idAfterAdding, sourceSignal, "input1", currentProject);
			}

			else if (rightProcessedType instanceof DisconnectedSignalBus) {
				//SignalBusUniqueLocater newSignalBusLocater = new  SignalBusUniqueLocater(idAfterAdding, "input1");
			//	this.fileProcessor.signalToBusMap.put(((DisconnectedSignalBus) rightProcessedType).getOriginalVmagicSignal(), newSignalBusLocater);
			
				if (!((DisconnectedSignalBus) rightProcessedType).isConstant) {
					SignalBusUniqueLocater newSignalBusLocater = new SignalBusUniqueLocater(
							idAfterAdding, "input1");
					this.fileProcessor.signalToBusMap.put(
							((DisconnectedSignalBus) rightProcessedType)
									.getOriginalVmagicSignal(),
							newSignalBusLocater);
				}
				else{// it is a constant 
					int value = ((DisconnectedSignalBus)rightProcessedType).getConstantValue();
							ConnectToConstant constantConnectionCommand = new ConnectToConstant();
					constantConnectionCommand.establishConstantConnection(
							currentProject, idAfterAdding, "input1",
							String.valueOf(value));		
				}
			
			}

		}

		return newEntityToAdd;
	}

	/**
	 * 
	 * @param currentProject
	 * @param inputProcessedType
	 * @param newEntityToAdd
	 * @return
	 * @throws ProcGenException
	 */
	private Entity establishConnectionAfterIdentifyingInputType(
			Project currentProject, ProcGenType inputProcessedType,
			Entity newEntityToAdd) throws ProcGenException {

		String idAfterAdding = "";

		if (newEntityToAdd != null) {
			EntityManager em = this.activeProject.getEntityManager();

			idAfterAdding = em.addEntity(newEntityToAdd);

			if (inputProcessedType instanceof Entity
					&& ((Entity) inputProcessedType).getId().length() > 0) {
				String entityIdLeft = ((Entity) inputProcessedType).getId();

				ConnectCommand newConnectCommandToExecute = new ConnectCommand();
				newConnectCommandToExecute.establishConnection(entityIdLeft,
						idAfterAdding, "output", "input0", currentProject);
			}

			else if (inputProcessedType instanceof ConstantValue) {
				Integer value = ((ConstantValue) inputProcessedType)
						.getIntegerValue();
				ConnectToConstant constantConnectionCommand = new ConnectToConstant();
				constantConnectionCommand.establishConstantConnection(
						currentProject, idAfterAdding, "input0",
						value.toString());
			}

			else if (inputProcessedType instanceof SignalBusUniqueLocater) {
				ConnectCommand newConnectCommandToExecute = new ConnectCommand();

				String sourceEntityId = ((SignalBusUniqueLocater) inputProcessedType)
						.getEntityId();
				String sourceSignal = ((SignalBusUniqueLocater) inputProcessedType)
						.getSignalBusName();

				newConnectCommandToExecute.establishConnection(sourceEntityId,
						idAfterAdding, sourceSignal, "input0", currentProject);
			}

			else if (inputProcessedType instanceof DisconnectedSignalBus) {
				if (!((DisconnectedSignalBus) inputProcessedType).isConstant) {
					SignalBusUniqueLocater newSignalBusLocater = new SignalBusUniqueLocater(
							idAfterAdding, "input0");
					this.fileProcessor.signalToBusMap.put(
							((DisconnectedSignalBus) inputProcessedType)
									.getOriginalVmagicSignal(),
							newSignalBusLocater);
				}
				else{// it is a constant 
					int value = ((DisconnectedSignalBus)inputProcessedType).getConstantValue();
							ConnectToConstant constantConnectionCommand = new ConnectToConstant();
					constantConnectionCommand.establishConstantConnection(
							currentProject, idAfterAdding, "input0",
							String.valueOf(value));		
				}
			}
		}

		return newEntityToAdd;
	}

	/**
	 * @param processedType
	 * @param width
	 * @return
	 * @throws ProcGenException
	 */
	private int getWidthFromProcessedType(ProcGenType processedType)
			throws ProcGenException {
		int width = 0;

		if (processedType instanceof Entity) {
			// entityIdLeft = ((Entity) leftProcessedType).getId();
			width = ((Entity) processedType).getOutputByName("output")
					.getBusWidth();
		}

		else if (processedType instanceof DisconnectedSignalBus) {
			width = ((SignalBus) processedType).getBusWidth();
		}

		else if (processedType instanceof SignalBusUniqueLocater) {
			EntityManager em = this.activeProject.getEntityManager();
			width = em
					.getEntityById(
							((SignalBusUniqueLocater) processedType)
									.getEntityId())
					.getSignalBusByName(
							((SignalBusUniqueLocater) processedType)
									.getSignalBusName()).getBusWidth();
		}

		else if (processedType instanceof ConstantValue) {
			Integer constantValue = ((ConstantValue) processedType)
					.getIntegerValue();
			width = ((ConstantValue) processedType).getStringLength();
		}
		return width;
	}

	private void evaluateExpressionType(ExpressionKind expressionKind,
			ConstantValue leftSideConstantValue,
			ConstantValue rightSideConstantValue) throws Exception {

		int leftSideIntegerValue = (leftSideConstantValue).getIntegerValue();
		int rightSideIntegerValue = (rightSideConstantValue).getIntegerValue();

		int result = 0;

		ConstantValue constantValue = null;

		switch (expressionKind) {
		case ABS:
			break;
		case AND:
			if (leftSideConstantValue.getStringLength() != rightSideConstantValue
					.getStringLength()) {
				throw new Exception("Bus lengths do not match");
			}
			result = leftSideIntegerValue & rightSideIntegerValue;
			constantValue = new ConstantValue(result,
					leftSideConstantValue.getStringLength());
			break;
		case CONCATENATE:
			try {
				String rightSideValue = Integer
						.toBinaryString(rightSideIntegerValue);
				String leftSideValue = Integer
						.toBinaryString(leftSideIntegerValue);
				String combinedString = leftSideValue + rightSideValue;

				result = Integer.parseInt(combinedString, 2);
				constantValue = new ConstantValue(result,
						combinedString.length());
			} catch (NumberFormatException e) {
				throw e;
			}
			break;
		case DIVIDE:
			// result = leftSideIntegerValue / rightSideIntegerValue;
			break;
		case EQUALS:
			break;
		case GREATER_EQUALS:
			break;
		case GREATER_THAN:
			break;
		case LESS_EQUALS:
			break;
		case LESS_THAN:
			break;
		case MINUS:
			result = leftSideIntegerValue - rightSideIntegerValue;
			constantValue = new ConstantValue(result, Math.max(
					leftSideConstantValue.getStringLength(),
					rightSideConstantValue.getStringLength()));
			break;
		case MOD:
			break;
		case MULTIPLY:
			result = leftSideIntegerValue * rightSideIntegerValue;
			constantValue = new ConstantValue(result,
					leftSideConstantValue.getStringLength()
							+ rightSideConstantValue.getStringLength());
			break;
		case NAND:
			break;
		case NOR:
			break;
		case NOT:
			break;
		case NOT_EQUALS:
			break;
		case OR:
			break;
		case PLUS:
			result = leftSideIntegerValue + rightSideIntegerValue;
			constantValue = new ConstantValue(result, Math.max(
					leftSideConstantValue.getStringLength(),
					rightSideConstantValue.getStringLength()));
			break;
		case POW:
			break;
		case REM:
			break;
		case ROL:
			break;
		case ROR:
			break;
		case SLA:
			break;
		case SLL:
			break;
		case SRA:
			break;
		case SRL:
			break;
		case XNOR:
			break;
		case XOR:
			break;
		default:
			break;
		}

		if (constantValue == null) {
			throw new Exception(Consts.ExceptionMessages.NOT_IMPLEMENTED + " "
					+ expressionKind.name());
		}

		this.processedType = constantValue;
	}

	private Entity instantiateAppropriateEntity(ExpressionKind expressionKind,
			ProcGenType leftProcessedType, ProcGenType rightProcessedType)
			throws Exception {

		// TODO: Implement other expressions once entity is implemented

		switch (expressionKind) {

		case ABS:
			break;
		case AND: {
			int inputBusWidth = validateBusWidthEqualityOnBothSides(
					leftProcessedType, rightProcessedType);

			if (inputBusWidth != -1 && inputBusWidth != 0) {
				Entity newEntityToAdd = new AndGate("", "AndGate", 2,
						inputBusWidth);

				if (parentEntity != null) {
					newEntityToAdd.setParent(parentEntity);
				}

				return establishConnectionAfterIdentifyingInputType(
						this.activeProject, leftProcessedType,
						rightProcessedType, newEntityToAdd);
			}
		}
			break;
		case CONCATENATE: {

			if (leftProcessedType == null || rightProcessedType == null) {
				throw new Exception(Consts.ExceptionMessages.IMPROPER_PARSING);
			}

			int leftSideWidth = getWidthFromProcessedType(leftProcessedType);
			int rightSideWidth = getWidthFromProcessedType(rightProcessedType);

			Entity newEntityToAdd = new SignalBusJoint("BusJoint",
					leftSideWidth, rightSideWidth);

			if (parentEntity != null) {
				newEntityToAdd.setParent(parentEntity);
			}
			return establishConnectionAfterIdentifyingInputType(
					this.activeProject, leftProcessedType, rightProcessedType,
					newEntityToAdd);
		}
		case DIVIDE:
			break;
		case EQUALS: {
			int inputBusWidth = validateBusWidthEqualityOnBothSides(
					leftProcessedType, rightProcessedType);

			if (inputBusWidth != -1) {
				Entity newEntityToAdd = new Comparator("EqualityCondition",
						Comparator.Type.Equals, inputBusWidth);

				if (parentEntity != null) {
					newEntityToAdd.setParent(parentEntity);
				}

				return establishConnectionAfterIdentifyingInputType(
						this.activeProject, leftProcessedType,
						rightProcessedType, newEntityToAdd);
			}
		}
			break;
		case GREATER_EQUALS: {
			int inputBusWidth = validateBusWidthEqualityOnBothSides(
					leftProcessedType, rightProcessedType);

			if (inputBusWidth != -1) {
				Entity newEntityToAdd = new Comparator("EqualityCondition",
						Comparator.Type.GreaterThanEquals, inputBusWidth);

				if (parentEntity != null) {
					newEntityToAdd.setParent(parentEntity);
				}

				return establishConnectionAfterIdentifyingInputType(
						this.activeProject, leftProcessedType,
						rightProcessedType, newEntityToAdd);
			}
		}
			break;
		case GREATER_THAN: {
			int inputBusWidth = validateBusWidthEqualityOnBothSides(
					leftProcessedType, rightProcessedType);

			if (inputBusWidth != -1) {
				Entity newEntityToAdd = new Comparator("EqualityCondition",
						Comparator.Type.GreaterThan, inputBusWidth);

				if (parentEntity != null) {
					newEntityToAdd.setParent(parentEntity);
				}

				return establishConnectionAfterIdentifyingInputType(
						this.activeProject, leftProcessedType,
						rightProcessedType, newEntityToAdd);
			}
		}
			break;
		case LESS_EQUALS: {
			int inputBusWidth = validateBusWidthEqualityOnBothSides(
					leftProcessedType, rightProcessedType);

			if (inputBusWidth != -1) {
				Entity newEntityToAdd = new Comparator("EqualityCondition",
						Comparator.Type.LessThanEquals, inputBusWidth);

				if (parentEntity != null) {
					newEntityToAdd.setParent(parentEntity);
				}

				return establishConnectionAfterIdentifyingInputType(
						this.activeProject, leftProcessedType,
						rightProcessedType, newEntityToAdd);
			}
		}
			break;
		case LESS_THAN: {
			int inputBusWidth = validateBusWidthEqualityOnBothSides(
					leftProcessedType, rightProcessedType);

			if (inputBusWidth != -1) {
				Entity newEntityToAdd = new Comparator("EqualityCondition",
						Comparator.Type.LessThan, inputBusWidth);

				if (parentEntity != null) {
					newEntityToAdd.setParent(parentEntity);
				}

				return establishConnectionAfterIdentifyingInputType(
						this.activeProject, leftProcessedType,
						rightProcessedType, newEntityToAdd);
			}
		}
			break;
		case MINUS:
			break;
		case MOD:
			break;
		case MULTIPLY:
			break;
		case NAND:
			break;
		case NOR:
			break;
		case NOT:
			break;
		case NOT_EQUALS:
			break;
		case OR: {
			int inputBusWidth = validateBusWidthEqualityOnBothSides(
					leftProcessedType, rightProcessedType);

			if (inputBusWidth != -1) {
				Entity newEntityToAdd = new OrGate("", "OrGate", 2,
						inputBusWidth);

				if (parentEntity != null) {
					newEntityToAdd.setParent(parentEntity);
				}

				return establishConnectionAfterIdentifyingInputType(
						this.activeProject, leftProcessedType,
						rightProcessedType, newEntityToAdd);
			}
		}
		case PLUS: {
			int leftSideWidth;
			int rightSideWidth;

			leftSideWidth = getWidthFromProcessedType(leftProcessedType);
			rightSideWidth = getWidthFromProcessedType(rightProcessedType);

			Entity newEntityToAdd = new Adder("Adder", 2, leftSideWidth,
					rightSideWidth, true);

			if (parentEntity != null) {
				newEntityToAdd.setParent(parentEntity);
			}

			return establishConnectionAfterIdentifyingInputType(
					this.activeProject, leftProcessedType, rightProcessedType,
					newEntityToAdd);
		}
		case POW:
			break;
		case REM:
			break;
		case ROL:
			break;
		case ROR:
			break;
		case SLA:
			break;
		case SLL:
			break;
		case SRA:
			break;
		case SRL:
			break;
		case XNOR:
			break;
		case XOR:
			break;
		default:
			break;
		}

		throw new Exception(Consts.ExceptionMessages.UNABLE_TO_PARSE_BINARY_EXP
				+ ": " + expressionKind.name());
	}

	/**
	 * @param leftProcessedType
	 * @param rightProcessedType
	 * @return
	 * @throws Exception
	 */
	private int validateBusWidthEqualityOnBothSides(
			ProcGenType leftProcessedType, ProcGenType rightProcessedType)
			throws Exception {

		if (leftProcessedType == null || rightProcessedType == null) {
			throw new Exception(Consts.ExceptionMessages.IMPROPER_PARSING);
		}

		int leftSideWidth;
		int rightSideWidth;

		leftSideWidth = getWidthFromProcessedType(leftProcessedType);
		rightSideWidth = getWidthFromProcessedType(rightProcessedType);

		if (leftSideWidth != rightSideWidth)
			// TODO: throw error ?
			return -1;
		return leftSideWidth;
	}

	@SuppressWarnings("rawtypes")
	protected void visitName(Name object) {
		if (object instanceof Signal) {
			Signal signalToProcess = (Signal) object;

			assert (this.fileProcessor.signalToBusMap
					.containsKey(signalToProcess));
			SignalBusUniqueLocater signalBusLocater = this.fileProcessor.signalToBusMap
					.get(signalToProcess);
			if (signalBusLocater != null) {
				this.processedType = signalBusLocater;
				return;
			}

			int busWidth = signalToProcess.getVhdlObjects().size();
			if(this.fileProcessor.disconnectedSignalBusMap.containsKey(object)){
				this.processedType = this.fileProcessor.disconnectedSignalBusMap.get(object);
			}
			else{
				this.processedType = new DisconnectedSignalBus(
					((Signal) object).getIdentifier(), busWidth,signalToProcess,false);
			}
		}

		if (object instanceof Constant) {
			Expression exp = ((Constant) object).getDefaultValue();
			this.visit(exp);
		}

		if (object instanceof Slice) {
			DiscreteRange sliceRange = ((Slice) object).getRange();
			if (sliceRange instanceof Range) {
				try {
					RangeProcessor rangeProc = new RangeProcessor(
							(Range) sliceRange, activeProject,
							this.fileProcessor, this.parentEntity);
					int busWidth = rangeProc.getBusWidthFromRange();
					int startingValue = rangeProc.getStartingIndex();
					int endingValue = rangeProc.getEndingIndex();

					Name prefixName = ((Slice) object).getPrefix();

					int widthOfInput = 0;
					this.visit(prefixName);
					if (processedType instanceof DisconnectedSignalBus) {
						widthOfInput = ((SignalBus) processedType)
								.getBusWidth();
					}

					else if (processedType instanceof SignalBusUniqueLocater) {
						EntityManager em = this.activeProject
								.getEntityManager();
						widthOfInput = em
								.getEntityById(
										((SignalBusUniqueLocater) processedType)
												.getEntityId())
								.getSignalBusByName(
										((SignalBusUniqueLocater) processedType)
												.getSignalBusName())
								.getBusWidth();
					}

					int index = 0;

					if (startingValue > endingValue) { // uses downto
						index = widthOfInput - startingValue - 1;
					} else { // increasing naming convention using to
						index = startingValue;
					}

					Entity newEntityToAdd = new SignalBusSplitter("Splitter",
							widthOfInput, busWidth, index, true);
					if (this.parentEntity != null) {
						newEntityToAdd.setParent(parentEntity);
					}

					this.establishConnectionAfterIdentifyingInputType(
							activeProject, processedType, newEntityToAdd);

					this.processedType = newEntityToAdd;
				} catch (ProcGenException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		if (object instanceof ArrayElement) {
			Name prefix = ((ArrayElement) object).getPrefix();
			List contents = ((ArrayElement) object).getIndices();
			Expression type = null;
			if (contents.size() > 0) {
				type = (Expression) contents.get(0);
			}
			this.visit(prefix);
			if (this.processedType instanceof SignalBusUniqueLocater) {
				SignalBusUniqueLocater correspondingSignalBus = (SignalBusUniqueLocater) this.processedType;
				this.visit(type);
				if (this.processedType instanceof ConstantValue) {
					ConstantValue signalIndex = (ConstantValue) this.processedType;

					EntityManager em = this.activeProject.getEntityManager();

					try {
						int widthOfInput = em
								.getEntityById(
										(correspondingSignalBus).getEntityId())
								.getSignalBusByName(
										(correspondingSignalBus)
												.getSignalBusName())
								.getBusWidth();

						Entity newEntityToAdd;
						int startingIndexInSignalBus = widthOfInput
								- signalIndex.getIntegerValue() - 1;
						assert (startingIndexInSignalBus >= 0);
						newEntityToAdd = new SignalBusSplitter("Splitter",
								widthOfInput, 1, startingIndexInSignalBus, true);
						if (this.parentEntity != null) {
							newEntityToAdd.setParent(parentEntity);
						}

						this.establishConnectionAfterIdentifyingInputType(
								activeProject, correspondingSignalBus,
								newEntityToAdd);
						this.processedType = newEntityToAdd;

					} catch (ProcGenException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	protected void visitLiteral(Literal expression) {
		if (expression instanceof DecimalLiteral) {
			String valueAsString = ((DecimalLiteral) expression).getValue();
			int value = Integer.parseInt(valueAsString);

			ConstantValue constValue = new ConstantValue(value,
					valueAsString.length());
			this.processedType = constValue;
		}

		if (expression instanceof CharacterLiteral) {
			String valueOfSignalBus = String
					.valueOf(((CharacterLiteral) expression).getCharacter());
			int value = Integer.parseInt(valueOfSignalBus);

			ConstantValue constValue = new ConstantValue(value,
					valueOfSignalBus.length());
			this.processedType = constValue;
		}

		if (expression instanceof StringLiteral) {
			String valueOfSignalBus = String.valueOf(expression);
			valueOfSignalBus = valueOfSignalBus.replace("\"", "");
			int value = Integer.parseInt(valueOfSignalBus, 2);

			ConstantValue constValue = new ConstantValue(value,
					valueOfSignalBus.length());
			this.processedType = constValue;
		}
	}

	protected void visitParentheses(Parentheses expression) {
		Expression valueExpression = expression.getExpression();
		this.visit(valueExpression);
	}

	public ProcGenType getProcessedType() {
		return processedType;
	}
}
