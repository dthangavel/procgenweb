/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

import de.upb.hni.vmagic.declaration.Component;
import de.upb.hni.vmagic.libraryunit.Entity;
import de.upb.hni.vmagic.parser.VhdlParserException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class VhdlToElectronicsConverter {

	Project newProject;
	HashMap<String,Boolean> isProcessed = new HashMap<String,Boolean>();
	HashMap<String,Component> processedEntities = new HashMap<String,Component>();
	
	public VhdlToElectronicsConverter(){
		newProject = new Project("NewProject");
	}
	
	public Project convertVhdlFilesToProcGen(HashMap<String,String> filesToConvert) throws IOException, VhdlParserException, ProcGenException{
		
		Set<String> fileNames = filesToConvert.keySet();
		for(String fileName:fileNames){
			this.isProcessed.put(fileName, false);
		}
		
		Iterator<String> itr = filesToConvert.keySet().iterator();
		
		while(itr.hasNext()){
			String fileName = itr.next();
			
			if (this.isProcessed.get(fileName) == false) {
				String vhdlStringToConvert = filesToConvert.get(fileName);
				
				VMagicFileProcessor fileProcessor = new  VMagicFileProcessor(this,fileName,filesToConvert);
				fileProcessor.processFile(vhdlStringToConvert,null);
				this.isProcessed.put(fileName,true);
			}
		}
		
		return newProject;
	}
	
}
