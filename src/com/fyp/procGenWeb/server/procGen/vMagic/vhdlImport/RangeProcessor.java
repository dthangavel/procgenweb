/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport;

import com.fyp.procGenWeb.client.procGen.electronics.core.ConstantValue;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

import de.upb.hni.vmagic.Range;
import de.upb.hni.vmagic.expression.Expression;
import de.upb.hni.vmagic.literal.DecimalLiteral;

/**
 * @author DINESH THANGAVEL
 *
 */
public class RangeProcessor{
		
		private Range range;
		private int startingIndex;
		public int getStartingIndex() {
			return startingIndex;
		}

		public int getEndingIndex() {
			return endingIndex;
		}

		private int endingIndex;
		
		VMagicFileProcessor fileProcessor;
		Entity parentEntity;
		
		RangeProcessor(Range range,Project activeProject,VMagicFileProcessor fileProcessor, Entity parentEntity) throws ProcGenException{
			
			this.fileProcessor = fileProcessor;
			this.parentEntity = parentEntity;		
			this.range = range;
			
			Expression fromExpression = range.getFrom();
			Expression toExpression = range.getTo();
			
			startingIndex = parseRangeExpression(fromExpression,activeProject);
			endingIndex = parseRangeExpression(toExpression,activeProject);
		}	
	
	  private Integer parseRangeExpression(Expression rangeExpression, Project activeProject) throws ProcGenException {
		if (rangeExpression instanceof DecimalLiteral) {
			return Integer.valueOf(((DecimalLiteral) rangeExpression)
					.getValue());
		}
		
		ExpressionProcessor expProcessor = new ExpressionProcessor(activeProject,this.fileProcessor,this.parentEntity);
		expProcessor.visit(rangeExpression);
		
		if(!(expProcessor.getProcessedType() instanceof ConstantValue))
			throw new ProcGenException(Consts.ExceptionMessages.UNABLE_TO_PARSE_VHDL);
		
		int value = ((ConstantValue)expProcessor.getProcessedType()).getIntegerValue();
		
		return value;
	}
	
	/**
	 * @param range
	 * @return
	 * @throws ProcGenException 
	 */
	  public int getBusWidthFromRange() {
		int signalBusWidth;

		signalBusWidth = (startingIndex - endingIndex) + 1;
		return signalBusWidth;
	}
	  
	  
}
