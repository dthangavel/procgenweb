/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport;

//import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.ConstantValue;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.logicHelper.InvalidSignalException;

import de.upb.hni.vmagic.object.Signal;

/**
 * @author DINESH THANGAVEL
 *
 */
public class DisconnectedSignalBus extends SignalBus {

	Signal signalId;
	boolean isConstant;
	int constantValue;
	
	/**
	 * @param name
	 * @param busWidth
	 */
	public DisconnectedSignalBus(String name, int busWidth,Signal originalVmagicSignal,boolean isConstant) {
		super(name, busWidth);
		this.signalId = originalVmagicSignal;
		this.isConstant = isConstant;
	}

	/**
	 * @param name
	 * @param busWidth
	 * @param valueToSet
	 */
	public DisconnectedSignalBus(String name, int busWidth,  int valueToSet,Signal originalVmagicSignal, boolean isConstant) {
		super(name, busWidth);
		this.signalId = originalVmagicSignal;
		this.isConstant = isConstant;
		this.constantValue = valueToSet;
	}

	
	public Signal getOriginalVmagicSignal(){
		return this.signalId;
	}

	public int getConstantValue() {
		return this.constantValue;
	}

}
