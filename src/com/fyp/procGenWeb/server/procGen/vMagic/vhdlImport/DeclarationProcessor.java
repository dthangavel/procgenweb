/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport;

import java.io.IOException;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

import de.upb.hni.vmagic.declaration.Component;
import de.upb.hni.vmagic.declaration.DeclarationVisitor;
import de.upb.hni.vmagic.declaration.SignalDeclaration;
import de.upb.hni.vmagic.declaration.VariableDeclaration;
import de.upb.hni.vmagic.object.Signal;
import de.upb.hni.vmagic.parser.VhdlParserException;

/**
 * @author DINESH THANGAVEL
 *
 */
public class DeclarationProcessor extends DeclarationVisitor{

	VMagicFileProcessor vMagicFileProcessor;
	Entity hostEntity;
	
	/**
	 * @param vMagicFileProcessor 
	 * @param parentEntity 
	 * 
	 */
	public DeclarationProcessor(VMagicFileProcessor vMagicFileProcessor, Entity parentEntity) {
		this.vMagicFileProcessor = vMagicFileProcessor;
		this.hostEntity = parentEntity;
	}

	/**
     * Visits a component declaration.
     * @param declaration the component declaration
     */
    protected void visitComponentDeclaration(Component declaration) {
    	String nameOfComponent = declaration.getIdentifier();
		String fileContents = this.vMagicFileProcessor.getFileList().get(nameOfComponent);
		
//		if(this.vMagicFileProcessor.checkIfComponentAlreadyProcessed(nameOfComponent)){
//			return;
//		}
		
		
		this.vMagicFileProcessor.electronicsConverter.processedEntities.put(nameOfComponent, declaration);			

    }
	
    /**
     * Visits a signal declaration.
     * @param declaration the signal declaration
     */
    protected void visitSignalDeclaration(SignalDeclaration declaration) {
    	List<Signal> signalList =  declaration.getObjects();
    	for(Signal declaredSignal:signalList){
    		int busWidth = declaredSignal.getVhdlObjects().size();
    		
    		this.vMagicFileProcessor.signalToBusMap.put(declaredSignal,null);
    	}
    }
    
    /**
     * Visits a variable declaration.
     * @param declaration the variable declaration
     */
    protected void visitVariableDeclaration(VariableDeclaration declaration) {
    	
    }
}
