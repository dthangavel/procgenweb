/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport;

import java.io.IOException;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.ConstantValue;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.ProcGenType;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBus;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusUniqueLocater;
import com.fyp.procGenWeb.client.procGen.electronics.entities.ComplexMultiplexer;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Multiplexer;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectCommand;
import com.fyp.procGenWeb.client.procGen.logic.commandslist.ConnectToConstant;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

import de.upb.hni.vmagic.Choice;
import de.upb.hni.vmagic.DiscreteRange;
import de.upb.hni.vmagic.Range;
import de.upb.hni.vmagic.WaveformElement;
import de.upb.hni.vmagic.concurrent.AbstractProcessStatement;
import de.upb.hni.vmagic.concurrent.ComponentInstantiation;
import de.upb.hni.vmagic.concurrent.ConcurrentStatementVisitor;
import de.upb.hni.vmagic.concurrent.ConditionalSignalAssignment;
import de.upb.hni.vmagic.concurrent.ConditionalSignalAssignment.ConditionalWaveformElement;
import de.upb.hni.vmagic.concurrent.SelectedSignalAssignment;
import de.upb.hni.vmagic.concurrent.SelectedSignalAssignment.SelectedWaveform;
import de.upb.hni.vmagic.declaration.ProcessDeclarativeItem;
import de.upb.hni.vmagic.expression.Aggregate;
import de.upb.hni.vmagic.expression.Expression;
import de.upb.hni.vmagic.literal.CharacterLiteral;
import de.upb.hni.vmagic.literal.StringLiteral;
import de.upb.hni.vmagic.object.ArrayElement;
import de.upb.hni.vmagic.object.RecordElement;
import de.upb.hni.vmagic.object.Signal;
import de.upb.hni.vmagic.object.SignalAssignmentTarget;
import de.upb.hni.vmagic.object.Slice;
import de.upb.hni.vmagic.parser.VhdlParserException;
import de.upb.hni.vmagic.statement.SequentialStatement;
import de.upb.hni.vmagic.type.IndexSubtypeIndication;
import de.upb.hni.vmagic.type.SubtypeIndication;

/**
 * @author DINESH THANGAVEL
 * 
 */
public class ConcurrentStatementProcessor extends ConcurrentStatementVisitor {

	VMagicFileProcessor vMagicFileProcessor;
	Entity hostEntity;

	/**
	 * @param vMagicFileProcessor
	 * @param parentEntity
	 * 
	 */
	public ConcurrentStatementProcessor(
			VMagicFileProcessor vMagicFileProcessor, Entity parentEntity) {
		this.vMagicFileProcessor = vMagicFileProcessor;
		this.hostEntity = parentEntity;
	}

	protected void visitProcessStatement(AbstractProcessStatement statement) {
		// this.hostConverter.addProcessStatementsToConverter(statement);
		List<Signal> sensitivitySignals = statement.getSensitivityList();
		List<ProcessDeclarativeItem> declarativeItems = statement.getDeclarations();
		List<SequentialStatement> seqStatementsList = statement.getStatements();
		
		for(SequentialStatement seqStatement:seqStatementsList){
			
		}
	}

	protected void visitComponentInstantiation(ComponentInstantiation statement) {

    	String nameOfComponent = statement.getComponent().getIdentifier();
		String fileContents = this.vMagicFileProcessor.getFileList().get(nameOfComponent);
		
//		if(this.vMagicFileProcessor.checkIfComponentAlreadyProcessed(nameOfComponent)){
//			return;
//		}
		
		VMagicFileProcessor newFileProcessor = new VMagicFileProcessor(this.vMagicFileProcessor.electronicsConverter,fileContents,this.vMagicFileProcessor.fileList);
		com.fyp.procGenWeb.client.procGen.electronics.core.Entity newChildEntity = null;
		try {
			// create an instantiation of the child component only when the instantiation is encountered
			newChildEntity = newFileProcessor.processFile(fileContents,this.hostEntity);
		} catch (IOException | VhdlParserException | ProcGenException e1) {
			e1.printStackTrace();
			return;
		}
		
		
		
		// this.hostConverter.addComponentInstantiationToConverter(statement);
//		if (!this.vMagicFileProcessor.electronicsConverter.processedEntities
//				.containsKey(statement.getComponent().getIdentifier())) {
//			try {
//				throw new Exception("Invalid situation");
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			
//		}

//		// add the entity
//		try {
//			this.vMagicFileProcessor.electronicsConverter.newProject
//					.getEntityManager().addEntity(newChildEntity);
//		} catch (ProcGenException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	/**
	 * Visits a conditional signal assignment.
	 * 
	 * @param statement
	 *            the statement
	 */
	protected void visitConditionalSignalAssignment(
			ConditionalSignalAssignment statement) {
		// this.hostConverter.addStatementAssignment(statement);

		SignalAssignmentTarget target = statement.getTarget();

		List<ConditionalWaveformElement> conditionalWaveFormList = statement
				.getConditionalWaveforms();

		if (conditionalWaveFormList.isEmpty()) {
			return;
		}

		/*
		 * Complex multiplexing is needed when we have more than one conditional
		 * waveforms or only one conditional waveform where condition is not
		 * null
		 */

		if (conditionalWaveFormList.size() > 1) {
			try {
				processComplexMulitplexing(target, conditionalWaveFormList);
			} catch (ProcGenException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}

		ConditionalWaveformElement condtionalWaveform = conditionalWaveFormList
				.get(0);
		if (condtionalWaveform.getCondition() != null) {
			try {
				processComplexMulitplexing(target, conditionalWaveFormList);
			} catch (ProcGenException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}

		List<WaveformElement> waveformList = condtionalWaveform.getWaveform();

		assert (waveformList.size() == 1);
		// wave form list contains the signals/elements on the right side of
		// signal assignment
		processWaveFormListForDirectConnection(target, waveformList);

		/*
		 * for(int i=0;i<waveform.size();i++){ Expression condition =
		 * waveform.get(i).getCondition(); List<WaveformElement> waveformList =
		 * waveform.get(i).getWaveform();
		 * 
		 * for(WaveformElement element:waveformList){ Expression exp =
		 * element.getValue(); ExpressionProcessor expProcessor = new
		 * ExpressionProcessor
		 * (this.vMagicFileProcessor.electronicsConverter.newProject
		 * ,this.vMagicFileProcessor,this.parentEntity);
		 * expProcessor.visit(exp);
		 * 
		 * if(expProcessor.getProcessedType() instanceof Entity){
		 * 
		 * Entity entityToConnect = (Entity) expProcessor.getProcessedType();
		 * 
		 * if(target instanceof Signal){
		 * if(this.vMagicFileProcessor.signalToBusMap.containsKey(target)){
		 * SignalBusUniqueLocater signalBusUniqueLocater =
		 * this.vMagicFileProcessor.signalToBusMap.get(target);
		 * if(signalBusUniqueLocater == null){ // target is a
		 * disconnectedSignalBus SignalBusUniqueLocater newSignalBusLocater =
		 * new SignalBusUniqueLocater(entityToConnect.getId(), "output");
		 * this.vMagicFileProcessor.signalToBusMap.put((Signal) target,
		 * newSignalBusLocater); }
		 * 
		 * else{ // left side of assignment refers to port of an entity
		 * ConnectCommand newConnectCommand = new ConnectCommand(); String
		 * sourceEntity = entityToConnect.getId(); String sourceSignal =
		 * "output"; try { newConnectCommand.establishConnection(sourceEntity,
		 * signalBusUniqueLocater.getEntityId(), sourceSignal,
		 * signalBusUniqueLocater.getSignalBusName(),
		 * this.vMagicFileProcessor.electronicsConverter.newProject); } catch
		 * (ProcGenException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } } } } }
		 * 
		 * } }
		 */

	}

	/**
	 * @param target
	 * @param waveformList
	 */
	private void processWaveFormListForDirectConnection(
			SignalAssignmentTarget target, List<WaveformElement> waveformList) {
		for (WaveformElement element : waveformList) {
			// right side condition
			Expression exp = element.getValue();
			ExpressionProcessor expProcessor = new ExpressionProcessor(
					this.vMagicFileProcessor.electronicsConverter.newProject,
					this.vMagicFileProcessor, this.hostEntity);
			expProcessor.visit(exp);

			if (expProcessor.getProcessedType() instanceof Entity) {
				processSourceEntityAndTargetSignalForDirectConnection(target,
						expProcessor);
			}
			if (expProcessor.getProcessedType() instanceof ConstantValue) {
				processSourceConstantAndTargetSignalForDirectConnection(target,
						expProcessor);
			}

		}
	}

	private void processWaveFormListForConditionalConnection(SignalAssignmentTarget target,ComplexMultiplexer cmplxMux, int dataIndex,List<WaveformElement> waveformList) throws Exception{
		if(waveformList.size()>1){
			throw new Exception("Unexpected size.. Look into this");
		}
		
		Project activeProject = this.vMagicFileProcessor.electronicsConverter.newProject;
		
		WaveformElement wv = waveformList.get(0);
		Expression exp = wv.getValue();
		ExpressionProcessor expProcessor = new ExpressionProcessor(
				this.vMagicFileProcessor.electronicsConverter.newProject,
				this.vMagicFileProcessor, this.hostEntity);
		expProcessor.visit(exp);
		
		ProcGenType type = expProcessor.getProcessedType();

		if ( type instanceof Entity) {
			// connect entity's output to input i of the complex multiplexer
			ConnectCommand command = new ConnectCommand();
			command.establishConnection(((Entity) type).getId(), cmplxMux.getId(), "output", "input"+dataIndex, activeProject);
		}

		if (type instanceof ConstantValue) {
			ConnectToConstant constantConnection = new ConnectToConstant();
			constantConnection.establishConstantConnection(activeProject, cmplxMux.getId(),"input"+dataIndex, String.valueOf(((ConstantValue) type).getIntegerValue()));
		}
	}
	
	private void processComplexMulitplexing(SignalAssignmentTarget target,
			List<ConditionalWaveformElement> conditionalWaveformList) throws ProcGenException {

		Project activeProject = this.vMagicFileProcessor.electronicsConverter.newProject;
		Project currentProjectInstance = activeProject;

		int selectionInputSize = conditionalWaveformList.size();

		if (selectionInputSize == 0)
			return;

		int inputDataSize = getSizeFromTarget(target);
		
		ComplexMultiplexer complexMux = new ComplexMultiplexer("ComplexMux",
				selectionInputSize, selectionInputSize,inputDataSize);

		complexMux.setParent(hostEntity);
		
		String id;

		try {
			id = activeProject.getEntityManager().addEntity(complexMux);
		} catch (ProcGenException e) {
			e.printStackTrace();
			return;
		}

		int selectionIndex = 0;
		for (ConditionalWaveformElement conditionElement : conditionalWaveformList) {

			Expression condition = conditionElement.getCondition();
			
			if(condition!= null){
			ExpressionProcessor expProcessor = new ExpressionProcessor(
					currentProjectInstance, vMagicFileProcessor, hostEntity);
			expProcessor.visit(condition);
			ProcGenType type = expProcessor.getProcessedType();

			if (type instanceof Entity) {
				// connect the entity's output to selection output
				ConnectCommand command = new ConnectCommand();
				try {
					command.establishConnection(((Entity) type).getId(), id,
							"output", "selectionInput" + selectionIndex,
							activeProject);
				} catch (ProcGenException e) {
					e.printStackTrace();
					return;
				}
			}
			}
			else{
				ConnectToConstant connectToConstant = new ConnectToConstant();
				connectToConstant.establishConstantConnection(currentProjectInstance, id, "selectionInput"+selectionIndex, "1");
			}

			// connect waveform to input
			List<WaveformElement> waveFormList = conditionElement.getWaveform();
			try {
				processWaveFormListForConditionalConnection(target, complexMux, selectionIndex, waveFormList);
			} catch (Exception e) {
				e.printStackTrace();
			}
			selectionIndex++;
		}
	}

	private int getSizeFromTarget(SignalAssignmentTarget target) {
		if(target instanceof Signal){
			SubtypeIndication subType = ((Signal) target).getType();
			List<DiscreteRange> ranges;
			if(subType instanceof IndexSubtypeIndication){
				ranges =  ((IndexSubtypeIndication) subType).getRanges();
				DiscreteRange dr = ranges.get(0);
				if(dr instanceof Range){
					ExpressionProcessor expProcessor = new ExpressionProcessor(
							this.vMagicFileProcessor.electronicsConverter.newProject, vMagicFileProcessor, hostEntity);
					Expression fromExp = ((Range) dr).getFrom();
					expProcessor.visit(fromExp);
					ProcGenType type = expProcessor.getProcessedType();
					int from = -1;
					if(type instanceof ConstantValue){
						from = ((ConstantValue) type).getIntegerValue();
					}
					
					int to  = -1;
					Expression toExp = ((Range) dr).getTo();
					expProcessor.visit(toExp);
					type = expProcessor.getProcessedType();
					if(type instanceof ConstantValue){
						to = ((ConstantValue) type).getIntegerValue();
					}
					
					if(from!=-1 && to !=-1){
						return Math.abs(from-to) + 1;
					}
				}
			}
			
			return ((Signal) target).getVhdlObjects().size();
		}
		
		else if(target instanceof Entity){
			return ((Entity) target).getInputByName("input0").getBusWidth();
		}
		return 0;
	}

	private void processSourceConstantAndTargetSignalForDirectConnection(
			SignalAssignmentTarget target, ExpressionProcessor expProcessor) {

		Integer value = ((ConstantValue) expProcessor.getProcessedType())
				.getIntegerValue();

		if (target instanceof Signal) {
			if (this.vMagicFileProcessor.signalToBusMap.containsKey(target)) {
				SignalBusUniqueLocater signalBusUniqueLocater = this.vMagicFileProcessor.signalToBusMap
						.get(target);
				if (signalBusUniqueLocater == null) {
					DisconnectedSignalBus disconnectedBus = new DisconnectedSignalBus(((Signal) target).getIdentifier(),this.getSizeFromTarget(target),value,(Signal) target,true);
					this.vMagicFileProcessor.disconnectedSignalBusMap.put((Signal) target, disconnectedBus);
				}

				else { // left side of assignment refers to port of an entity
					ConnectToConstant newConnectCommand = new ConnectToConstant();
					String destinationEntity = signalBusUniqueLocater
							.getEntityId();
					String destinationSignalName = signalBusUniqueLocater.getSignalBusName();

					Project currentProjectInstance = this.vMagicFileProcessor.electronicsConverter.newProject;
					try {
						newConnectCommand.establishConstantConnection(
								currentProjectInstance, destinationEntity,
								destinationSignalName, String.valueOf(value));
					} catch (ProcGenException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * @param target
	 * @param expProcessor
	 */
	private void processSourceEntityAndTargetSignalForDirectConnection(
			SignalAssignmentTarget target, ExpressionProcessor expProcessor) {
		Entity entityToConnect = (Entity) expProcessor.getProcessedType();

		if (target instanceof Signal) {
			if (this.vMagicFileProcessor.signalToBusMap.containsKey(target)) {
				SignalBusUniqueLocater signalBusUniqueLocater = this.vMagicFileProcessor.signalToBusMap
						.get(target);
				if (signalBusUniqueLocater == null) {
					// target is a disconnectedSignalBus

					SignalBusUniqueLocater newSignalBusLocater = new SignalBusUniqueLocater(
							entityToConnect.getId(), "output");
					this.vMagicFileProcessor.signalToBusMap.put(
							(Signal) target, newSignalBusLocater);
				}

				else { // left side of assignment refers to port of an entity
					ConnectCommand newConnectCommand = new ConnectCommand();
					String sourceEntity = entityToConnect.getId();
					String sourceSignal = "output";
					try {
						newConnectCommand
								.establishConnection(
										sourceEntity,
										signalBusUniqueLocater.getEntityId(),
										sourceSignal,
										signalBusUniqueLocater
												.getSignalBusName(),
										this.vMagicFileProcessor.electronicsConverter.newProject);
					} catch (ProcGenException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	private Object processAssignmentTarget(SignalAssignmentTarget target) {
		if (target instanceof Signal) {
			SignalBus signalBusToAssign = null;
			if (this.vMagicFileProcessor.signalToBusMap.containsKey(target)) {
				SignalBusUniqueLocater signalBusUniqueLocater = this.vMagicFileProcessor.signalToBusMap
						.get(target);
				if (signalBusUniqueLocater == null) {
					signalBusToAssign = new DisconnectedSignalBus(
							((Signal) target).getIdentifier(),
							this.getSizeFromTarget(target),(Signal)target,false);
					return signalBusToAssign;
				}

				return signalBusUniqueLocater;
			}

			else {
				// TODO: throw exception here
			}

			return signalBusToAssign;
		}

		else if (target instanceof Aggregate) {

		}

		else if (target instanceof ArrayElement) {

		}

		else if (target instanceof RecordElement) {

		}

		else if (target instanceof Slice) {

		}
		return null;
	}

	protected void visitSelectedSignalAssignment(
			SelectedSignalAssignment statement) {

		int selectionBusWidth = 1;
		int inputWidth = 1;
		String selectionEntityId = "";
		Multiplexer newMux = null;

		Expression exp = statement.getExpression();
		ExpressionProcessor processor = new ExpressionProcessor(
				this.vMagicFileProcessor.electronicsConverter.newProject,
				this.vMagicFileProcessor, this.hostEntity);
		processor.visit(exp);
		ProcGenType selectionType = processor.getProcessedType();

		if (selectionType == null)
			return;

		if (selectionType instanceof Entity) {
			assert (((Entity) selectionType).getOutputPortList().size() == 1);
			selectionBusWidth = ((Entity) selectionType).getOutputPortList()
					.get(0).getBusWidth();
			selectionEntityId = ((Entity) selectionType).getId();
		}

		else if (selectionType instanceof DisconnectedSignalBus) {
			selectionBusWidth = ((SignalBus) selectionType).getBusWidth();
		}

		else if (selectionType instanceof SignalBusUniqueLocater) {
			EntityManager em = this.vMagicFileProcessor.electronicsConverter.newProject
					.getEntityManager();
			try {
				selectionBusWidth = em
						.getEntityById(
								((SignalBusUniqueLocater) selectionType)
										.getEntityId())
						.getSignalBusByName(
								((SignalBusUniqueLocater) selectionType)
										.getSignalBusName()).getBusWidth();
			} catch (ProcGenException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		SignalAssignmentTarget target = statement.getTarget();
		Object processedTarget = this.processAssignmentTarget(target);

		if (processedTarget != null
				&& processedTarget instanceof DisconnectedSignalBus) {
			inputWidth = ((SignalBus) processedTarget).getBusWidth();
		}

		if (processedTarget instanceof SignalBusUniqueLocater) {
			EntityManager em = this.vMagicFileProcessor.electronicsConverter.newProject
					.getEntityManager();
			try {
				selectionBusWidth = em
						.getEntityById(
								((SignalBusUniqueLocater) selectionType)
										.getEntityId())
						.getSignalBusByName(
								((SignalBusUniqueLocater) selectionType)
										.getSignalBusName()).getBusWidth();
			} catch (ProcGenException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			newMux = new Multiplexer("Mux", selectionBusWidth, inputWidth);

			if (hostEntity != null) {
				newMux.setParent(hostEntity);
			}

			String muxId = this.vMagicFileProcessor.electronicsConverter.newProject
					.getEntityManager().addEntity(newMux);

			List<SelectedWaveform> selectedWaveformsList = statement
					.getSelectedWaveforms();
			int i = 0;
			for (SelectedWaveform selectedWaveform : selectedWaveformsList) {

				List<Choice> choiceList = selectedWaveform.getChoices();
				assert (choiceList.size() == 1);

				List<WaveformElement> waveformElementList = selectedWaveform
						.getWaveform();

				for (WaveformElement waveformElement : waveformElementList) {
					String choiceAsString = getStringFromChoice(choiceList
							.get(0));
					Expression expressionToSet = waveformElement.getValue();

					processor.visit(expressionToSet);
					ProcGenType expressionType = processor.getProcessedType();
					if (expressionType instanceof SignalBus) {
						newMux.programMux(choiceAsString, "input" + i);
					}

					else if (expressionType instanceof Entity) {
						// establish connection
						String arguments = ((Entity) expressionType).getId()
								+ " " + muxId + " " + "output" + " " + "input"
								+ i + " " + "default";
						ConnectCommand newConnectCommandToExecute = new ConnectCommand();
						newConnectCommandToExecute.execute(arguments);
					}

					i++;

				}
			}

			if (selectionEntityId.length() > 0) {
				ConnectCommand newConnectCommandToExecute = new ConnectCommand();
				newConnectCommandToExecute
						.establishConnection(
								selectionEntityId,
								muxId,
								"output",
								"selectionInput",
								this.vMagicFileProcessor.electronicsConverter.newProject);
			}

		} catch (ProcGenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String getStringFromChoice(Choice choice) {
		if (choice instanceof CharacterLiteral) {
			char charValue = ((CharacterLiteral) choice).getCharacter();

			char charArray[] = new char[1];
			charArray[0] = charValue;

			String convertedString = new String(charArray);
			return convertedString;
		}

		else if (choice instanceof StringLiteral) {
			return ((StringLiteral) choice).getString();
		}
		return null;
	}
}
