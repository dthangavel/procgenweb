/**
 * 
 */
package com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusUniqueLocater;
import com.fyp.procGenWeb.client.procGen.logicHelper.Consts;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

import de.upb.hni.vmagic.Range;
import de.upb.hni.vmagic.VhdlFile;
import de.upb.hni.vmagic.concurrent.ConcurrentStatement;
import de.upb.hni.vmagic.declaration.BlockDeclarativeItem;
import de.upb.hni.vmagic.declaration.DeclarativeItem;
import de.upb.hni.vmagic.libraryunit.Architecture;
import de.upb.hni.vmagic.libraryunit.Entity;
import de.upb.hni.vmagic.object.Signal;
import de.upb.hni.vmagic.object.VhdlObject.Mode;
import de.upb.hni.vmagic.object.VhdlObjectProvider;
import de.upb.hni.vmagic.parser.VhdlParser;
import de.upb.hni.vmagic.parser.VhdlParserException;
import de.upb.hni.vmagic.type.IndexSubtypeIndication;
import de.upb.hni.vmagic.util.VhdlCollections;

/**
 * @author DINESH THANGAVEL
 *
 */
public class VMagicFileProcessor {

	String fileNameToProcess;
	HashMap<String,String> fileList;
	VhdlToElectronicsConverter electronicsConverter;
	
	// key Vhdl signal
	// value Procgen SignalBus
	// signalBus value can be null if the exact source signalbus has not been identified
	HashMap<Signal,SignalBusUniqueLocater> signalToBusMap; 
	HashMap<Signal,DisconnectedSignalBus> disconnectedSignalBusMap;
	
	public HashMap<String, String> getFileList() {
		return fileList;
	}

	public VMagicFileProcessor(VhdlToElectronicsConverter electronicsConverter, String fileNameToProcess,HashMap<String,String> fileList) {
		this.fileNameToProcess = fileNameToProcess;
		this.fileList = fileList;
		this.electronicsConverter = electronicsConverter;
		this.signalToBusMap = new HashMap<Signal,SignalBusUniqueLocater>();
		this.disconnectedSignalBusMap = new HashMap<Signal,DisconnectedSignalBus>();
	}
	
	/**
	 * 
	 * @param fileContentToProcess
	 * @param parentEntity : null when calling for the first time 
	 * @return
	 * @throws IOException
	 * @throws VhdlParserException
	 * @throws ProcGenException
	 */
	public com.fyp.procGenWeb.client.procGen.electronics.core.Entity processFile(String fileContentToProcess,com.fyp.procGenWeb.client.procGen.electronics.core.Entity parentEntity) throws IOException, VhdlParserException, ProcGenException{
		VhdlFile inFileToProcess = VhdlParser.parseString(fileContentToProcess);
		com.fyp.procGenWeb.client.procGen.electronics.core.Entity entityToReturn =  this.processVMagicFile(inFileToProcess,parentEntity);
		this.setComponentAlreadyProcessed(entityToReturn.getName());
		return entityToReturn;
	}
	
	private com.fyp.procGenWeb.client.procGen.electronics.core.Entity processVMagicFile(
			VhdlFile vhdlFileInput, com.fyp.procGenWeb.client.procGen.electronics.core.Entity parentEntity) throws ProcGenException{
		
		List<Entity> entityList = VhdlCollections.getAll(
				vhdlFileInput.getElements(), Entity.class);
		
		if(entityList.size() <1){
			throw new ProcGenException(Consts.ExceptionMessages.NO_VHDL_ENTITY_PRESENT_FOR_IMPORT);
		}
			
		
		assert (entityList.size() == 1);

		// add the entity
		com.fyp.procGenWeb.client.procGen.electronics.core.Entity newProgenEntity = this.processVMagicEntity(entityList.get(0),parentEntity);

		// Connections
		List<Architecture> architectureList = VhdlCollections.getAll(
				vhdlFileInput.getElements(), Architecture.class);
		
		// assuming only one architecture
		assert (architectureList.size() == 1);
		Architecture newEntityArchitecture = architectureList.get(0);
		processVMagicArchitecture(newEntityArchitecture, newProgenEntity);

		// saving architecture so that it can be reused later
		//this.coreEntityArchitecture = newEntityArchitecture;
		return newProgenEntity;
	}

	private com.fyp.procGenWeb.client.procGen.electronics.core.Entity processVMagicEntity(Entity vMagicEntity, com.fyp.procGenWeb.client.procGen.electronics.core.Entity parentEntity) throws ProcGenException {
		com.fyp.procGenWeb.client.procGen.electronics.core.Entity newEntity = new com.fyp.procGenWeb.client.procGen.electronics.core.Entity(vMagicEntity.getIdentifier());
		if(parentEntity != null){
			newEntity.setParent(parentEntity);
		}
			
		String id = this.electronicsConverter.newProject.getEntityManager().addEntity(newEntity);
		
		EntityManager em = this.electronicsConverter.newProject.getEntityManager();
		
		for (VhdlObjectProvider<Signal> portsList : vMagicEntity.getPort()) {
			Iterator<Signal> iterator = portsList.getVhdlObjects().iterator();
			while (iterator.hasNext()) {
				Signal port = iterator.next();
				
				// default signalBusWidth
				int signalBusWidth = 1; 
				
				if(port.getType() instanceof IndexSubtypeIndication){
					IndexSubtypeIndication s = (IndexSubtypeIndication) port.getType();
					Range range = (Range)s.getRanges().get(0);
					
					RangeProcessor rangeProc =  new RangeProcessor(range,this.electronicsConverter.newProject,this,parentEntity);
					signalBusWidth = rangeProc.getBusWidthFromRange();
				}
				
				if(port.getMode() == Mode.IN){
					em.addInputSignal(id, port.getIdentifier(), signalBusWidth);
				}
				
				else if(port.getMode() == Mode.OUT){
					em.addOutputSignal(id, port.getIdentifier(), signalBusWidth);
				}
				
				// TODO: Take care of other Modes as well
				
				// map the signal to input and output signalbus
				if(!this.signalToBusMap.containsKey(port)){
					this.signalToBusMap.put(port, new SignalBusUniqueLocater(id,port.getIdentifier()));
				}
			}
		}
		
		return newEntity;
	}

	private void processVMagicArchitecture(Architecture newEntityArchitecture,
			com.fyp.procGenWeb.client.procGen.electronics.core.Entity newProgenEntity)
			throws ProcGenException {

		// identify the registers
		List<BlockDeclarativeItem> declarationList = newEntityArchitecture
				.getDeclarations();
		processDeclarationStatements(declarationList,newProgenEntity);
		
		List<ConcurrentStatement> concurrentStatementList = newEntityArchitecture.getStatements();
		this.processConcurrentStatements(concurrentStatementList,newProgenEntity);
	}
	
	private void processDeclarationStatements(
			List<BlockDeclarativeItem> declarationList, com.fyp.procGenWeb.client.procGen.electronics.core.Entity newProcgenEntity) {

		DeclarationProcessor declarationProcessor = new DeclarationProcessor(
				this,newProcgenEntity);
		for (BlockDeclarativeItem declaration : declarationList) {
			if (declaration instanceof DeclarativeItem) {
				declarationProcessor.visit((DeclarativeItem) declaration);
			}
		}

	}
	
	private void processConcurrentStatements(List<ConcurrentStatement> concurrentStatementList, com.fyp.procGenWeb.client.procGen.electronics.core.Entity parentEntity) {

		ConcurrentStatementProcessor concurrentStatementProcessor = new ConcurrentStatementProcessor(this,parentEntity);
		for (ConcurrentStatement concurrentStatement : concurrentStatementList) {
			concurrentStatementProcessor.visit(concurrentStatement);
		}
	}
	
	public boolean checkIfComponentAlreadyProcessed(String fileNameToCheck){
		return this.electronicsConverter.isProcessed.get(fileNameToCheck);
	}
	
	public void setComponentAlreadyProcessed(String fileNameToSet){
		this.electronicsConverter.isProcessed.put(fileNameToSet, true);
	}
}
