/**
 * 
 */
package com.fyp.procGenWeb.server;

import java.util.HashMap;

import com.fyp.procGenWeb.client.HdlTranslationService;
import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsToHdlConverter;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.ElectronicsToVhdlConverter;
import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")

public class HdlTranslationServiceImpl extends RemoteServiceServlet implements HdlTranslationService{

	@Override
	public HashMap<String,String> transferProjectDetails(ProjectTransferObject projectDtoReceived) {
		HashMap<String,String> answerToReturn = new HashMap<String,String>();
		
		
		try {
			Project projectTransferred = projectDtoReceived.convertToProject(); 
			ElectronicsToHdlConverter hdlConverter = new ElectronicsToVhdlConverter(projectTransferred); 
			answerToReturn = hdlConverter.convertProjectToHdl(projectTransferred);
		} catch (ProcGenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		answerToReturn.put("test", "It is great!");
		return answerToReturn;
		
	}

}
