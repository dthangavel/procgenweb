/**
 * 
 */
package com.fyp.procGenWeb.server;

import java.io.IOException;
import java.util.HashMap;

import com.fyp.procGenWeb.client.ImportHdlService;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport.VhdlToElectronicsConverter;
import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.upb.hni.vmagic.parser.VhdlParserException;

/**
 * @author DINESH THANGAVEL
 *
 */
@SuppressWarnings("serial")
public class ImportHdlServiceImpl extends RemoteServiceServlet implements
		ImportHdlService {

	
	@Override
	public ProjectTransferObject transferHdlFiles(
			HashMap<String, String> hdlFiles) throws Exception {
		// TODO Auto-generated method stub
					
		VhdlToElectronicsConverter vhdlConverter = new VhdlToElectronicsConverter();
		try {
			Project newProject = vhdlConverter.convertVhdlFilesToProcGen(hdlFiles);
			ProjectTransferObject test = new ProjectTransferObject(newProject);
			return test;
		} catch (IOException | VhdlParserException | ProcGenException e) {
			e.printStackTrace();
			throw e;
		}
	}

}
