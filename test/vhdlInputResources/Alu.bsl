----------------------------------------------------------------------------------
-- Company: NUS
-- Engineer: Rajesh Panicker
-- 
-- Create Date:   10:39:18 13/09/2014
-- Design Name: 	ALU
-- Target Devices: Nexys 4 (Artix 7 100T)
-- Tool versions: ISE 14.7
-- Description: ALU template for MIPS processor
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------


------------------------------------------------------------------
-- ALU Entity
------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.math_real.log2;
use ieee.math_real.ceil;

entity alu is
generic (width 	: integer := 32);
Port (Clk			: in	STD_LOGIC;
		Control		: in	STD_LOGIC_VECTOR (5 downto 0);
		Operand1		: in	STD_LOGIC_VECTOR (width-1 downto 0);
		Operand2		: in	STD_LOGIC_VECTOR (width-1 downto 0);
		Result1		: out	STD_LOGIC_VECTOR (width-1 downto 0);
		Result2		: out	STD_LOGIC_VECTOR (width-1 downto 0);
		Status		: out	STD_LOGIC_VECTOR (2 downto 0); -- busy (multicycle only), overflow (add and sub), zero (sub)
		Debug			: out	STD_LOGIC_VECTOR (width-1 downto 0));		
end alu;


------------------------------------------------------------------
-- ALU Architecture
------------------------------------------------------------------

architecture Behavioral of alu is

type states is (COMBINATIONAL, MULTI_CYCLE);
signal state, n_state 	: states := COMBINATIONAL;

constant shift_bits : integer := integer(ceil(log2(real(width))));

----------------------------------------------------------------------------
-- Adder instantiation
----------------------------------------------------------------------------
component adder is
generic (width : integer);
port (A 		: in 	std_logic_vector(width-1 downto 0);
		B 		: in 	std_logic_vector(width-1 downto 0);
		C_in 	: in 	std_logic;
		S 		: out std_logic_vector(width-1 downto 0);
		C_out	: out std_logic);
end component adder;

----------------------------------------------------------------------------
-- Adder signals
----------------------------------------------------------------------------
signal B 		: std_logic_vector(width-1 downto 0) := (others => '0'); 
signal C_in 	: std_logic := '0';
signal S 		: std_logic_vector(width-1 downto 0) := (others => '0'); 
signal C_out	: std_logic := '0'; --not used


----------------------------------------------------------------------------
-- Signals for MULTI_CYCLE_PROCESS
----------------------------------------------------------------------------
signal Result1_multi		: STD_LOGIC_VECTOR (width-1 downto 0) := (others => '0'); 
signal Result2_multi		: STD_LOGIC_VECTOR (width-1 downto 0) := (others => '0');
signal Debug_multi		: STD_LOGIC_VECTOR (width-1 downto 0) := (others => '0'); 
signal done		 			: STD_LOGIC := '0';

begin

-- <port maps>
adder32 : adder generic map (width =>  width) port map (  A=>Operand1, B=>B, C_in=>C_in, S=>S, C_out=>C_out );
-- </port maps>


----------------------------------------------------------------------------
-- COMBINATIONAL PROCESS
----------------------------------------------------------------------------
COMBINATIONAL_PROCESS : process (
											Control, Operand1, Operand2, state, -- external inputs
											S, C_out, -- ouput from the adder (or other components)
											Result1_multi, Result2_multi, Debug_multi, done -- from multi-cycle process(es)
											)
variable overflow : std_logic;
variable shift_temp : STD_LOGIC_VECTOR (width-1 downto 0);

begin

-- <default outputs>
Status(2 downto 0) <= "000"; -- both statuses '0' by default 
Result1 <= (others=>'0');
Result2 <= (others=>'0');
Debug <= (others=>'0');

n_state <= state;

B <= Operand2;
C_in <= '0';
-- </default outputs>

--reset
if Control(5) = '1' then
	n_state <= COMBINATIONAL;
else

case state is
	when COMBINATIONAL =>
		case Control(4 downto 0) is
		--and
		when "00000" =>   -- takes 0 cycles to execute
			Result1 <= Operand1 and Operand2;
		--or
		when "00001" =>
			Result1 <= Operand1 or Operand2;
		--xor
		when "00100" => 
			Result1 <= Operand1 xor Operand2;
		--nor
		when "01100" => 
			Result1 <= Operand1 nor Operand2;
		--sll
		when "00101" =>
			shift_temp := Operand1;
			for i in 0 to shift_bits-1 loop
				if Operand2(i) = '1' then
					shift_temp := shift_temp(width-2**i-1 downto 0) & (2**i-1 downto 0 => '0');
				end if;
			end loop;
			Result1 <= shift_temp;
		--srl, sra			
		when "01101" | "01001" =>
			shift_temp := Operand1;
			for i in 0 to shift_bits-1 loop
				if Operand2(i) = '1' then
					shift_temp := (2**i-1 downto 0 => not(Control(2)) and Operand1(width-1)) & shift_temp(width-1 downto 2**i);
				end if;
			end loop;	
			Result1 <= shift_temp;
		--add
		when "00010" =>
			Result1 <= S;
			-- overflow
			Status(1) <= ( Operand1(width-1) xnor  Operand2(width-1) )  and ( Operand2(width-1) xor S(width-1) );
		-- sub, slt, sltu
		when "00110" | "00111" | "01110" =>
			B <= not(Operand2);
			C_in <= '1';
			-- overflow
			overflow := ( Operand1(width-1) xor  Operand2(width-1) )  and ( Operand2(width-1) xnor S(width-1) );
			Status(1) <= overflow;
			-- zero
			if S = x"00000000" then 
				Status(0) <= '1'; 
			else
				Status(0) <= '0';
			end if;			
			if Control(3) = '0' and Control(0) = '1' then --slt
				--Result1(0) <= S(width-1) xor overflow; -- overflow = MSB if no overflow, MSB' if overflow
			elsif Control(3) = '1' and Control(0) = '0' then --sltu
				Result1(0) <= C_out; 		-- overflow = borrow
			else 						--sub
				Result1 <= S;
			end if;
		-- multi-cycle operations
		when "10000"|"10001"|"10010"|"10011"|"11110" => 
			n_state <= MULTI_CYCLE;
			Status(2) <= '1';
		-- default cases (already covered)
		when others=> null;
		end case;
	when MULTI_CYCLE => 
		if done = '1' then
			Result1 <= Result1_multi;
			Result2 <= Result2_multi;
			Debug <= Debug_multi;
			n_state <= COMBINATIONAL;
			Status(2) <= '0';
		else
			Status(2) <= '1';
			n_state <= MULTI_CYCLE;
		end if;
	end case;
end if;	
end process;


----------------------------------------------------------------------------
-- STATE UPDATE PROCESS
----------------------------------------------------------------------------

STATE_UPDATE_PROCESS : process (Clk) -- state updating
begin  
   if (Clk'event and Clk = '1') then
		state <= n_state;
   end if;
end process;

----------------------------------------------------------------------------
-- MULTI CYCLE PROCESS
----------------------------------------------------------------------------

MULTI_CYCLE_PROCESS : process (Clk) -- multi-cycle operations done here
-- assume that Operand1 and Operand 2 do not change while multi-cycle operations are being performed
variable count : std_logic_vector(15 downto 0) := (others => '0');
variable temp_sum : std_logic_vector(2*width-1 downto 0) := (others => '0');
variable shifted_op1 : std_logic_vector(2*width-1 downto 0) := (others => '0');
variable shifted_op2 : std_logic_vector(2*width-1 downto 0) := (others => '0');
begin  
   if (Clk'event and Clk = '1') then 
		if Control(5) = '1' then
			count := (others => '0');
			temp_sum := (others => '0');
		end if;
		done <= '0';
		if n_state = MULTI_CYCLE then
			case Control(4 downto 0) is
			-- 10001 takes 32 cycles to execute, returns unsigned(Operand1)*unsigned(Operand2)
			-- 10000 takes 64 cycles to execute, returns signed(Operand1)*signed(Operand2)
			when "10000"|"10001" =>  
			-- n_state = MULTI_CYCLE and state = COMBINATIONAL implies we are just transitioning into MULTI_CYCLE
				if state = COMBINATIONAL then  
					temp_sum := (others => '0');
					count := (others => '0');							
					shifted_op1 := (2*width-1 downto width => not(Control(0)) and Operand1(width-1)) & Operand1;					
					shifted_op2 := (2*width-1 downto width => not(Control(0)) and Operand2(width-1)) & Operand2;				
				end if;		
				count := count+1;	
				if shifted_op2(0)= '1' then -- add only if b0 = 1
					temp_sum := temp_sum + shifted_op1;
				end if;
				if (Control(0)='1' and count=width) or (Control(0)='0' and count=2*width) then	
					Result1_multi <= temp_sum(width-1 downto 0);
					Result2_multi <= temp_sum(2*width-1 downto width);
					Debug_multi <= Operand1(width/2-1 downto 0) & Operand2(width/2-1 downto 0); -- just a random output
					done <= '1';	
				end if;
					shifted_op2 := '0'& shifted_op2(2*width-1 downto 1);
					shifted_op1 := shifted_op1(2*width-2 downto 0)&'0';
			-- takes 1 cycle to execute, just returns the operands		
			when "11110" => 
				if state = COMBINATIONAL then
					Result1_multi <= Operand1;
					Result2_multi <= Operand2;
					Debug_multi <= Operand1(width-1 downto width/2) & Operand2(width-1 downto width/2);
					done <= '1';
				end if;	
			when others=> null;
			end case;
		end if;
	end if;
end process;


end Behavioral;