library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    signal Register_1_input0 : std_logic_vector(1 downto 0);
    signal Register_1_enable : std_logic;
    signal Register_1_clock : std_logic;
    signal Register_1_output : std_logic_vector(1 downto 0);
    signal Register_2_input0 : std_logic;
    signal Register_2_enable : std_logic;
    signal Register_2_clock : std_logic;
    signal Register_2_output : std_logic;
begin
    Register_1_process : process(Register_1_clock)
    begin
        if Register_1_clock'EVENT and Register_1_clock = '1' then
            if Register_1_enable = '1' then
                Register_1_output <= Register_1_input0;
            end if;
        end if;
    end process;
    Register_2_process : process(Register_2_clock)
    begin
        if Register_2_clock'EVENT and Register_2_clock = '0' then
            if Register_2_enable = '1' then
                Register_2_output <= Register_2_input0;
            end if;
        end if;
    end process;
end;
