library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    type MEM_1_2x4 is array (1 downto 0) of std_logic_vector(3 downto 0);
    signal testMemoryUnit_1 : MEM_1_2x4;
    signal testMemoryUnit_1_inputAddress : std_logic_vector(3 downto 0);
    signal testMemoryUnit_1_enable : std_logic;
    signal testMemoryUnit_1_inputData : std_logic_vector(1 downto 0);
    signal testMemoryUnit_1_output : std_logic_vector(1 downto 0);
begin
end;
