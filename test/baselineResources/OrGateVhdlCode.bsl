library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    signal Or_1_input0 : std_logic;
    signal Or_1_input1 : std_logic;
    signal Or_1_output : std_logic;
begin
    Or_1_output <= Or_1_input0 or Or_1_input1;
end;
