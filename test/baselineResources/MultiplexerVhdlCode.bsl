library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    signal mux1_1_selectionInput : std_logic;
    signal mux1_1_input0 : std_logic_vector(2 downto 0);
    signal mux1_1_input1 : std_logic_vector(2 downto 0);
    signal mux1_1_output : std_logic_vector(2 downto 0);
begin
    with mux1_1_selectionInput select
        mux1_1_output <=
            mux1_1_input1 when '1',
            mux1_1_input0 when '0';
end;
