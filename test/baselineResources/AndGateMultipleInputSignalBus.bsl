library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    signal multipleAnd_1_input0 : std_logic_vector(1 downto 0);
    signal multipleAnd_1_input1 : std_logic_vector(1 downto 0);
    signal multipleAnd_1_input2 : std_logic_vector(1 downto 0);
    signal multipleAnd_1_output : std_logic_vector(1 downto 0);
begin
    multipleAnd_1_output <= multipleAnd_1_input0 and multipleAnd_1_input1 and multipleAnd_1_input2;
end;
