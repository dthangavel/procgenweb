library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    signal multipleOr_1_input0 : std_logic_vector(1 downto 0);
    signal multipleOr_1_input1 : std_logic_vector(1 downto 0);
    signal multipleOr_1_input2 : std_logic_vector(1 downto 0);
    signal multipleOr_1_output : std_logic_vector(1 downto 0);
begin
    multipleOr_1_output <= multipleOr_1_input0 or multipleOr_1_input1 or multipleOr_1_input2;
end;
