library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    signal testJoint_1_input0 : std_logic_vector(1 downto 0);
    signal testJoint_1_input1 : std_logic_vector(2 downto 0);
    signal testJoint_1_output : std_logic_vector(4 downto 0);
begin
    testJoint_1_output <= testJoint_1_input0 & testJoint_1_input1;
end;
