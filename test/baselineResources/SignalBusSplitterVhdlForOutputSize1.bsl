library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    signal splitter_1_input0 : std_logic_vector(4 downto 0);
    signal splitter_1_output : std_logic;
begin
    splitter_1_output <= splitter_1_input0(4);
end;
