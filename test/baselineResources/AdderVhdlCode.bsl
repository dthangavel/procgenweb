library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity testProject is
    port (
        CLK : in std_logic
    );
end;
architecture rtl of testProject is
    signal S_wider : std_logic_vector(2 downto 0);
    signal adder1_1_input0 : std_logic_vector(1 downto 0);
    signal adder1_1_input1 : std_logic_vector(1 downto 0);
    signal adder1_1_input2 : std_logic;
    signal adder1_1_output : std_logic_vector(1 downto 0);
    signal adder1_1_carry : std_logic;
begin
    S_wider <= ('0' & adder1_1_input0) + ('0' & adder1_1_input1) + adder1_1_input2;
    adder1_1_output <= S_wider(1 downto 0);
    adder1_1_carry <= S_wider(2);
end;
