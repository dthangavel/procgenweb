package vhdlInputTests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import vhdlOutputTests.TestUtils;

import com.fyp.procGenWeb.client.procGen.electronics.core.Connection;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.EntityManager;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.AndGate;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Multiplexer;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport.VhdlToElectronicsConverter;

import de.upb.hni.vmagic.parser.VhdlParserException;

public class VhdlToMuxTest {
	
	@Test
	public void vhdlToSimpleMux() {
		String vhdlInputFileName = TestUtils.VhdlInputFolderDirectory + "\\" + "MultiplexerVhdlCode.bsl";
		
		try {
			File inputVhdlFile = new File(vhdlInputFileName);
			String inputVhdlCodeAsString = FileUtils.readFileToString(inputVhdlFile);
			
			HashMap<String,String> fileResources = new HashMap<String,String>();
			fileResources.put("TestMux", inputVhdlCodeAsString);
			
			VhdlToElectronicsConverter converter = new VhdlToElectronicsConverter();
			Project convertedProject = converter.convertVhdlFilesToProcGen(fileResources);
			
			EntityManager entityManager = convertedProject.getEntityManager();
			Assert.assertEquals(entityManager.getBaseEntities().size(),1);
				
			Entity andGate = entityManager.getEntityById("1-1");
			if(!(andGate instanceof AndGate)){
				Assert.fail();
			}
			
			Entity mux = entityManager.getEntityById("1-2");
			if(!(mux instanceof Multiplexer)){
				Assert.fail();
			}
			
			HashMap<String,String> mapping = ((Multiplexer)mux).getMuxMapping();
			Assert.assertEquals(mapping.size(), 2);
			Assert.assertEquals(mapping.get("0"), "input1");
			Assert.assertEquals(mapping.get("1"), "input0");
			
			 HashMap<String, HashMap<String, List<Connection>>> connectionDirectory =  entityManager.getEntityById("1").getEntityConnectionManager().getConnectionDirectory();
			List<Connection> connectionListForOutput = connectionDirectory.get("1-1").get("output");
			Assert.assertEquals(connectionListForOutput.size(), 1);
			Assert.assertEquals(connectionListForOutput.get(0).getDestinationEntityId(), "1-2");
			Assert.assertEquals(connectionListForOutput.get(0).getOutputSignal().getName(), "selectionInput");
		} catch (IOException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (VhdlParserException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		}
	}
}
