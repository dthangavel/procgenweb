package vhdlInputTests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import vhdlOutputTests.TestUtils;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusObserver;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalValueRecord;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport.VhdlToElectronicsConverter;
import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;

import de.upb.hni.vmagic.parser.VhdlParserException;

public class VhdlToAdderTest {
  
  @Test
  public void testVhdlToAdderConversion() {
		String vhdlInputFileName = TestUtils.VhdlInputFolderDirectory + "\\" + "AdderForAlu.bsl";
		
		try {
			File inputVhdlFile = new File(vhdlInputFileName);
			String inputVhdlCodeAsString = FileUtils.readFileToString(inputVhdlFile);
			
			HashMap<String,String> fileResources = new HashMap<String,String>();
			fileResources.put("adder", inputVhdlCodeAsString);
			
			VhdlToElectronicsConverter converter = new VhdlToElectronicsConverter();
			Project convertedProject = converter.convertVhdlFilesToProcGen(fileResources);
		
			Assert.assertEquals(convertedProject.getEntityManager().getBaseEntities().size(),1);
			
			Entity projectEntity = convertedProject.getEntityManager().getEntityById("1");
			// 2 signalBusJoints, 2 adder and 
			Assert.assertEquals(projectEntity.getChildEntityList().size(),6);
		} catch (IOException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (VhdlParserException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		}
  }
  
  @Test
  public void testAdderSimulationAfterConversion() {
	  String vhdlInputFileName = TestUtils.VhdlInputFolderDirectory + "\\" + "AdderForAlu.bsl";
	  
	  File inputVhdlFile = new File(vhdlInputFileName);
		String inputVhdlCodeAsString;
		try {
			inputVhdlCodeAsString = FileUtils.readFileToString(inputVhdlFile);
			HashMap<String,String> fileResources = new HashMap<String,String>();
			fileResources.put("adder", inputVhdlCodeAsString);
			
			VhdlToElectronicsConverter converter = new VhdlToElectronicsConverter();
			Project convertedProject = converter.convertVhdlFilesToProcGen(fileResources);
			
			ElectronicsLogicFacade facade = ElectronicsLogicFacade.getInstance();
			facade.setActiveProjectInstance(convertedProject);
			
			ProjectTransferObject pto = new ProjectTransferObject(convertedProject);
			Project testProject = pto.convertToProject();
			
			facade.setActiveProjectInstance(testProject);
			
			SignalBusObserver outputObserver = new SignalBusObserver(testProject.getEntityManager().getEntityById("1").getOutputByName("S"),testProject.getProjectSimulator(),"1-S");
			SignalBusObserver adder_1_4_Observer = new SignalBusObserver(testProject.getEntityManager().getEntityById("1-4").getOutputByName("output"),testProject.getProjectSimulator(),"1-4-output");
			SignalBusObserver splitter_1_5_Observer = new SignalBusObserver(testProject.getEntityManager().getEntityById("1-5").getOutputByName("output"),testProject.getProjectSimulator(),"1-5-output");
			SignalBusObserver splitter_1_6_Observer = new SignalBusObserver(testProject.getEntityManager().getEntityById("1-6").getOutputByName("output"),testProject.getProjectSimulator(),"1-6-output");
			
			SignalBusObserver carryObserver = new SignalBusObserver(testProject.getEntityManager().getEntityById("1").getOutputByName("C_out"),testProject.getProjectSimulator(),"1-carry");
			
			LogicFacade logicInterface = new LogicFacade();
			
			System.out.println(logicInterface.processInput("new_input_simulator simulator1 2 1 A"));
			System.out.println(logicInterface.processInput("new_input_simulator simulator2 2 1 B"));
			System.out.println(logicInterface.processInput("new_input_simulator simulator3 1 1 C_in"));
			
//			System.out.println(logicInterface.processInput("change_sim_input simulator1 00"));
//			System.out.println(logicInterface.processInput("change_sim_input simulator2 01"));
//			System.out.println(logicInterface.processInput("change_sim_input simulator3 0"));
			
			System.out.println(logicInterface.processInput("add_input_stimulus 1 400 'change_sim_input simulator2 01'"));
			System.out.println(logicInterface.processInput("add_input_stimulus 1 600 'change_sim_input simulator3 1'"));
			System.out.println(logicInterface.processInput("add_input_stimulus 1 800 'change_sim_input simulator1 11'"));
			
			System.out.println(logicInterface.processInput("simulate 0 1"));
			System.out.println("S output:");
			outputObserver.printSignalLogToConsole();
			
			System.out.println("\n 1-4 Adder output:");
			adder_1_4_Observer.printSignalLogToConsole();
			
			System.out.println("\n 1-5 Splitter output:");
			splitter_1_5_Observer.printSignalLogToConsole();
			
			System.out.println("\n 1-6 Splitter output:");
			splitter_1_6_Observer.printSignalLogToConsole();
			
			System.out.println("\n C_out output:");
			carryObserver.printSignalLogToConsole();
			//outputObserver1.printSignalLogToConsole();
			
			List<SignalValueRecord> outputSignalVariations = outputObserver.getSignalValueRecords();
			List<SignalValueRecord> carryVariations = carryObserver.getSignalValueRecords();
			List<SignalValueRecord> adder4_variations = adder_1_4_Observer.getSignalValueRecords();
			
			Assert.assertEquals(outputSignalVariations.size(), 4);
			Assert.assertEquals(outputSignalVariations.get(0).getDisplayStringForCurrentValue(), "LL");
			Assert.assertEquals(outputSignalVariations.get(1).getDisplayStringForCurrentValue(), "LH");
			Assert.assertEquals(outputSignalVariations.get(2).getDisplayStringForCurrentValue(), "HL");
			Assert.assertEquals(outputSignalVariations.get(3).getDisplayStringForCurrentValue(), "LH");
			
			Assert.assertEquals(carryVariations.size(), 2);
			Assert.assertEquals(carryVariations.get(0).getDisplayStringForCurrentValue(), "L");
			Assert.assertEquals(carryVariations.get(1).getDisplayStringForCurrentValue(), "H");
			
			Assert.assertEquals(adder4_variations.size(), 4);
			Assert.assertEquals(adder4_variations.get(0).getDisplayStringForCurrentValue(), "LLL");
			Assert.assertEquals(adder4_variations.get(1).getDisplayStringForCurrentValue(), "LLH");
			Assert.assertEquals(adder4_variations.get(2).getDisplayStringForCurrentValue(), "LHL");
			Assert.assertEquals(adder4_variations.get(3).getDisplayStringForCurrentValue(), "HLH");
			
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		} catch (VhdlParserException e) {
			e.printStackTrace();
			Assert.fail();
		} catch (ProcGenException e) {
			e.printStackTrace();
			Assert.fail();
		}
  }
}
