package vhdlInputTests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport.VhdlToElectronicsConverter;

import de.upb.hni.vmagic.parser.VhdlParserException;
import vhdlOutputTests.TestUtils;

public class VhdlToAndGateTest {

	@Test
	public void testVhdlToSimpleAndGate() {
		String vhdlInputFileName = TestUtils.VhdlInputFolderDirectory + "\\" + "AndGateVhdlCode.bsl";
				
		try {
			File inputVhdlFile = new File(vhdlInputFileName);
			String inputVhdlCodeAsString = FileUtils.readFileToString(inputVhdlFile);
			
			HashMap<String,String> fileResources = new HashMap<String,String>();
			fileResources.put("TestAnd", inputVhdlCodeAsString);
			
			VhdlToElectronicsConverter converter = new VhdlToElectronicsConverter();
			Project convertedProject = converter.convertVhdlFilesToProcGen(fileResources);
			
			Assert.assertEquals(convertedProject.getEntityManager().getBaseEntities().size(),1);
		} catch (IOException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (VhdlParserException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		}
	}
	
}
