package vhdlInputTests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import vhdlOutputTests.TestUtils;

import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.vhdlImport.VhdlToElectronicsConverter;

import de.upb.hni.vmagic.parser.VhdlParserException;

public class VhdlProcessToEntity {
  
  @Test
  public void convertVhdlProcessToEntityTest() {
	  String vhdlInputFileName = TestUtils.VhdlInputFolderDirectory + "\\" + "Alu.bsl";
	  String vhdlInputAdderFile = 	TestUtils.VhdlInputFolderDirectory + "\\" + "AdderForAlu.bsl";
		try {
			File inputVhdlFile = new File(vhdlInputFileName);
			String inputVhdlCodeAsString = FileUtils.readFileToString(inputVhdlFile);
			
			File inputAdderVhdlFile = new File(vhdlInputAdderFile);
			String inputAdderVhdlCodeAsString = FileUtils.readFileToString(inputAdderVhdlFile);
			
			HashMap<String,String> fileResources = new HashMap<String,String>();
			fileResources.put("alu", inputVhdlCodeAsString);
			fileResources.put("adder", inputAdderVhdlCodeAsString);
			
			VhdlToElectronicsConverter converter = new VhdlToElectronicsConverter();
			Project convertedProject = converter.convertVhdlFilesToProcGen(fileResources);
		}
		
		catch (IOException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (VhdlParserException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		}
  }
  
}
