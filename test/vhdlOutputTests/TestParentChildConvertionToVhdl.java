package vhdlOutputTests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Entity;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.hdlConversion.HdlConsts.HdlConversionType;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.ElectronicsToVhdlConverter;

public class TestParentChildConvertionToVhdl {
  
	Project testProject = null;
	LogicFacade logicInterface = new LogicFacade();

	@BeforeMethod
	public void beforeMethod() {
	
		// Create a new project
		try {
			logicInterface.processInput("new_project testProject");
			testProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance();
			TestUtils.clearFilesInOutputFolder();
		} catch (ProcGenException e) {
			
			Assert.fail();
			e.printStackTrace();
		} catch (IOException e) {
			Assert.fail();
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	public void afterMethod(){
		try {
			System.out.println(logicInterface.processInput("close_project"));
		} catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		}
	}
	
  @Test
  public void testMultipleGateVhdlConversion(){
	  ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(testProject);
	  try {
		  	// Entity1 with 2 or gates
			System.out.println(logicInterface.processInput("new_entity entity1 3 1 0 input0 1 input1 1 input2 1 output 1"));
			System.out.println(logicInterface.processInput("new_or_gate orGateTest 2 1"));
			System.out.println(logicInterface.processInput("new_or_gate orGateTest 2 1"));
			
			System.out.println(logicInterface.processInput("connect 1 1-1 input0 input0 default"));
			System.out.println(logicInterface.processInput("connect 1 1-1 input1 input1 default"));
			System.out.println(logicInterface.processInput("connect 1-1 1-2 output input0 default"));
			System.out.println(logicInterface.processInput("connect 1 1-2 input2 input1 default"));
			System.out.println(logicInterface.processInput("connect 1-2 1 output output default"));
			
			System.out.println(logicInterface.processInput("new_entity entity2 3 1 0 input0 1 input1 1 input2 1 output 1"));
			System.out.println(logicInterface.processInput("new_and_gate andGateTest 2 2"));
			System.out.println(logicInterface.processInput("new_and_gate andGateTest 2 2"));
			
			System.out.println(logicInterface.processInput("connect 2 2-1 input0 input0 default"));
			System.out.println(logicInterface.processInput("connect 2 2-1 input1 input1 default"));
			System.out.println(logicInterface.processInput("connect 2-1 2-2 output input0 default"));
			System.out.println(logicInterface.processInput("connect 2 2-2 input2 input1 default"));
			System.out.println(logicInterface.processInput("connect 2-2 2 output output default"));
			
			System.out.println(logicInterface.processInput("connect 1 2 output input0 default"));
			
			HashMap<String,String> generatedCodes = ec.convertProjectToHdl(testProject);
			
			String testProjectVhdGeneratedCode = generatedCodes.get(testProject.getName());
			String testProjectVhdbaselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "ParentChildTestProject.bsl";
			
			File baselineFile = new File(testProjectVhdbaselineFileName);
			
			Assert.assertEquals(testProjectVhdGeneratedCode, FileUtils.readFileToString(baselineFile));
			
			// check entity1 file:
			String entity1VhdGeneratedCode = generatedCodes.get("entity1_1");
			String entity1VhdbaselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "ParentChildEntity1InlineAndOr.bsl";
			
			File enitity1BaselineFile = new File(entity1VhdbaselineFileName);
			
			Assert.assertEquals(entity1VhdGeneratedCode, FileUtils.readFileToString(enitity1BaselineFile));
			
			// check entity2 file:
			String entity2VhdConvertedString = generatedCodes.get("entity2_2");
			String entity2VhdbaselineFileName = TestUtils.BaselineFolderDirectory
					+ "\\" + "ParentChildEntity2InlineAndOr.bsl";

			File enitity2BaselineFile = new File(entity2VhdbaselineFileName);

			Assert.assertEquals(entity2VhdConvertedString,
					FileUtils.readFileToString(enitity2BaselineFile));
			
	  } catch (IOException e) {
		Assert.fail();
		e.printStackTrace();
	} catch (ProcGenException e) {
		Assert.fail();
		e.printStackTrace();
	}
  }
  
  
  @Test
  public void testMultipleGateVhdlConversionWithSeparateAndOrGates(){
	  ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(testProject);
	  try {
		  	// Entity1 with 2 or gates
			System.out.println(logicInterface.processInput("new_entity entity1 3 1 0 input0 1 input1 1 input2 1 output 1"));
			System.out.println(logicInterface.processInput("new_or_gate orGateTest 2 1"));
			System.out.println(logicInterface.processInput("new_or_gate orGateTest 2 1"));
			
			Entity entityToChange = testProject.getEntityManager().getEntityById("1-1");
			entityToChange.setHdlConversionType(HdlConversionType.SeparateEntity);
			
			System.out.println(logicInterface.processInput("connect 1 1-1 input0 input0 default"));
			System.out.println(logicInterface.processInput("connect 1 1-1 input1 input1 default"));
			System.out.println(logicInterface.processInput("connect 1-1 1-2 output input0 default"));
			System.out.println(logicInterface.processInput("connect 1 1-2 input2 input1 default"));
			System.out.println(logicInterface.processInput("connect 1-2 1 output output default"));
			
			System.out.println(logicInterface.processInput("new_entity entity2 3 1 0 input0 1 input1 1 input2 1 output 1"));
			System.out.println(logicInterface.processInput("new_and_gate andGateTest 2 2"));
			System.out.println(logicInterface.processInput("new_and_gate andGateTest 2 2"));
			
			System.out.println(logicInterface.processInput("connect 2 2-1 input0 input0 default"));
			System.out.println(logicInterface.processInput("connect 2 2-1 input1 input1 default"));
			System.out.println(logicInterface.processInput("connect 2-1 2-2 output input0 default"));
			System.out.println(logicInterface.processInput("connect 2 2-2 input2 input1 default"));
			System.out.println(logicInterface.processInput("connect 2-2 2 output output default"));
			
			System.out.println(logicInterface.processInput("connect 1 2 output input0 default"));
			
			HashMap<String,String> generatedCodes = ec.convertProjectToHdl(testProject);
			
			String testProjectVhdCode = generatedCodes.get(testProject.getName());
			String testProjectVhdbaselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "ParentChildTestProjectSeparateOr.bsl";
			
			File baselineFile = new File(testProjectVhdbaselineFileName);
			
			Assert.assertEquals(testProjectVhdCode, FileUtils.readFileToString(baselineFile));
			
			// check entity1 file:
			String entity1VhdGeneratedCode = generatedCodes.get("entity1_1") ;
			String entity1VhdbaselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "ParentChildEntity1SeparateOr.bsl";
			
			File enitity1BaselineFile = new File(entity1VhdbaselineFileName);
			
			Assert.assertEquals(entity1VhdGeneratedCode, FileUtils.readFileToString(enitity1BaselineFile));
			
			// check entity2 file:
			String entity2VhdCode = generatedCodes.get("entity2_2");
			String entity2VhdbaselineFileName = TestUtils.BaselineFolderDirectory
					+ "\\" + "ParentChildEntity2InlineAndOr.bsl";

			File enitity2BaselineFile = new File(entity2VhdbaselineFileName);

			Assert.assertEquals(entity2VhdCode,
					FileUtils.readFileToString(enitity2BaselineFile));
			
	} catch (IOException e) {
		Assert.fail();
		e.printStackTrace();
	} catch (ProcGenException e) {
		Assert.fail();
		e.printStackTrace();
	}
	  
  }
  
}
