package vhdlOutputTests;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusObserver;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalValueRecord;
import com.fyp.procGenWeb.client.procGen.electronics.entities.MemoryUnit;
import com.fyp.procGenWeb.client.procGen.electronics.entities.OrGate;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.ElectronicsToVhdlConverter;
import com.fyp.procGenWeb.server.procGen.vMagic.EntityToVhdlConverter;
import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;

public class MemoryUnitToVhdlTest {
	  Project testProject = null;
		LogicFacade logicInterface = new LogicFacade();

		@BeforeMethod
		public void beforeMethod() {
		
			// Create a new project
			try {
				logicInterface.processInput("new_project testProject");
				testProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance();
				TestUtils.clearFilesInOutputFolder();
			} catch (ProcGenException e) {
				
				Assert.fail();
				e.printStackTrace();
			} catch (IOException e) {
				Assert.fail();
				e.printStackTrace();
			}
		}
		
		@AfterMethod
		public void afterMethod(){
			try {
				System.out.println(logicInterface.processInput("close_project"));
			} catch (ProcGenException e) {
				Assert.fail();
				e.printStackTrace();
			}
		}


		  @Test
		  public void testRomMemoryUnitToVhdl() {
				try {
					System.out.println(logicInterface.processInput("new_memory_unit testMemoryUnit 2 4 2 0 Rom"));
					MemoryUnit memUnit = (MemoryUnit) testProject.getEntityManager().getEntityById("1");
					EntityToVhdlConverter e = new EntityToVhdlConverter();
					System.out.println("Converting Memory unit...");
					
					ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(testProject);
					java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(testProject);

					String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
					String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "RomBasicVhdlCode.bsl";
					
					File baselineFile = new File(baselineFileName);
				
					Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
								}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				} catch (IOException e1) {
					Assert.fail();
					System.out.println(e1.getMessage());
					e1.printStackTrace();
				}
		  }
		  
		  
		  @Test
		  public void testRomMemoryUnitToVhdlAfterDtoConversion() {
				try {
					System.out.println(logicInterface.processInput("new_memory_unit testMemoryUnit 2 4 2 0 Rom"));
					MemoryUnit memUnit = (MemoryUnit) testProject.getEntityManager().getEntityById("1");
					EntityToVhdlConverter e = new EntityToVhdlConverter();
					System.out.println("Converting Memory unit after conversion...");
					
					ProjectTransferObject pto = new ProjectTransferObject(testProject);
					
					ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(pto.convertToProject());
					java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(pto.convertToProject());

					String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
					String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "RomBasicVhdlCode.bsl";
					
					File baselineFile = new File(baselineFileName);
				
					Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
								}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				} catch (IOException e1) {
					Assert.fail();
					System.out.println(e1.getMessage());
					e1.printStackTrace();
				}
		  }
		  
		  @Test
		  public void testRamMemoryUnitToVhdl() {
				try {
					System.out.println(logicInterface.processInput("new_memory_unit testMemoryUnit 2 4 2 0 Ram"));
					MemoryUnit memUnit = (MemoryUnit) testProject.getEntityManager().getEntityById("1");
					EntityToVhdlConverter e = new EntityToVhdlConverter();
					System.out.println("Converting Memory unit...");
					
					ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(testProject);
					java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(testProject);

					String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
					String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "RamBasicVhdlCode.bsl";
					
					File baselineFile = new File(baselineFileName);
				
					Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
								}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				} catch (IOException e1) {
					Assert.fail();
					System.out.println(e1.getMessage());
					e1.printStackTrace();
				}
		  }
		  
		  @Test
		  public void testRamMemoryUnitToVhdlAfterVhdlConversion() {
				try {
					System.out.println(logicInterface.processInput("new_memory_unit testMemoryUnit 2 4 2 0 Ram"));
					MemoryUnit memUnit = (MemoryUnit) testProject.getEntityManager().getEntityById("1");
					EntityToVhdlConverter e = new EntityToVhdlConverter();
					System.out.println("Converting Memory unit...");
					
					ProjectTransferObject pto = new ProjectTransferObject(testProject);
					
					ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(pto.convertToProject());
					java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(pto.convertToProject());

					String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
					String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "RamBasicVhdlCode.bsl";
					
					File baselineFile = new File(baselineFileName);
				
					Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
								}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				} catch (IOException e1) {
					Assert.fail();
					System.out.println(e1.getMessage());
					e1.printStackTrace();
				}
		  }
		  
		  @Test
		  public void testRamFunctionality(){
				try {
					System.out.println(logicInterface.processInput("new_memory_unit testMemoryUnit 2 4 2 0 Ram"));
					
					System.out.println(logicInterface.processInput("new_input_simulator simulator1 4 1 inputAddress"));
					System.out.println(logicInterface.processInput("new_input_simulator simulator2 1 1 enable"));
					System.out.println(logicInterface.processInput("new_input_simulator simulator3 2 1 inputData"));
					System.out.println(logicInterface.processInput("new_input_simulator simulator4 4 1 writeAddress"));
					
					SignalBusObserver outputObserver = new SignalBusObserver(testProject.getEntityManager().getEntityById("1").getOutputByName("output"),testProject.getProjectSimulator(),"1-output"); 
					
					System.out.println(testProject.getEntityManager().getEntityById("1").getOutputByName("output").getValue());
					
					System.out.println(logicInterface.processInput("add_input_stimulus 4 200 'change_sim_input simulator1 0000' 200 'change_sim_input simulator4 0000' 200 'change_sim_input simulator2 1' 200 'change_sim_input simulator3 11'"));
					
					System.out.println(logicInterface.processInput("add_input_stimulus 1 400 'change_sim_input simulator2 0'"));
					
					System.out.println(logicInterface.processInput("add_input_stimulus 2 600 'change_sim_input simulator1 0001' 600 'change_sim_input simulator1 0001'"));
					
					System.out.println(logicInterface.processInput("simulate 0 1"));
					
					outputObserver.printSignalLogToConsole();
					
					List<SignalValueRecord> outputSignalVariations = outputObserver.getSignalValueRecords();
					Assert.assertEquals(outputSignalVariations.size(),2);
					Assert.assertEquals(outputSignalVariations.get(0).getDisplayStringForCurrentValue(), "HH");
					Assert.assertEquals(outputSignalVariations.get(1).getDisplayStringForCurrentValue(), "ZZ");
					
				}		
				catch (ProcGenException e) {
					e.printStackTrace();
					Assert.fail();
				}

		  }
		  
		  
		  @Test
		  public void testRomFunctionality(){
				try {
					System.out.println(logicInterface.processInput("new_memory_unit testMemoryUnit 2 4 2 0 Rom"));
					
					System.out.println(logicInterface.processInput("new_input_simulator simulator1 4 1 inputAddress"));
					
					SignalBusObserver outputObserver = new SignalBusObserver(testProject.getEntityManager().getEntityById("1").getOutputByName("output"),testProject.getProjectSimulator(),"1-output"); 
					
					System.out.println(logicInterface.processInput("insert_contents_in_mem 1 00 11 "));
					
					System.out.println(testProject.getEntityManager().getEntityById("1").getOutputByName("output").getValue());
					
					System.out.println(logicInterface.processInput("add_input_stimulus 1 200 'change_sim_input simulator1 0001'"));
									
					System.out.println(logicInterface.processInput("add_input_stimulus 1 600 'change_sim_input simulator1 0000'"));
					
					System.out.println(logicInterface.processInput("simulate 0 1"));
					
					outputObserver.printSignalLogToConsole();
					
					List<SignalValueRecord> outputSignalVariations = outputObserver.getSignalValueRecords();
					Assert.assertEquals(outputSignalVariations.size(),3);
					Assert.assertEquals(outputSignalVariations.get(0).getDisplayStringForCurrentValue(), "LL");
					Assert.assertEquals(outputSignalVariations.get(1).getDisplayStringForCurrentValue(), "HH");
					Assert.assertEquals(outputSignalVariations.get(2).getDisplayStringForCurrentValue(), "LL");
				}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				}

		  }
		
}
