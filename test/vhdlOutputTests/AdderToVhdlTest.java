package vhdlOutputTests;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.Adder;
import com.fyp.procGenWeb.client.procGen.electronics.entities.AndGate;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.ElectronicsToVhdlConverter;
import com.fyp.procGenWeb.server.procGen.vMagic.EntityToVhdlConverter;

public class AdderToVhdlTest {
	  Project testProject = null;
		LogicFacade logicInterface = new LogicFacade();

		@BeforeMethod
		public void beforeMethod() {
		
			// Create a new project
			try {
				logicInterface.processInput("new_project testProject");
				testProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance();
				TestUtils.clearFilesInOutputFolder();
			} catch (ProcGenException e) {
				
				Assert.fail();
				e.printStackTrace();
			} catch (IOException e) {
				Assert.fail();
				e.printStackTrace();
			}
		}
		
		@AfterMethod
		public void afterMethod(){
			try {
				System.out.println(logicInterface.processInput("close_project"));
			} catch (ProcGenException e) {
				Assert.fail();
				e.printStackTrace();
			}
		}
		
	@Test
	public void convertAdder() {
		try {
			System.out.println(logicInterface.processInput("new_adder adder1 2 2 0 true"));
			
			System.out.println("Converting Adder...");
			
			ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(testProject);
			java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(testProject);

			String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
			String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "AdderVhdlCode.bsl";
			
			File baselineFile = new File(baselineFileName);
		
			System.out.println(actualOutputFileVhdCode);
			Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
						}		
		catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		} catch (IOException e1) {
			Assert.fail();
			System.out.println(e1.getMessage());
			e1.printStackTrace();
		}

	}
}
