package vhdlOutputTests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.ElectronicsToVhdlConverter;

public class TestConnectedGatesVhdlConversion {
	Project testProject = null;
	LogicFacade logicInterface = new LogicFacade();

	@BeforeMethod
	public void beforeMethod() {
	
		// Create a new project
		try {
			logicInterface.processInput("new_project testProject");
			testProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance();
			TestUtils.clearFilesInOutputFolder();
		} catch (ProcGenException e) {
			
			Assert.fail();
			e.printStackTrace();
		} catch (IOException e) {
			Assert.fail();
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	public void afterMethod(){
		try {
			System.out.println(logicInterface.processInput("close_project"));
		} catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		}
	}

	  @Test
	  public void testMultipleGateVhdlConversion(){
		  ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(testProject);
		  try {
				System.out.println(logicInterface.processInput("new_or_gate"));
				System.out.println(logicInterface.processInput("new_and_gate"));
				System.out.println(logicInterface.processInput("connect 1 2 output input0 default"));
				
				HashMap<String,String> generatedVhdlCodes = ec.convertProjectToHdl(testProject);
				
				String actualOutputVhdlCode = generatedVhdlCodes.get(testProject.getName());
				String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "ConnectedOrAndGates.bsl";
				
				File baselineFile = new File(baselineFileName);
				
				Assert.assertEquals(actualOutputVhdlCode, FileUtils.readFileToString(baselineFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProcGenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  } 
}
