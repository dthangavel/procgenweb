package vhdlOutputTests;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.entities.AndGate;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.server.procGen.vMagic.ElectronicsToVhdlConverter;
import com.fyp.procGenWeb.server.procGen.vMagic.EntityToVhdlConverter;
import com.fyp.procGenWeb.shared.dto.ProjectTransferObject;

public class AndGateToVhdlTest {
	  Project testProject = null;
		LogicFacade logicInterface = new LogicFacade();

		@BeforeMethod
		public void beforeMethod() {
		
			// Create a new project
			try {
				logicInterface.processInput("new_project testProject");
				testProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance();
				TestUtils.clearFilesInOutputFolder();
			} catch (ProcGenException e) {
				
				Assert.fail();
				e.printStackTrace();
			} catch (IOException e) {
				Assert.fail();
				e.printStackTrace();
			}
		}
		
		@AfterMethod
		public void afterMethod(){
			try {
				System.out.println(logicInterface.processInput("close_project"));
			} catch (ProcGenException e) {
				Assert.fail();
				e.printStackTrace();
			}
		}


		  @Test
		  public void testSimpleAndGateToVhdl() {
				try {
					System.out.println(logicInterface.processInput("new_and_gate"));
					AndGate andGate = (AndGate) testProject.getEntityManager().getEntityById("1");
					EntityToVhdlConverter e = new EntityToVhdlConverter();
					System.out.println("Converting And Gate...");
					
					ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(testProject);
					java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(testProject);

					String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
					String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "AndGateVhdlCode.bsl";
					
					File baselineFile = new File(baselineFileName);
				
					Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
								}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				} catch (IOException e1) {
					Assert.fail();
					System.out.println(e1.getMessage());
					e1.printStackTrace();
				}
		  }
		
		  
		  @Test
		  public void testMultipleInputAndGate() {
				try {
					System.out.println(logicInterface.processInput("new_and_gate multipleAnd 3 0 2"));
					AndGate andGate = (AndGate) testProject.getEntityManager().getEntityById("1");
					EntityToVhdlConverter e = new EntityToVhdlConverter();
					System.out.println("Converting And Gate...");
					
					ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(testProject);
					java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(testProject);

					String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
					String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "AndGateMultipleInputSignalBus.bsl";
					
					File baselineFile = new File(baselineFileName);
				
					Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
				}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				} catch (IOException e1) {
					Assert.fail();
					System.out.println(e1.getMessage());
					e1.printStackTrace();
				}
		  	}
		  
		  @Test
		  public void testSimpleAndGateToVhdlAfterDtoConversion() {
				try {
					System.out.println(logicInterface.processInput("new_and_gate"));
					AndGate andGate = (AndGate) testProject.getEntityManager().getEntityById("1");
					EntityToVhdlConverter e = new EntityToVhdlConverter();
					System.out.println("Converting And Gate After Dto conversion...");
					
					ProjectTransferObject projectDto = new ProjectTransferObject(testProject);
					
					ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(projectDto.convertToProject());
					java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(projectDto.convertToProject());

					String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
					String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "AndGateVhdlCode.bsl";
					
					File baselineFile = new File(baselineFileName);
				
					Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
								}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				} catch (IOException e1) {
					Assert.fail();
					System.out.println(e1.getMessage());
					e1.printStackTrace();
				}
		  }
		
		  
		  @Test
		  public void testMultipleInputAndGateAfterDtoConversion() {
				try {
					System.out.println(logicInterface.processInput("new_and_gate multipleAnd 3 0 2"));
					AndGate andGate = (AndGate) testProject.getEntityManager().getEntityById("1");
					EntityToVhdlConverter e = new EntityToVhdlConverter();
					System.out.println("Converting And Gate After Dto conversion...");
					
					ProjectTransferObject projectDto = new ProjectTransferObject(testProject);
					
					ElectronicsToVhdlConverter ec = new ElectronicsToVhdlConverter(projectDto.convertToProject());
					java.util.HashMap<String,String> generatedVhdl = ec.convertProjectToHdl(projectDto.convertToProject());

					String actualOutputFileVhdCode = generatedVhdl.get(testProject.getName());
					String baselineFileName = TestUtils.BaselineFolderDirectory + "\\" + "AndGateMultipleInputSignalBus.bsl";
					
					File baselineFile = new File(baselineFileName);
				
					Assert.assertEquals(actualOutputFileVhdCode, FileUtils.readFileToString(baselineFile));
				}		
				catch (ProcGenException e) {
					Assert.fail();
					e.printStackTrace();
				} catch (IOException e1) {
					Assert.fail();
					System.out.println(e1.getMessage());
					e1.printStackTrace();
				}
		  	}
}
