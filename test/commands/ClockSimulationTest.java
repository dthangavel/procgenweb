package commands;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.Signal;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusObserver;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalValueRecord;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;
import com.fyp.procGenWeb.client.procGen.simulation.ProjectSimulator;

public class ClockSimulationTest {
	Project testProject = null;
	LogicFacade logicInterface = new LogicFacade();

	@BeforeMethod
	public void beforeMethod() {
	
		// Create a new project
		try {
			logicInterface.processInput("new_project testProject");
			testProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance();
		} catch (ProcGenException e) {
			
			Assert.fail();
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	public void afterMethod(){
		try {
			System.out.println(logicInterface.processInput("close_project"));
		} catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		}
	}

  @Test
  public void testCircuitClock() {
	  try {
		ProjectSimulator newPS = testProject.getProjectSimulator();
		SignalBusObserver prjClkObserver = new SignalBusObserver(newPS.getRisingEdgeTriggerSignal(),newPS,"clockObserver"); 
		
		float timeToSimulate = 1000;
		float clockPeriod = 200;
		newPS.runSimulation(timeToSimulate, clockPeriod,false,false);
		
		List<SignalValueRecord> actualClkValues = prjClkObserver.getSignalValueRecords();
		
		// computing expectedClkValue
		Signal[] initialOldSignal = new Signal[1];
		initialOldSignal[0] = Signal.UNDEFINED;
		
		Signal[] initialNewSignal = new Signal[1];
		initialNewSignal[0] = Signal.LOW;
		
		SignalValueRecord recordAfterChange = new SignalValueRecord(0,initialOldSignal,initialNewSignal);
		
		Assert.assertEquals(actualClkValues.get(0),recordAfterChange);
		float t = 0;
		Signal signalValue = Signal.HIGH;
		
		for(int i =0; i< actualClkValues.size();i++){
			SignalValueRecord recordAfterFirstChange = actualClkValues.get(i);
			Assert.assertEquals(recordAfterFirstChange.getTimeOfRecord(), t);
			t = t + clockPeriod/2;

			if(signalValue == Signal.HIGH){
				Assert.assertEquals(recordAfterFirstChange.getDisplayStringForCurrentValue(), "L");
				signalValue = Signal.LOW;
			}
			
			else{
				Assert.assertEquals(recordAfterFirstChange.getDisplayStringForCurrentValue(), "H");
				signalValue = Signal.HIGH;
			}
		}
		
		prjClkObserver.printSignalLogToConsole();
		
	} catch (ProcGenException e) {
		Assert.fail();
		e.printStackTrace();
	}
  }

}
