package commands;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.electronics.core.Project;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalBusObserver;
import com.fyp.procGenWeb.client.procGen.electronics.core.SignalValueRecord;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

public class SimpleProgramCounterMemoryTest {

	Project testProject = null;
	LogicFacade logicInterface = new LogicFacade();
	
	
	SignalBusObserver outputObserver;
	SignalBusObserver adder_2_Observer;

	@BeforeMethod
	public void beforeMethod() {
	
		// Create a new project
		try {
			logicInterface.processInput("new_project testProject");
			testProject = ElectronicsLogicFacade.getInstance().getActivePrjectInstance();
		} catch (ProcGenException e) {
			
			Assert.fail();
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	public void afterMethod(){
		try {
			System.out.println(logicInterface.processInput("close_project"));
		} catch (ProcGenException e) {
			Assert.fail();
			e.printStackTrace();
		}
	}
	
  public void createAndAddProgramCounterToProject() throws ProcGenException{
	  	System.out.println(logicInterface.processInput("new_entity Program-Counter 2 1 0 clk 1 input0 2 output 2"));
	  	System.out.println(logicInterface.processInput("set_trigger_type 1 rising"));
	  	
	  	System.out.println(logicInterface.processInput("new_register 2 rising pc_reg 1"));
	  	System.out.println(logicInterface.processInput("connect 1 1-1 input0 input0 default"));
	  	System.out.println(logicInterface.processInput("connect 1 1-1 clk clock default"));
	  
	  	System.out.println(logicInterface.processInput("connect 1 1-1 input0 input0 default")); // connect pc to register
	  	System.out.println(logicInterface.processInput("connect 1-1 1 output output default"));
	  	System.out.println(logicInterface.processInput("connect_to_constant 1-1 enable 1"));
	  	
	  	System.out.println(logicInterface.processInput("new_adder adder1 2 2 0"));
	  	
		outputObserver = new SignalBusObserver(testProject.getEntityManager().getEntityById("1").getOutputByName("output"),testProject.getProjectSimulator(),"1-output");
		adder_2_Observer = new SignalBusObserver(testProject.getEntityManager().getEntityById("2").getOutputByName("output"),testProject.getProjectSimulator(),"2-output");

	  	
		System.out.println(logicInterface.processInput("connect_to_constant 2 input1 1"));
		System.out.println(logicInterface.processInput("connect 1 2 output input0 default"));
		System.out.println(logicInterface.processInput("connect 2 1 output input0 default"));
	  	
		System.out.println(logicInterface.processInput("connect_to_clock 1 clk"));
		System.out.println(logicInterface.processInput("initialise_signal_bus 1 output 00"));
  } 	
	
  @Test
  public void testProgramCounter() {
	  try {
		  createAndAddProgramCounterToProject();
		  		  	
			System.out.println(logicInterface.processInput("simulate 0 1 1200"));
			
			System.out.println("\n Output observer");
			outputObserver.printSignalLogToConsole();
			
			System.out.println("\n Adder observer");
			adder_2_Observer.printSignalLogToConsole();
			
			List<SignalValueRecord> outputSignalVariations = outputObserver.getSignalValueRecords();
			List<SignalValueRecord> adder_2_variations = adder_2_Observer.getSignalValueRecords();
			
			Assert.assertEquals(outputSignalVariations.size(),7);
			Assert.assertEquals(adder_2_variations.size(),7);
			
			Assert.assertEquals(outputSignalVariations.get(0).getDisplayStringForCurrentValue(),"LL");
			Assert.assertEquals(outputSignalVariations.get(1).getDisplayStringForCurrentValue(),"LH");
			Assert.assertEquals(outputSignalVariations.get(2).getDisplayStringForCurrentValue(),"HL");
			Assert.assertEquals(outputSignalVariations.get(3).getDisplayStringForCurrentValue(),"HH");
			Assert.assertEquals(outputSignalVariations.get(4).getDisplayStringForCurrentValue(),"LL");
			Assert.assertEquals(outputSignalVariations.get(5).getDisplayStringForCurrentValue(),"LH");
			Assert.assertEquals(outputSignalVariations.get(6).getDisplayStringForCurrentValue(),"HL");
			
			Assert.assertEquals(adder_2_variations.get(0).getDisplayStringForCurrentValue(),"LH");
			Assert.assertEquals(adder_2_variations.get(1).getDisplayStringForCurrentValue(),"HL");
			Assert.assertEquals(adder_2_variations.get(2).getDisplayStringForCurrentValue(),"HH");
			Assert.assertEquals(adder_2_variations.get(3).getDisplayStringForCurrentValue(),"LL");
			Assert.assertEquals(adder_2_variations.get(4).getDisplayStringForCurrentValue(),"LH");
			Assert.assertEquals(adder_2_variations.get(5).getDisplayStringForCurrentValue(),"HL");
			Assert.assertEquals(adder_2_variations.get(6).getDisplayStringForCurrentValue(),"HH");
			//Assert.fail("Incomplete");
			
	} catch (ProcGenException e) {
		e.printStackTrace();
		Assert.fail();
	}
  }
  
  @Test
  public void createRomForStoringInstructions(){
	  try {
		  	createAndAddProgramCounterToProject();
			
		  	System.out.println(logicInterface.processInput("new_memory_unit testMemoryUnit 4 2 4 0 Rom"));
		  	System.out.println(logicInterface.processInput("connect 1 3 output inputAddress default"));
		  	
			System.out.println(logicInterface.processInput("insert_contents_in_mem 3 1000 1011 1100 1001"));
		  	
			SignalBusObserver rom_3_Observer = new SignalBusObserver(testProject.getEntityManager().getEntityById("3").getOutputByName("output"),testProject.getProjectSimulator(),"3-output");
			
			
			System.out.println(logicInterface.processInput("simulate 0 1 1200"));
			
			System.out.println("\n Output observer");
			outputObserver.printSignalLogToConsole();
			
			System.out.println("\n Adder observer");
			adder_2_Observer.printSignalLogToConsole();
			
			System.out.println("\n Rom observer");
			rom_3_Observer.printSignalLogToConsole();
			
			List<SignalValueRecord> outputSignalVariations = outputObserver.getSignalValueRecords();
			List<SignalValueRecord> adder_2_variations = adder_2_Observer.getSignalValueRecords();
			
	} catch (ProcGenException e) {
		e.printStackTrace();
		Assert.fail();
	}
	  
  }
}
