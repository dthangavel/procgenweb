package commands;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fyp.procGenWeb.client.procGen.electronics.core.ElectronicsLogicFacade;
import com.fyp.procGenWeb.client.procGen.logic.LogicFacade;
import com.fyp.procGenWeb.client.procGen.logicHelper.ProcGenException;

public class NewProjectTest {
  @Test
  public void createNewProject() {
	  LogicFacade logicInterface = new LogicFacade();
	  try {
		  Assert.assertEquals(ElectronicsLogicFacade.getInstance().getActivePrjectInstance(),null);
		  
		  System.out.println(logicInterface.processInput("new_project testProject"));
		  Assert.assertNotNull(ElectronicsLogicFacade.getInstance().getActivePrjectInstance());
		  System.out.println(logicInterface.processInput("close_project"));
		  
	} catch (ProcGenException e) {
		Assert.fail();
		e.printStackTrace();
	}
  }

}
